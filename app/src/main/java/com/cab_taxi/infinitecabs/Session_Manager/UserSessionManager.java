package com.cab_taxi.infinitecabs.Session_Manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.LoginSignUp.Login;

import java.util.HashMap;


public class UserSessionManager {

    // Shared Preferences reference
    static SharedPreferences pref;

    // Editor reference for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "User Login Data";

    private static final String IS_LOGIN = "IsLoggedIn";
    // User name (make variable public to access from outside)
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "first_name";
    public static final String KEY_NAME_LAST = "last_name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_USER_STATUS = "user_status";
    public static final String KEY_IMAGE = "user_image";
    public static final String KEY_NOTIFICATION = "notification_status";


    // Constructor
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void createUserLoginSession(String id, String name, String last_name, String email, String mobile,
                                       String user_status,String user_image,String notification_status) {
        // Storing login value as TRUE

        // Storing name in pref
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_NAME_LAST, last_name);
        editor.putString(KEY_IMAGE, user_image);
        editor.putString(KEY_NOTIFICATION, notification_status);

        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_USER_STATUS, user_status);


        editor.apply();
        editor.commit();

    }

    public void setLogin(){
        editor.putBoolean(IS_LOGIN, true);
        editor.apply();
        editor.commit();
    }

    public void createUserLoginSession1(String notification_status) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_NOTIFICATION, notification_status);
        editor.apply();
        editor.commit();

    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_NAME_LAST, pref.getString(KEY_NAME_LAST, null));
        user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));
        user.put(KEY_USER_STATUS, pref.getString(KEY_USER_STATUS, null));
        user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, null));
        user.put(KEY_NOTIFICATION, pref.getString(KEY_NOTIFICATION, null));


        // return user
        return user;
    }

    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, Login.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        } else {
            Intent i1 = new Intent(_context, Dashboard.class);
            i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i1);

        }

    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        editor.putBoolean(IS_LOGIN, false);
        editor.apply();
        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, Login.class);
        // After logout redirect user to Login Activity

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        _context.startActivity(i);


    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public static String isLoginId() {
        return pref.getString(KEY_ID, "0");

    }

    public void iseditor() {
        editor.clear();
        editor.commit();

    }
}