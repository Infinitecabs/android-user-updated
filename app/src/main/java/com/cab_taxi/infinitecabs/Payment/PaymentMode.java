package com.cab_taxi.infinitecabs.Payment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.Model.Card_List;
import com.cab_taxi.infinitecabs.Model.DeleteCardModel;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMode extends AppCompatActivity implements View.OnClickListener {
    List<Card_List.Cash> paymenttypeList;
    List<Card_List.Cash> paymenttypenew;
    List<Card_List.Paywith> cardList;
    PaymentModeAdapter card_listAdapter;
    UserSessionManager session;
    HashMap<String, String> user;
    int row_index;
    boolean position1 = false;
    NoInternetDialog noInternetDialog;
    private ImageView iv_close;
    private TextView tv_done;
    private RecyclerView recyclerViewmain;
    private NestedScrollView nestedScrollView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mode);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(PaymentMode.this).build();
        Window window = this.getWindow();        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_close = findViewById(R.id.iv_close);
        tv_done = findViewById(R.id.tv_done);
        recyclerViewmain = findViewById(R.id.recyclerViewmain);


        iv_close.setOnClickListener(this);
        tv_done.setOnClickListener(this);
       /* checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent intent = new Intent();
                    Log.d("vishal123", intent + "");
                    intent.putExtra("Payment_mode", "Cash");
                    intent.putExtra("card_id", "");
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    CustomToast.makeText(PaymentMode.this, "Please Select a Payment Method ", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                }
            }
        });*/
        Map<String, String> map = new HashMap<>();
        //(email,password)
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        Card_Lists(map);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        iv_close.setOnClickListener(this);
        tv_done = (TextView) findViewById(R.id.tv_done);
        tv_done.setOnClickListener(this);
        recyclerViewmain = (RecyclerView) findViewById(R.id.recyclerViewmain);
        recyclerViewmain.setOnClickListener(this);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        nestedScrollView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                finish();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;
            case R.id.tv_done:
                Intent intent = new Intent();
                Log.d("vishal123", Utility.getPaytype(PaymentMode.this) + "__"+Utility.getCard(PaymentMode.this));
                intent.putExtra("Payment_mode", Utility.getPaytype(PaymentMode.this));
                intent.putExtra("card_id", Utility.getCard(PaymentMode.this));
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.btn_addPaymentMethod:
                startActivity(new Intent(PaymentMode.this, AddCard.class)
                        .putExtra("cardform", "Payment Mode"));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
        }
    }

    private void deleteCard(Map<String, String> map) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<DeleteCardModel> call = Apis.getAPIService().deleteCard(map);
        call.enqueue(new Callback<DeleteCardModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<DeleteCardModel> call, Response<DeleteCardModel> response) {
                dialog.dismiss();
                DeleteCardModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        Map<String, String> map = new HashMap<>();
                        //(email,password)
                        map.put("user_id", user.get(UserSessionManager.KEY_ID));
                        Card_Lists(map);

                    } else {
//                        swipeToRefresh.setVisibility(View.GONE);
                        CustomToast.makeText(PaymentMode.this, userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<DeleteCardModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Card_Lists(Map<String, String> map) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Card_List> call = Apis.getAPIService().getCardList(map);
        call.enqueue(new Callback<Card_List>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<Card_List> call, Response<Card_List> response) {
                dialog.dismiss();
                Card_List userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        paymenttypeList = new ArrayList<>();
                        cardList = new ArrayList<>();
                        paymenttypeList = userdata.getData().getCash();
                        paymenttypenew = new ArrayList<>();
                        Log.d("hgfhgfhg",paymenttypeList.size()+"");
                        for (int i =0;i<paymenttypeList.size();i++){
                            if (paymenttypeList.get(i).getSelected().equals("1")){
                                paymenttypenew.add(0,paymenttypeList.get(i));
                            }else{
                            }
                        }
                        for (int i =0;i<paymenttypeList.size();i++){
                            if (paymenttypeList.get(i).getSelected().equals("0")){
                                paymenttypenew.add(1,paymenttypeList.get(i));
                            }else{
                            }
                        }

                        cardList = userdata.getData().getPaywith();
                        if (paymenttypeList.size() > 0) {
                            recyclerViewmain.setAdapter(new PaymentMainAdapter());
                        }
                        if (cardList.size() > 0) {
                            recyclerViewmain.setAdapter(new PaymentMainAdapter());
                        }
                    } else {

                    }

                }
            }

            @Override
            public void onFailure(Call<Card_List> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        //(email,password)
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        Card_Lists(map);
    }

    public class PaymentModeAdapter extends RecyclerView.Adapter<PaymentModeAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_payment_mode, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (cardList.get(position).getType().equals("1")) {
                holder.tvcardtype.setText("Personal");
                if (!cardList.get(position).getCard_number().isEmpty()) {
                    holder.add_card.setVisibility(View.GONE);
                    holder.ll_card.setVisibility(View.VISIBLE);
                    String last3;
                    if (cardList.get(position).getCard_number() == null || cardList.get(position).getCard_number().length() < 4) {
                        last3 = cardList.get(position).getCard_number();
                    } else {
                        last3 = cardList.get(position).getCard_number().substring(cardList.get(position).getCard_number().length() - 4);
                    }
                    holder.tv_expiryDate.setText(cardList.get(position).getExpairy_date());
                    holder.tv_card_number.setText("****************" + last3);
                    holder.checkman.setEnabled(true);
                    if (cardList.get(position).getSelected().equals("1")) {
                        holder.checkman.setChecked(true);
                    } else {
                        holder.checkman.setChecked(false);
                    }
                } else {
                    holder.checkman.setVisibility(View.GONE);
                    holder.checkman.setEnabled(false);
                    holder.add_card.setVisibility(View.VISIBLE);
                    holder.ll_card.setVisibility(View.GONE);
                }
            } else {
                holder.tvcardtype.setText("Business");
                if (!cardList.get(position).getCard_number().isEmpty()) {
                    holder.add_card.setVisibility(View.GONE);
                    holder.ll_card.setVisibility(View.VISIBLE);
                    String last3;
                    if (cardList.get(position).getCard_number() == null || cardList.get(position).getCard_number().length() < 4) {
                        last3 = cardList.get(position).getCard_number();
                    } else {
                        last3 = cardList.get(position).getCard_number().substring(cardList.get(position).getCard_number().length() - 4);
                    }
                    if (cardList.get(position).getSelected().equals("1")) {
                        holder.checkman.setChecked(true);
                    } else {
                        holder.checkman.setChecked(false);
                    }
                    holder.tv_expiryDate.setText(cardList.get(position).getExpairy_date());
                    holder.tv_card_number.setText("****************" + last3);
                    holder.checkman.setEnabled(true);
                    holder.checkman.setVisibility(View.VISIBLE);
                } else {
                    holder.checkman.setVisibility(View.GONE);
                    holder.checkman.setEnabled(false);
                    holder.add_card.setVisibility(View.VISIBLE);
                    holder.ll_card.setVisibility(View.GONE);
                }
            }
            holder.add_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cardList.get(position).getType().equals("1")){
                        startActivity(new Intent(PaymentMode.this, AddCard.class)
                                .putExtra("cardform", "Payment Mode").putExtra("type", "1"));
                        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                    }else{
                        startActivity(new Intent(PaymentMode.this, AddCard.class)
                                .putExtra("cardform", "Payment Mode").putExtra("type", "2"));
                        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                    }

                }
            });

            holder.checkman.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.checkman.isChecked()) {
                        Utility.setCard(PaymentMode.this,cardList.get(position).getId(),"card");

                        Map<String, String> map = new HashMap<>();
                        //(email,password)
                        map.put("user_id", user.get(UserSessionManager.KEY_ID));
                        if (cardList.get(position).getType().equals("1")) {
                            map.put("selected", cardList.get(position).getId());
//                            map.put("paywith", "card");
                        }else{
                            map.put("selected", cardList.get(position).getId());
//                            map.put("paywith", "card");
                        }
                        Card_Lists(map);
                        // Code to display your message.
                    } else {
                        holder.checkman.setChecked(true);
                    }

                }
            });
            holder.iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(PaymentMode.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Infinite Cabs")
                            .setContentText("Do you want to delete the card?")
                            .setConfirmText("OK")
                            .showCancelButton(false)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                    if (holder.checkman.isChecked()){
                                        Utility.setCard(PaymentMode.this,"0","0");
                                    }
                                    Map<String, String> map = new HashMap<>();
                                    //(email,password)
                                    map.put("id", cardList.get(position).getId());
                                    map.put("user_id", user.get(UserSessionManager.KEY_ID));
                                    deleteCard(map);
                                }
                            })
                            .show();

                }
            });


        }


        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private LinearLayout ll_payment_item, add_card;
            private RelativeLayout ll_card;
            private RelativeLayout rlcardlayout;
            private ImageView iv_cardtype;
            private TextView iv_delete;
            private TextView tv_card_number;
            private TextView tv_expiryDate;
            private TextView tvcardtype;
            private CheckBox checkman;

            public ViewHolder(View itemView) {
                super(itemView);
                this.iv_cardtype = itemView.findViewById(R.id.iv_cardtype);
                this.tvcardtype = itemView.findViewById(R.id.tvcardtype);
                this.iv_delete = itemView.findViewById(R.id.iv_delete);
                this.ll_payment_item = itemView.findViewById(R.id.ll_payment_item);
                this.rlcardlayout = itemView.findViewById(R.id.rlcardlayout);
                this.ll_card = itemView.findViewById(R.id.ll_card);
                this.tv_card_number = itemView.findViewById(R.id.tv_card_number);
                this.tv_expiryDate = itemView.findViewById(R.id.tv_expiryDate);
                this.add_card = itemView.findViewById(R.id.add_card);
                this.checkman = itemView.findViewById(R.id.checkman);
            }
        }
    }

    public class PaymentMainAdapter extends RecyclerView.Adapter<PaymentMainAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.main_card_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.recyclerViewCard.setAdapter(new PaymentModeAdapter());

            if (position==0){
                if (paymenttypenew.get(position).getType().equals("cash")) {
                    holder.ivicon.setImageResource(R.drawable.ic_business_);
                    holder.texttitle.setText("Pay the Driver Directly");
                    holder.tv_subtitle.setText("with Cash/Eftpos");
                    holder.uppertitle.setVisibility(View.GONE);
                    holder.cb_cash.setEnabled(true);
                    holder.recyclerViewCard.setVisibility(View.GONE);
                } else {
                    if (paymenttypenew.get(position).getSelected().equals("1")) {
                        holder.cb_cash.setVisibility(View.VISIBLE);
                    } else {
                        holder.cb_cash.setVisibility(View.GONE);
                    }
                    holder.ivicon.setImageResource(R.drawable.ic_phone);
                    holder.uppertitle.setVisibility(View.GONE);
                    holder.texttitle.setText("Pay with the App");
                    holder.cb_cash.setEnabled(false);
                    holder.tv_subtitle.setVisibility(View.GONE);

                }
            }else{
                if (paymenttypenew.get(position).getType().equals("cash")) {
                    holder.ivicon.setImageResource(R.drawable.ic_business_);
                    holder.texttitle.setText("Pay the Driver Directly");
                    holder.tv_subtitle.setText("with Cash/Eftpos");
                    holder.uppertitle.setVisibility(View.VISIBLE);
                    holder.cb_cash.setEnabled(true);
                    holder.recyclerViewCard.setVisibility(View.GONE);
                } else {
                    if (paymenttypenew.get(position).getSelected().equals("1")) {
                        holder.cb_cash.setVisibility(View.VISIBLE);
                    } else {
                        holder.cb_cash.setVisibility(View.GONE);
                    }

                    holder.ivicon.setImageResource(R.drawable.ic_phone);
                    holder.uppertitle.setVisibility(View.VISIBLE);
                    holder.cb_cash.setEnabled(false);
                    holder.texttitle.setText("Pay with the App");
                    holder.tv_subtitle.setVisibility(View.GONE);

                }
            }

            if (paymenttypenew.get(position).getSelected().equals("1")) {
                holder.cb_cash.setChecked(true);
            } else {
                holder.cb_cash.setChecked(false);
            }

            holder.cb_cash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.cb_cash.isChecked()) {

                        if (paymenttypenew.get(position).getType().equals("cash")) {
                            Utility.setCard(PaymentMode.this,"0","cash");
                            Map<String, String> map = new HashMap<>();
                            map.put("user_id", user.get(UserSessionManager.KEY_ID));
                            map.put("paywith", "cash");
                            Card_Lists(map);
                        }else{
                        }
                        // Code to display your message.
                    } else {
                        holder.cb_cash.setChecked(true);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return paymenttypenew.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView ivicon;
            private TextView texttitle;
            private TextView tv_subtitle, uppertitle;
            private CheckBox cb_cash;
            private RecyclerView recyclerViewCard;

            public ViewHolder(View itemView) {
                super(itemView);
                this.recyclerViewCard = itemView.findViewById(R.id.recyclerViewCard);
                this.ivicon = itemView.findViewById(R.id.ivicon);
                this.texttitle = itemView.findViewById(R.id.texttitle);
                this.tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
                this.uppertitle = itemView.findViewById(R.id.uppertitle);
                this.cb_cash = itemView.findViewById(R.id.cb_cash);
            }
        }
    }
}
