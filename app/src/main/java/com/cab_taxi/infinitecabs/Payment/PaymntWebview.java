package com.cab_taxi.infinitecabs.Payment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;

import androidx.appcompat.app.AppCompatActivity;

public class PaymntWebview extends AppCompatActivity {

    private WebView webView;
    private ImageView iv_back;
    private RelativeLayout header;
    private RelativeLayout rlprogress;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymnt_webview);
        noInternetDialog = new NoInternetDialog.Builder(PaymntWebview.this).build();
        initView();
    }

    NoInternetDialog noInternetDialog;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        rlprogress = findViewById(R.id.rlprogress);
        webView = findViewById(R.id.webView);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        header = (RelativeLayout) findViewById(R.id.header);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl(getIntent().getStringExtra("url"));
        if (getIntent().getStringExtra("url").equals("cash")) {
            webView.setVisibility(View.GONE);
            rlprogress.setVisibility(View.VISIBLE);
        } else {
            webView.setVisibility(View.VISIBLE);
            rlprogress.setVisibility(View.GONE);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    //return true load with system-default-browser or other browsers, false with your webView
                    return false;
                }

                @Override
                public void onLoadResource(WebView view, String url) {
                    super.onLoadResource(view, url);
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    if (url.contains("https://admin.infinitecabs.com.au/Cardsuccess")) //check if that's a url you want to load internally
                    {
                        startActivity(new Intent(PaymntWebview.this, PaymenttSuccess.class).putExtra("tag", "1"));

                    } else if (url.equals("https://admin.infinitecabs.com.au/payment_failed")) {
                        startActivity(new Intent(PaymntWebview.this, PaymenttSuccess.class).putExtra("tag", "2"));
                    }

                    Log.d("sdsgd", "_" + url + "");
                }

                @Override
                public void onReceivedError(WebView view, int errorCode,
                                            String description, String failingUrl) {
                    startActivity(new Intent(PaymntWebview.this, PaymenttSuccess.class).putExtra("tag", "2"));
                }
            });
            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {

                    Log.d("sdsgd", String.valueOf(newProgress));
                    //put your code here if your want to show the progress with progressbar
                }
            });

            webView.loadUrl(getIntent().getStringExtra("url"));
        }
    }
}
