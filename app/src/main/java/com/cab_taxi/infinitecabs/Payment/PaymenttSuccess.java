package com.cab_taxi.infinitecabs.Payment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.R;

import androidx.appcompat.app.AppCompatActivity;

public class PaymenttSuccess extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivimage;
    private ImageView ivimage1;
    private Button btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymentt_success);
        initView();
    }

    private void initView() {
        ivimage = findViewById(R.id.ivimage);
        ivimage1 = findViewById(R.id.ivimage1);
        if (getIntent().getStringExtra("tag").equals("1")){
            ivimage.setVisibility(View.VISIBLE);
            ivimage1.setVisibility(View.GONE);
        }else{
            ivimage.setVisibility(View.GONE);
            ivimage1.setVisibility(View.VISIBLE);
            ivimage.setBackground(getResources().getDrawable(R.drawable.paymentfaild));
        }
        btnHome = findViewById(R.id.btnHome);
        btnHome.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PaymenttSuccess.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnHome:
                startActivity(new Intent(PaymenttSuccess.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;
        }
    }
}
