package com.cab_taxi.infinitecabs.Payment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.braintreepayments.cardform.view.CardForm;
import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.Terms_and_conditions;
import com.cab_taxi.infinitecabs.Model.Insert_Card_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab_taxi.infinitecabs.Api.Apis.CARD_URL;

public class AddCard extends AppCompatActivity implements View.OnClickListener {

    DatePickerDialog dateDlg;
    UserSessionManager session;
    HashMap<String, String> user;
    CardForm cardForm;
    int LAUNCH_SECOND_ACTIVITY = 123;
    NoInternetDialog noInternetDialog;
    private ImageView iv_back;
    private Button btn_save;
    private TextView card_date;
    private TextInputLayout card_date1;
    private EditText et_holder_name;
    private EditText card_number;
    private EditText et_cvv;
    private CardView header1;
    private TextView card_add;
    private CardForm card_form;
    private RelativeLayout holder_name;
    private CheckBox checkbox;
    private AppCompatTextView tvterm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        noInternetDialog = new NoInternetDialog.Builder(AddCard.this).build();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        btn_save = findViewById(R.id.btn_save);
        iv_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);

//        et_holder_name = (EditText) findViewById(R.id.et_holder_name);

        cardForm = findViewById(R.id.card_form);
        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .cardholderName(CardForm.FIELD_REQUIRED)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .mobileNumberExplanation("SMS is required on this number")
                .actionLabel("Purchase")
                .setup(AddCard.this);

//        card_number.addTextChangedListener(new TextWatcher() {
//
//            private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
//            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
//            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
//            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
//            private static final char DIVIDER = '-';
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // noop
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // noop
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
//                    s.replace(0, s.length(), buildCorrectString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
//                }
//            }
//
//            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
//                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
//                for (int i = 0; i < s.length(); i++) { // check that every element is right
//                    if (i > 0 && (i + 1) % dividerModulo == 0) {
//                        isCorrect &= divider == s.charAt(i);
//                    } else {
//                        isCorrect &= Character.isDigit(s.charAt(i));
//                    }
//                }
//                return isCorrect;
//            }
//
//            private String buildCorrectString(char[] digits, int dividerPosition, char divider) {
//                final StringBuilder formatted = new StringBuilder();
//
//                for (int i = 0; i < digits.length; i++) {
//                    if (digits[i] != 0) {
//                        formatted.append(digits[i]);
//                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
//                            formatted.append(divider);
//                        }
//                    }
//                }
//
//                return formatted.toString();
//            }
//
//            private char[] getDigitArray(final Editable s, final int size) {
//                char[] digits = new char[size];
//                int index = 0;
//                for (int i = 0; i < s.length() && index < size; i++) {
//                    char current = s.charAt(i);
//                    if (Character.isDigit(current)) {
//                        digits[index] = current;
//                        index++;
//                    }
//                }
//                return digits;
//            }
//        });
        header1 = findViewById(R.id.header1);
        header1.setOnClickListener(this);
        card_add = findViewById(R.id.card_add);
        card_add.setOnClickListener(this);
        card_form = findViewById(R.id.card_form);
        card_form.setOnClickListener(this);
        holder_name = findViewById(R.id.holder_name);
        holder_name.setOnClickListener(this);
        checkbox = findViewById(R.id.checkbox);
        checkbox.setOnClickListener(this);
        tvterm = findViewById(R.id.tvterm);
        tvterm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;
            case R.id.tvterm:
                startActivity(new Intent(this, Terms_and_conditions.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.btn_save:
                submit();
                break;
            /*case R.id.card_date:
                final Calendar c = Calendar.getInstance();
                int mYear1 = c.get(Calendar.YEAR);
                int mMonth1 = c.get(Calendar.MONTH);
                int mDay1 = c.get(Calendar.DAY_OF_MONTH);
                dateDlg = new DatePickerDialog(AddCard.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        CharSequence strDate = null;
                        Time chosenDate = new Time();
                        chosenDate.set(dayOfMonth, monthOfYear, year);
                        long dtDob = chosenDate.toMillis(true);

                        /// strDate = DateFormat.format("dd-MM-yyyy", dtDob);
                        strDate = DateFormat.format("MM/yyyy", dtDob);

                        card_date.setText(strDate);
                    }
                }, mYear1, mMonth1, mDay1);

                dateDlg.show();
                break;*/
        }
    }

    private void submit() {

        // validate
        String name = cardForm.getCardholderName().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Card Holder Name", Toast.LENGTH_SHORT).show();
            return;
        }

        String number = cardForm.getCardNumber().trim();
        if (TextUtils.isEmpty(number)) {
            Toast.makeText(this, "Credit Card Number", Toast.LENGTH_SHORT).show();
            return;
        }

        String cvv = cardForm.getCvv().trim();
        if (TextUtils.isEmpty(cvv)) {
            Toast.makeText(this, "CVV", Toast.LENGTH_SHORT).show();
            return;
        }
        String carddate = cardForm.getExpirationMonth().trim();
        if (TextUtils.isEmpty(carddate)) {
            Toast.makeText(this, "Add Expiry Date", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!checkbox.isChecked()) {
            Toast.makeText(this, "Please accept terms and conditions", Toast.LENGTH_SHORT).show();
            return;
        }


        Log.d("cardtype", cardForm.getTransitionName() + "");
        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        map.put("card_holder_name", name);
        map.put("card_number", number);
        map.put("csv", cvv);
        map.put("expairy", carddate + "/" + cardForm.getExpirationYear());
        map.put("type", getIntent().getStringExtra("type"));
        map.put("card_type", "cardtype");
        AddCard(map);

    }

    public void AddCard(final Map<String, String> map) {
        final Dialog dialog = new Dialog(AddCard.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Insert_Card_Model> call = Apis.getAPIService().getInsertCard(map);

        call.enqueue(new Callback<Insert_Card_Model>() {
            @Override
            public void onResponse(Call<Insert_Card_Model> call, Response<Insert_Card_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Insert_Card_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                       /* Intent i = new Intent(AddCard.this, PaymntWebview_addcard.class);
                        i.putExtra("url", CARD_URL + user.getCardId());
                        startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);*/

                        final Dialog dialog = new Dialog(AddCard.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_webview);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                       WebView webView = dialog.findViewById(R.id.webView);
                       ImageView iv_back = dialog.findViewById(R.id.iv_back);
                        iv_back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.cancel();
                            }
                        });
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.getSettings().setLoadWithOverviewMode(true);
                        webView.getSettings().setUseWideViewPort(true);
                        webView.getSettings().setBuiltInZoomControls(true);
                        webView.getSettings().setDisplayZoomControls(false);
                        webView.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                //return true load with system-default-browser or other browsers, false with your webView
                                return false;
                            }

                            @Override
                            public void onLoadResource(WebView view, String url) {
                                super.onLoadResource(view, url);
                            }

                            @Override
                            public void onPageFinished(WebView view, String url) {
                                super.onPageFinished(view, url);

                            }

                            @Override
                            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                super.onPageStarted(view, url, favicon);
                                Log.d("sdfasdfasd", url + "__");
                                if (url.contains("https://admin.infinitecabs.com.au/Cardsuccess")) //check if that's a url you want to load internally
                                {
                                    CustomToast.makeText(getApplicationContext(), "Card Added Successfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                                    finish();
                                } else if (url.equals("https://admin.infinitecabs.com.au/payment_failed")) {
                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                                    sweetAlertDialog.setTitleText("Infinite Cabs");
                                    sweetAlertDialog.setContentText("Card details are invalid or expired. Please enter valid card details.");
                                    sweetAlertDialog.setConfirmText("OK");
                                    sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.girl));
                                    sweetAlertDialog.setCancelable(true);
                                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.cancel();
                                            dialog.cancel();
                                        }
                                    });
                                    sweetAlertDialog.show();
                                }

                            }
                        });

                        webView.setWebChromeClient(new WebChromeClient() {
                            @Override
                            public void onProgressChanged(WebView view, int newProgress) {
                                Log.d("sdsgd", String.valueOf(newProgress));
                                //put your code here if your want to show the progress with progressbar
                            }
                        });

//            private String loadUrl = "http://www.baidu.com";
                        webView.loadUrl(CARD_URL + user.getCardId());


                        dialog.show();



                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Insert_Card_Model> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                if (result.equals("card added")) {
                    CustomToast.makeText(getApplicationContext(), "Card Added Successfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    finish();
                } else {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                    sweetAlertDialog.setTitleText("Infinite Cabs");
                    sweetAlertDialog.setContentText("Card details are invalid or expired. Please enter valid card details.");
                    sweetAlertDialog.setConfirmText("OK");
                    sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.girl));
                    sweetAlertDialog.setCancelable(true);
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sweetAlertDialog.show();
//                    CustomToast.makeText(getApplicationContext(), "Card Error", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult
}
