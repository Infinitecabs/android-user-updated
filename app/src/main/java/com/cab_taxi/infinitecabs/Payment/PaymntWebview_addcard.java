package com.cab_taxi.infinitecabs.Payment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;

public class PaymntWebview_addcard extends AppCompatActivity {

    ProgressDialog progressDialog;
    Dialog dialog;
    NoInternetDialog noInternetDialog;
    private WebView webView;
    private ImageView iv_back;
    private RelativeLayout header;
    private RelativeLayout rlprogress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymnt_webview);
        noInternetDialog = new NoInternetDialog.Builder(PaymntWebview_addcard.this).build();

        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        rlprogress = findViewById(R.id.rlprogress);
        webView = findViewById(R.id.webView);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        header = (RelativeLayout) findViewById(R.id.header);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl(getIntent().getStringExtra("url"));

        if (getIntent().getStringExtra("url").equals("cash")) {
            webView.setVisibility(View.GONE);
            rlprogress.setVisibility(View.VISIBLE);
        } else {
            webView.setVisibility(View.VISIBLE);
            rlprogress.setVisibility(View.GONE);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    //return true load with system-default-browser or other browsers, false with your webView
                    return false;
                }

                @Override
                public void onLoadResource(WebView view, String url) {
                    super.onLoadResource(view, url);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    dialog = new Dialog(PaymntWebview_addcard.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_login);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                    Log.d("sdfasdfasd", url + "__");
                    if (url.contains("https://admin.infinitecabs.com.au/Cardsuccess")) //check if that's a url you want to load internally
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "card added");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else if (url.equals("https://admin.infinitecabs.com.au/payment_failed")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "not added");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }

                }
            });

            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    Log.d("sdsgd", String.valueOf(newProgress));
                    //put your code here if your want to show the progress with progressbar
                }
            });

//            private String loadUrl = "http://www.baidu.com";
            webView.loadUrl(getIntent().getStringExtra("url"));






         /*
            webView.setWebViewClient(new WebViewClient());
            webView.loadUrl(getIntent().getStringExtra("url"));
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView wView, String url) {
                    Log.d("vishnal", "jhgjhg"+url);
                    if (url.contains("http://admin.infinitecabs.com.au/texiapp-new/web_service/check/")) //check if that's a url you want to load internally
                    {
                        startActivity(new Intent(PaymntWebview.this, PaymenttSuccess.class).putExtra("tag", "1"));

                    } else if(url.equals("http://stageofproject.com/texiapp-new/web_service/payment_failed")){
                        startActivity(new Intent(PaymntWebview.this, PaymenttSuccess.class).putExtra("tag", "2"));
                    }
                    return false;
                }
            });*/
        }
//        Log.d("vishnal", getIntent().getStringExtra("url"));

//        startWebView("www.google.com");


    }


}
