package com.cab_taxi.infinitecabs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Model.User_Status_Background_Model;
import com.cab_taxi.infinitecabs.Model.User_Submit_Rating_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateDriver extends AppCompatActivity implements View.OnClickListener {

    Float ratingvalue;
    UserSessionManager session;
    HashMap<String, String> user;
    String driverId = "";
    String booking_id = "";
    private ImageView iv_back;
    private CircleImageView civ_driver;
    private TextView tv_driverRating;
    private TextView tv_name_driver;
    private TextView tv_cab_no;
    private TextView tv_cab_model;
    private TextView tv_fair;
    private TextView tv_paymentType;
    private RatingBar ratingbar;
    private EditText et_comment;
    private Button btn_submit;
    private Button btn_cancel;
    private RatingBar MyRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_driver);
        noInternetDialog = new NoInternetDialog.Builder(RateDriver.this).build();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }
    NoInternetDialog noInternetDialog;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    private void initView() {
        MyRating = findViewById(R.id.MyRating);
        MyRating.setIsIndicator(true);
        iv_back = findViewById(R.id.iv_back);
        civ_driver = findViewById(R.id.civ_driver);
        tv_driverRating = findViewById(R.id.tv_driverRating);
        tv_name_driver = findViewById(R.id.tv_name_driver);
        tv_cab_no = findViewById(R.id.tv_cab_no);
        tv_cab_model = findViewById(R.id.tv_cab_model);
        tv_fair = findViewById(R.id.tv_fair);
        tv_paymentType = findViewById(R.id.tv_paymentType);
        ratingbar = findViewById(R.id.ratingbar);
        et_comment = findViewById(R.id.et_comment);
        btn_submit = findViewById(R.id.btn_submit);
        btn_cancel = findViewById(R.id.btn_cancel);

        iv_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                Float ratingVal = rating;
                ratingvalue = ratingbar.getRating();
                //Toast.makeText(getApplicationContext(), " Ratings : " + String.valueOf(ratingVal) + "", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), " Ratings1 : " + ratingvalue + "", Toast.LENGTH_SHORT).show();
            }
        });


        Map<String, String> map = new HashMap<>();
        // map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        User_Status_Check_Background_Store(map);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                CustomToast.makeText(RateDriver.this, "Please Review the Driver", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();

                break;

            case R.id.btn_submit:
                submit();
                break;

            case R.id.btn_cancel:
                startActivity(new Intent(RateDriver.this, Dashboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

        }
    }

    // driver status in background API-------------------------
    private void User_Status_Check_Background_Store(Map<String, String> map) {
        Call<User_Status_Background_Model> call = Apis.getAPIService().getUserStatus(map);
        call.enqueue(new Callback<User_Status_Background_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Status_Background_Model> call, Response<User_Status_Background_Model> response) {
                // dialog.dismiss();
                final User_Status_Background_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        switch (user.getBookingData().getStatus()) {
                            case "Accepted":

                                break;
                            case "Completed":
                                tv_driverRating.setText(user.getBookingData().getDriverRating());
                                MyRating.setRating(Float.parseFloat(user.getBookingData().getDriverRating()));
                                tv_cab_no.setText(user.getBookingData().getDriverCar_No());
                                tv_name_driver.setText(user.getBookingData().getDriverName());
                                Log.d("adsad","https://admin.infinitecabs.com.au/driverimages/" + user.getBookingData().getDriverImage());
                                Log.d("adsad","https://admin.infinitecabs.com.au/car_image/" + user.getBookingData().getCarImage());
                                try {
                                    Picasso.get().load("https://admin.infinitecabs.com.au/driverimages/" + user.getBookingData().getDriverImage())
                                            .error(R.drawable.user)
                                            .priority(Picasso.Priority.HIGH)
                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                tv_driverRating.setText(user.getBookingData().getUserstarrating());
                                tv_fair.setText("$" + user.getBookingData().getFinalAmount());
                                tv_paymentType.setText(user.getBookingData().getPaymentType());
                             /*    tv_name_driver.setText("Jhon");
                                tv_cab_no.setText(*//*user.getBookingData().getDriverCar_No()*//* "Taxi ABCD1000");
                                tv_cab_model.setText(*//*user.getBookingData().getDriverCar_Model()*//*"Sedan");*/
                                driverId = user.getBookingData().getDriverId();
                                booking_id = user.getBookingData().getBookingId();
                                MyRating.setRating(Float.parseFloat(user.getBookingData().getUserstarrating()));
                                MyRating.setIsIndicator(true);
                        }
                    } else {
                    }
                }
            }

            @Override
            public void onFailure(Call<User_Status_Background_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }


    private void submit() {
        // validate
        String comment = et_comment.getText().toString().trim();
        if (TextUtils.isEmpty(comment)) {
            CustomToast.makeText(getApplicationContext(), "Enter Leave a Comment", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();

            return;
        }
        if (ratingbar.getRating() == 0.0) {
            CustomToast.makeText(getApplicationContext(), "Please Give a Rating", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
            return;
        }

//        Toast.makeText(this, "Thank you for using Infinity cabs. Hope you are doing well!", Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ratingvalue + "Leave a Comment", Toast.LENGTH_SHORT).show();
        Map<String, String> map = new HashMap<>();
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        map.put("driver_id", driverId);
        map.put("rating", ratingvalue + "");
        map.put("booking_id", booking_id + "");
        map.put("message", comment);
        Rating_Driver(map);


    }

    public void Rating_Driver(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(RateDriver.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<User_Submit_Rating_Model> call = Apis.getAPIService().submit_rating(map);

        call.enqueue(new Callback<User_Submit_Rating_Model>() {
            @Override
            public void onResponse(Call<User_Submit_Rating_Model> call, Response<User_Submit_Rating_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                User_Submit_Rating_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(RateDriver.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("Thank you for using Infinite Cabs");
                        sweetAlertDialog.setContentText("Your driver rating has successfully been received.");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                startActivity(new Intent(RateDriver.this, Dashboard.class));
                                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

                            }
                        });

                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }
                        /**/

//                         CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }

                } else {
                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<User_Submit_Rating_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        CustomToast.makeText(RateDriver.this, "Please Review the Driver", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
    }
}
