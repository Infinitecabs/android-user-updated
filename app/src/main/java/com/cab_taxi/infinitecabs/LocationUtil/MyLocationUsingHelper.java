package com.cab_taxi.infinitecabs.LocationUtil;

import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.cab_taxi.infinitecabs.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;


public class MyLocationUsingHelper extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback, View.OnClickListener {


    private Location mLastLocation;

    double latitude;
    double longitude;

    LocationHelper locationHelper;
    private ImageView iv_close;
    private Button btn_home;
    private Button btn_work;
    private Button btn_other;
    private EditText et_place_name;
    private Button btn_pickLocation;
    private EditText et_place_address;
    private CardView card_picklocation;
    private Button btn_savePlace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_address);
        initView();

        locationHelper = new LocationHelper(this);
        locationHelper.checkpermission();


        card_picklocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLastLocation = locationHelper.getLocation();

                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();

                } else {

                    if (btn_savePlace.isEnabled())
                        btn_savePlace.setEnabled(false);

                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }
            }
        });


        btn_savePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast("Proceed to the next step");
            }
        });

        // check availability of play services
        if (locationHelper.checkPlayServices()) {

            // Building the GoogleApi client
            locationHelper.buildGoogleApiClient();
        }

    }

    private void initView() {
        iv_close = (ImageView) findViewById(R.id.iv_close);
        btn_home = (Button) findViewById(R.id.btn_home);
        btn_work = (Button) findViewById(R.id.btn_work);
        btn_other = (Button) findViewById(R.id.btn_other);
        et_place_name = (EditText) findViewById(R.id.et_place_name);
        btn_pickLocation = (Button) findViewById(R.id.btn_pickLocation);

        btn_home.setOnClickListener(this);
        btn_work.setOnClickListener(this);
        btn_other.setOnClickListener(this);
        btn_pickLocation.setOnClickListener(this);
        et_place_address = (EditText) findViewById(R.id.et_place_address);
        et_place_address.setOnClickListener(this);
        card_picklocation = (CardView) findViewById(R.id.card_picklocation);
        card_picklocation.setOnClickListener(this);
        btn_savePlace = (Button) findViewById(R.id.btn_savePlace);
        btn_savePlace.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home:

                break;
            case R.id.btn_work:

                break;
            case R.id.btn_other:

                break;
            case R.id.btn_pickLocation:

                break;
            case R.id.btn_savePlace:
                break;
        }
    }



    private void submit() {
        // validate
        String address = et_place_address.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "Place address", Toast.LENGTH_SHORT).show();
            return;
        }

        // TODO validate success, do something


    }


    public void getAddress() {
        Address locationAddress;

        locationAddress = locationHelper.getAddress(latitude, longitude);

        if (locationAddress != null) {

            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();


            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;

                et_place_address.setText(currentLocation);
                et_place_address.setVisibility(View.VISIBLE);

                if (!btn_savePlace.isEnabled())
                    btn_savePlace.setEnabled(true);
            }

        } else
            showToast("Something went wrong");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        locationHelper.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onResume() {
        super.onResume();
        locationHelper.checkPlayServices();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("Connection failed:", " ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        mLastLocation = locationHelper.getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        locationHelper.connectApiClient();
    }


    // Permission check functions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


}
