package com.cab_taxi.infinitecabs.DrawerActivities.My_Trips;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Model.User_Cancel_Ride_Model;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.gms.maps.MapView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Cancelled extends Fragment implements View.OnClickListener {

    private Button btn_getStart;
    private RecyclerView recyclerCancel;
    UserSessionManager session;
    HashMap<String, String> user;
    CancelledAdapter cancelledAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    private List<User_Cancel_Ride_Model.Data> array_cancel_ride_list;
    public Cancelled() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cancelled, container, false);
        Window window = getActivity().getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.black_text_color));
        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        initView(view);
        return view;
    }

    private void initView(View view) {
        Log.d("asdfh","3");
        btn_getStart = view.findViewById(R.id.btn_getStart);
        recyclerCancel = view.findViewById(R.id.recyclerCancel);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        btn_getStart.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        //(email,password)
        //5
        map.put("user_id",  user.get(UserSessionManager.KEY_ID));
        Cancel_Ride(map);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorRed);
        swipeRefreshLayout.setSize(23);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Map<String, String> map = new HashMap<>();
                //(email,password)
                map.put("user_id",  user.get(UserSessionManager.KEY_ID));
                Cancel_Ride(map);
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }
    public class CancelledAdapter extends RecyclerView.Adapter<CancelledAdapter.MyViewHolder> {


        public CancelledAdapter() {
        }

        @Override
        public CancelledAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.cancel_cards, parent, false);
            return new CancelledAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final CancelledAdapter.MyViewHolder holder, final int i) {

            String oldFormat= "dd/MM/yyyy HH:mm";
            String newFormat= "dd/MM/yyyy";
            String newFormat2= "HH:mm";
            String mStringDate = "25-11-15 14:23:34";
            String formatedDate = "";
            String formatedDate2 = "";
            SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
            Date myDate = null;
            try {
                myDate = dateFormat.parse(array_cancel_ride_list.get(i).getBook_create_date_time());
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
            formatedDate = timeFormat.format(myDate);


            SimpleDateFormat dateFormat2 = new SimpleDateFormat(oldFormat);
            Date myDate2 = null;
            try {
                myDate2 = dateFormat2.parse(array_cancel_ride_list.get(i).getBook_create_date_time());
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat2 = new SimpleDateFormat(newFormat2);
            formatedDate2 = timeFormat2.format(myDate2);




            holder.tv_rideTime.setText("Date: "+formatedDate);
            holder.tv_rideTime_.setText("Time: "+formatedDate2);


            holder.tv_serviceType.setText("Booking ID: "+array_cancel_ride_list.get(i).getBooking_id());
            holder.tv_priceToday.setText("$0");
//            holder.tv_rideTime.setText("Date :"+array_cancel_ride_list.get(i).getBook_create_date_time());
            holder.drivername.setText("Driver Name: "+array_cancel_ride_list.get(i).getDriver_name());
            holder.tv_from.setText(""+array_cancel_ride_list.get(i).getPickup_area());
            holder.tv_to.setText(""+array_cancel_ride_list.get(i).getDrop_area());
            holder.cancelReason.setText("Cancelled By: "+array_cancel_ride_list.get(i).getStatus_code());
            holder.cancelby.setText("Reason: "+array_cancel_ride_list.get(i).getCancel());
//            holder.tv_date_time.setText("Date :"+array_complete_ride_list.get(i).getBook_create_date_time());
            holder.tv_previousRideCharge.setText("$"+"N/A");
            holder.tv_carCategory.setText("Cab Type: "+array_cancel_ride_list.get(i).getCar_type());
            holder.picktime.setText("Pick Date & Time: "+array_cancel_ride_list.get(i).getPickupDateTime());
            holder.dropime.setText("Drop Date & Time: "+array_cancel_ride_list.get(i).getDropTime());
            holder.carnumber.setText("Cab Number: "+array_cancel_ride_list.get(i).getDriver_car_number());
            holder.driverID.setText("Driver ID: "+array_cancel_ride_list.get(i).getDriver_id());
            holder.carduppor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.cardbottom.getVisibility()==View.VISIBLE){
                        holder.cardbottom.setVisibility(View.GONE);
                    }else{
                        holder.cardbottom.setVisibility(View.VISIBLE);
                              }

                }
            });

        }

        @Override
        public int getItemCount() {
            return array_cancel_ride_list.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            private CardView carduppor;
            private CardView cardbottom;
            private TextView tv_serviceType;
            private TextView tv_priceToday;
            private TextView tv_rideTime;
            private TextView tv_from;
            private TextView tv_to;
            private TextView tv_previousRideCharge;
            private TextView tv_date_time;
            private TextView cancelby;
            private TextView drivername;
            private TextView tv_carCategory;
            private TextView tv_rideTime_;
            private TextView cancelReason;
            private TextView dropime;
            private TextView picktime;
            private TextView carnumber;
            private TextView driverID;
            private Button ivdecline;
            private Button ivaccept;
            private MapView mMapView;

            public MyViewHolder(View v) {
                super(v);
                carduppor = v.findViewById(R.id.carduppor);
                tv_serviceType = v.findViewById(R.id.tv_serviceType);
                tv_priceToday = v.findViewById(R.id.tv_priceToday);
                mMapView = v.findViewById(R.id.mapView);
                tv_rideTime = v.findViewById(R.id.tv_rideTime);
                tv_date_time = v.findViewById(R.id.tv_rideTime);
                drivername = v.findViewById(R.id.drivername);
                cancelReason = v.findViewById(R.id.cancelReason);
                cancelby = v.findViewById(R.id.cancelby);
                tv_to = v.findViewById(R.id.tv_to);
                picktime = v.findViewById(R.id.picktime);
                dropime = v.findViewById(R.id.dropime);
                tv_rideTime = v.findViewById(R.id.tv_rideTime);
                tv_carCategory = v.findViewById(R.id.tv_carCategory);
                tv_previousRideCharge = v.findViewById(R.id.tv_previousRideCharge);
                tv_from = v.findViewById(R.id.tv_from);
                tv_rideTime_ = v.findViewById(R.id.tv_rideTime_);
                cardbottom = v.findViewById(R.id.cardbottom);
                carnumber = v.findViewById(R.id.carnumber);
                driverID = v.findViewById(R.id.driverID);
            }
        }

    }

    private void Cancel_Ride(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        Call<User_Cancel_Ride_Model> call = Apis.getAPIService().getCancelRide(map);
        call.enqueue(new Callback<User_Cancel_Ride_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Cancel_Ride_Model> call, Response<User_Cancel_Ride_Model> response) {
                dialog.dismiss();
                User_Cancel_Ride_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()){
                            swipeRefreshLayout.setVisibility(View.GONE);
                        }else {
                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                            array_cancel_ride_list = new ArrayList<>();
                            array_cancel_ride_list.addAll(userdata.getData());
                            cancelledAdapter = new CancelledAdapter();
                            recyclerCancel.setAdapter(cancelledAdapter);
                            cancelledAdapter.notifyDataSetChanged();
                        }

                    } else {
                        swipeRefreshLayout.setVisibility(View.GONE);
                        recyclerCancel.setVisibility(View.GONE);
                       // CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<User_Cancel_Ride_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_getStart:
                startActivity(new Intent(getActivity(), Dashboard.class));
                break;


        }
    }


    @Override
    public void onResume() {
        super.onResume();

        Map<String, String> map = new HashMap<>();
        //(email,password)
        map.put("user_id",  user.get(UserSessionManager.KEY_ID));
        Cancel_Ride(map);
    }
}
