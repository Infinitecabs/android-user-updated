package com.cab_taxi.infinitecabs.DrawerActivities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Model.PromoCodeModel;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferScreen extends AppCompatActivity {

    TextView no_data_found;
    List<PromoCodeModel.Data> prmocodelist;
    NoInternetDialog noInternetDialog;
    private ImageView iv_back;
    private CardView header1;
    private LinearLayout llerror;
    private RecyclerView recyclerCancel;
    private SwipeRefreshLayout swipeToRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_screen);
        noInternetDialog = new NoInternetDialog.Builder(OfferScreen.this).build();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        llerror = findViewById(R.id.llerror);
        header1 = (CardView) findViewById(R.id.header1);
        no_data_found = findViewById(R.id.no_data_found);
        recyclerCancel = (RecyclerView) findViewById(R.id.recyclerCancel);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setSize(23);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                OfferApi();
                swipeToRefresh.setRefreshing(false);

            }
        });
//        recyclerCancel.setAdapter(new OfferdAdapter());
        OfferApi();
    }

    private void OfferApi() {
        final Dialog dialog = new Dialog(OfferScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<PromoCodeModel> call = Apis.getAPIService().promocodeList();
        call.enqueue(new Callback<PromoCodeModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<PromoCodeModel> call, Response<PromoCodeModel> response) {
                dialog.dismiss();
                PromoCodeModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()) {
                            no_data_found.setVisibility(View.VISIBLE);
                            llerror.setVisibility(View.VISIBLE);
                            no_data_found.setText(userdata.getMessage());
                            swipeToRefresh.setVisibility(View.GONE);
                        } else {
                            llerror.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.GONE);
                            swipeToRefresh.setVisibility(View.VISIBLE);
                            prmocodelist = new ArrayList<>();
                            prmocodelist.addAll(userdata.getData());
                            OfferdAdapter completedAdapter = new OfferdAdapter();
                            recyclerCancel.setAdapter(completedAdapter);
                            completedAdapter.notifyDataSetChanged();
                        }
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        llerror.setVisibility(View.VISIBLE);
                        no_data_found.setText(userdata.getMessage());
                        swipeToRefresh.setVisibility(View.GONE);
                        //   CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PromoCodeModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class OfferdAdapter extends RecyclerView.Adapter<OfferdAdapter.MyViewHolder> {

        public OfferdAdapter() {
        }

        @Override
        public OfferdAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(OfferScreen.this).inflate(R.layout.offer_cards, parent, false);
            return new OfferdAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OfferdAdapter.MyViewHolder holder, final int i) {
            holder.tv_serviceType.setText(prmocodelist.get(i).getPromocode());
            holder.tv_rideTime.setText(prmocodelist.get(i).getDescription());
        }

        @Override
        public int getItemCount() {
            return prmocodelist.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            private final TextView tv_serviceType;
            private final TextView tv_rideTime;

            public MyViewHolder(View v) {
                super(v);
                tv_serviceType = v.findViewById(R.id.tv_serviceType);
                tv_rideTime = v.findViewById(R.id.tv_rideTime);
            }
        }
    }
}
