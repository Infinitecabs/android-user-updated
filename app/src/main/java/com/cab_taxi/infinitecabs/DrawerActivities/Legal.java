package com.cab_taxi.infinitecabs.DrawerActivities;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.DrawerActivities.Setting.AboutUs;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.PrivacyPolicy;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.Terms_and_conditions;
import com.cab_taxi.infinitecabs.R;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class Legal extends AppCompatActivity implements View.OnClickListener {
    WebView webView;
    private ImageView iv_back;
    Dialog dialog;
    private TextView tv_aboutUs;
    private TextView tv_version;
    private TextView tv_privacy;
    private TextView tv_terms;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_text_color));
        iv_back = (ImageView) findViewById(R.id.iv_back);
        webView = findViewById(R.id.wevView);
      /*  webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl("https://infinitecabs.com.au/index.php");*/

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });


        tv_privacy = (TextView) findViewById(R.id.tv_privacy);
        tv_privacy.setOnClickListener(this);
        tv_aboutUs = (TextView) findViewById(R.id.tv_aboutUs);
        tv_aboutUs.setOnClickListener(this);
        tv_terms = (TextView) findViewById(R.id.tv_terms);
        tv_terms.setOnClickListener(this);
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            dialog = new Dialog(Legal.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_login);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(dialog!=null){
                dialog.dismiss();
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_aboutUs:
                startActivity(new Intent(this, AboutUs.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.tv_terms:
                startActivity(new Intent(this, Terms_and_conditions.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.tv_privacy:
                startActivity(new Intent(this, PrivacyPolicy.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

        }
    }
}
