package com.cab_taxi.infinitecabs.DrawerActivities.Setting;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Model.Password_Change_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    HashMap<String, String> user;
    NoInternetDialog noInternetDialog;
    private ImageView iv_back;
    private EditText et_password;
    private EditText et_confirmPassword;
    private Button btn_submit;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        noInternetDialog = new NoInternetDialog.Builder(ChangePassword.this).build();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        et_password = findViewById(R.id.et_password);
        et_confirmPassword = findViewById(R.id.et_confirmPassword);
        btn_submit = findViewById(R.id.btn_submit);

        iv_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.btn_submit:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            CustomToast.makeText(getApplicationContext(), "Please enter Password", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }

        String confirmPassword = et_confirmPassword.getText().toString().trim();
        if (TextUtils.isEmpty(confirmPassword)) {
            CustomToast.makeText(getApplicationContext(), "Enter Confirm Password", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!password.equals(confirmPassword)) {
            CustomToast.makeText(getApplicationContext(), "Confirm Password do not match?", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("uid", user.get(UserSessionManager.KEY_ID));
        map.put("password", password);
        Change_Password(map);
    }

    public void Change_Password(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(ChangePassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Password_Change_Model> call = Apis.getAPIService().changepassword(map);

        call.enqueue(new Callback<Password_Change_Model>() {
            @Override
            public void onResponse(Call<Password_Change_Model> call, Response<Password_Change_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Password_Change_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ChangePassword.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("Thank You.");
                        sweetAlertDialog.setContentText("Your password has successfully changed.");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                finish();
                            }
                        });

                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }

                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                    }

                } else {
                    CustomToast.makeText(getApplicationContext(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                }
            }

            @Override
            public void onFailure(Call<Password_Change_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
}
