package com.cab_taxi.infinitecabs.DrawerActivities.Setting;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.LoginSignUp.SignUp;
import com.cab_taxi.infinitecabs.Model.Profile_Model;
import com.cab_taxi.infinitecabs.Model.User_EditProfile_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    private static final String IMAGE_DIRECTORY = "/InfiniteCabs App Camara Image";
    private static final int gallery = 2;
    String abc = "";
    UserSessionManager session;
    HashMap<String, String> user;
    File file;
    String picturePath;
    NoInternetDialog noInternetDialog;
    Bitmap rotatedBitmap;
    private ImageView iv_close;
    private TextView tv_name_profile;
    private CircleImageView civProfileimage;
    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_email;
    private ImageView iv_checkMark;
    private EditText et_phoneNo;
    private Button btn_save, change_password_layout;
    private String device_id;
    CountryCodePicker countryCodePicker;
    public static Bitmap decodeFile(String path) { // this method is for avoiding the image rotation
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 4;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;
            ExifInterface exif = new ExifInterface(path);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();
            if ((orientation == 3)) {
                m.postRotate(180);
                m.postScale((float) bm.getWidth(), (float) bm.getHeight());
//               if(m.preRotate(90)){
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == 6) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == 8) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
        }
        return null;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(EditProfile.this).build();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_close = findViewById(R.id.iv_close);
        tv_name_profile = findViewById(R.id.tv_name_profile);
        civProfileimage = findViewById(R.id.civProfileimage);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_email = findViewById(R.id.et_email);
        change_password_layout = findViewById(R.id.change_password_layout);
        iv_checkMark = findViewById(R.id.iv_checkMark);
        et_phoneNo = findViewById(R.id.et_phoneNo);
        countryCodePicker = findViewById(R.id.ccp);
        btn_save = findViewById(R.id.btn_save);
        iv_close.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        civProfileimage.setOnClickListener(this);
        change_password_layout.setOnClickListener(this);
        et_email.setEnabled(false);
        et_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    iv_checkMark.setVisibility(View.VISIBLE);
                } else {
                    iv_checkMark.setVisibility(View.GONE);
                }
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        GetProfileInfo(map);
        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onBackPressed() {
        Intent d = new Intent(this, Dashboard.class);
        startActivity(d);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_close:


                if (getIntent().getStringExtra("tag").equals("1")) {
                    onBackPressed();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    onBackPressed();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
                break;

            case R.id.btn_save:

                submit();
                break;
            case R.id.change_password_layout:
                startActivity(new Intent(this, ChangePassword.class));
                break;

            case R.id.civProfileimage:
                String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                Permissions.check(this/*context*/, permissions, null/*rationale*/, null/*options*/, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        View view = getLayoutInflater().inflate(R.layout.select_custom_gallery, null);
                        LinearLayout gallery = view.findViewById(R.id.gallery);
                        LinearLayout camara = view.findViewById(R.id.camara);
                        final BottomSheetDialog dialog = new BottomSheetDialog(EditProfile.this);
                        dialog.setContentView(view);
                        dialog.show();
                        gallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, 2);
                                dialog.dismiss();
                            }
                        });
                        camara.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent1, 1);
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                        /*Log.d("asdfasdf", "======____");
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1);*/
                    }


                });

         /*       Dexter.withContext(this)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Log.d("asdfasdf", "________________");
                            View view = getLayoutInflater().inflate(R.layout.select_custom_gallery, null);
                            LinearLayout gallery = view.findViewById(R.id.gallery);
                            LinearLayout camara = view.findViewById(R.id.camara);
                            final BottomSheetDialog dialog = new BottomSheetDialog(EditProfile.this);
                            dialog.setContentView(view);
                            dialog.show();
                            gallery.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(intent, 2);
                                    dialog.dismiss();
                                }
                            });
                            camara.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent1, 1);
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            if (report.isAnyPermissionPermanentlyDenied()){
                                Log.d("asdfasdf", "======____");
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, 1);
                            }

                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        Log.d("asdfasdf", "____+++++++________======____");
                        Utility.checkAndRequestPermissions(EditProfile.this);
                    }
                }).check();

*/
                break;
        }
    }

    /* Camara and Gallery get a image path*/
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CAMERA:
                if (resultCode == RESULT_OK) {
                    //use imageUri here to access the image
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    picturePath = saveImage(thumbnail);
                    Bitmap newBitmap = decodeFile(picturePath);
                    picturePath = saveImage(newBitmap);
                    try {
                        rotatedBitmap = getRotateImage(picturePath, thumbnail);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //image rotation code........
                    civProfileimage.setImageBitmap(rotatedBitmap);


                    //civProfileimage.setImageBitmap(newBitmap);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
                }
                break;
            case gallery:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    picturePath = c.getString(columnIndex);

                    c.close();
                    Bitmap thumbnail = BitmapFactory.decodeFile(picturePath);
                    Bitmap bitmaop = uriToBitmap(selectedImage);
                    File imgFile = new  File(picturePath);
                    Bitmap myBitmap = decodeFile(imgFile.getAbsolutePath());
                    //Drawable d = new BitmapDrawable(getResources(), myBitmap);


                    Log.w("PATH", picturePath + "");
                    try {
                        rotatedBitmap =    getRotateImage(picturePath, myBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //image rotation code........
                    civProfileimage.setImageBitmap(rotatedBitmap);
                    //civProfileimage.setImageBitmap(thumbnail);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
                }
                break;
        }


//        case Gallery:
//        if (resultCode == 1) {
//            return;
//        }
//        if (requestCode == CAMERA) {
//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            picturePath = saveImage(thumbnail);
//            civProfileimage.setImageBitmap(thumbnail);
//
//        } else if (requestCode == 2) {
//            Uri selectedImage = data.getData();
//            String[] filePath = {MediaStore.Images.Media.DATA};
//            Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
//            c.moveToFirst();
//            int columnIndex = c.getColumnIndex(filePath[0]);
//            picturePath = c.getString(columnIndex);
//            c.close();
//            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
//            Log.w("PATH", picturePath + "");
//            civProfileimage.setImageBitmap(thumbnail);
//        } else if (resultCode == RESULT_CANCELED) {
//            Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Picture was not taken", Toast.LENGTH_SHORT).show();
//        }
    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
        Bitmap image = null;
        try {
            ParcelFileDescriptor parcelFileDescriptor =
                    getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            image = BitmapFactory.decodeFileDescriptor(fileDescriptor);

            parcelFileDescriptor.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }


    public void GetProfileInfo(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(EditProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Profile_Model> call = Apis.getAPIService().getUserProfile(map);

        call.enqueue(new Callback<Profile_Model>() {
            @Override
            public void onResponse(Call<Profile_Model> call, Response<Profile_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Profile_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        tv_name_profile.setText("Hey " + user.getData().getFirst_name() + "!");
                        et_last_name.setText(user.getData().getLast_name());
                        et_first_name.setText(user.getData().getFirst_name());
                        et_email.setText(user.getData().getEmail() + " ");
                        et_phoneNo.setText(user.getData().getMobile() + " ");
                        if (!user.getData().getPhoneCode().isEmpty()) {
                            countryCodePicker.setCountryForPhoneCode(Integer.parseInt(user.getData().getPhoneCode()));
                        }
                        Utility.setdetail(EditProfile.this, user.getData().getImage());
                        Utility.setCountryCode(EditProfile.this, user.getData().getPhoneCode());
                        /*Picasso.get().load(user.getData().getImagepath() + user.getData().getImage())
                                .error(R.drawable.user)
                                .priority(Picasso.Priority.HIGH)
                                .networkPolicy(NetworkPolicy.NO_CACHE).into(civProfileimage);*/
                        RequestOptions options = new RequestOptions()
                                .placeholder(R.mipmap.ic_launcher_round)
                                .error(R.drawable.no_image);


                        Glide.with(EditProfile.this).load("https://admin.infinitecabs.com.au/user_image/" + Utility.getProfilePic(EditProfile.this)).apply(options).into(civProfileimage);
//                        et_Password.setText(user.getData().ge() + " " );
                    } else {
                        showmessage(user.getMessage());
                    }

                } else {

                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<Profile_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public Bitmap getRotateImage(String photoPath, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }

        return rotatedBitmap;

    }


    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    private void submit() {
        // validate
        String firstname = et_first_name.getText().toString().trim();
        if (TextUtils.isEmpty(firstname)) {
            Toast.makeText(this, "Name", Toast.LENGTH_SHORT).show();

            CustomToast.makeText(getApplicationContext(), "Please Enter First Name", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        String lastname = et_last_name.getText().toString().trim();
        if (TextUtils.isEmpty(lastname)) {
            Toast.makeText(this, "Last Name", Toast.LENGTH_SHORT).show();

            CustomToast.makeText(getApplicationContext(), "Please Enter Last Name", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        String emailString = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(emailString)) {
            Toast.makeText(this, "Email", Toast.LENGTH_SHORT).show();
            CustomToast.makeText(getApplicationContext(), "Please enter Email", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!Utility.isValidEmailId(emailString)) {
            CustomToast.makeText(getApplicationContext(), "Please enter Valid Email", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        String mobileString = et_phoneNo.getText().toString().trim();
        if (TextUtils.isEmpty(mobileString)) {
            CustomToast.makeText(getApplicationContext(), "Please Enter Mobile Number", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
            return;
        }
        if (!Utility.isValidMobile(mobileString)) {
            CustomToast.makeText(getApplicationContext(), "Please enter valid mobile no.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
            return;
        }
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("uid", user.get(UserSessionManager.KEY_ID));
        hashmap.put("first_name", firstname);
        hashmap.put("last_name", lastname);
        hashmap.put("email", emailString);
        hashmap.put("mobile", mobileString);
        hashmap.put("isdevice", "android");
        hashmap.put("phoneCode", countryCodePicker.getSelectedCountryCodeWithPlus());
        hashmap.put("country", countryCodePicker.getSelectedCountryEnglishName());
        if (picturePath != null) {
            hashmap.put("image", picturePath);
        } else {
            hashmap.put("image", "");
        }
        Update_Profiles(hashmap);

    }


    private void Update_Profiles(HashMap<String, String> datapart) {
        final Dialog dialog = new Dialog(EditProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        file = new File(datapart.get("image"));
        // Map is used to multipart the file using okhttp3.RequestBody

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part user_id = MultipartBody.Part.createFormData("uid", Objects.requireNonNull(datapart.get("uid")));
        MultipartBody.Part first_name = MultipartBody.Part.createFormData("first_name", Objects.requireNonNull(datapart.get("first_name")));
        MultipartBody.Part last_name = MultipartBody.Part.createFormData("last_name", Objects.requireNonNull(datapart.get("last_name")));
        MultipartBody.Part user_email = MultipartBody.Part.createFormData("email", Objects.requireNonNull(datapart.get("email")));
        MultipartBody.Part user_mobile = MultipartBody.Part.createFormData("mobile", Objects.requireNonNull(datapart.get("mobile")));
        MultipartBody.Part phoneCode = MultipartBody.Part.createFormData("phoneCode", Objects.requireNonNull(datapart.get("phoneCode")));
        MultipartBody.Part country = MultipartBody.Part.createFormData("country", Objects.requireNonNull(datapart.get("country")));
        MultipartBody.Part isdevice = MultipartBody.Part.createFormData("isdevice", Objects.requireNonNull(datapart.get("isdevice")));
        MultipartBody.Part profile_photo;
        Log.d("adsfasdf",picturePath+"___");
        if (picturePath != null && !picturePath.isEmpty()) {
            profile_photo = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        } else {
            profile_photo = MultipartBody.Part.createFormData("image", "");
        }

        RequestBody.create(MediaType.parse("text/plain"), file.getName());
        // Parsing any Media type file
        Call<User_EditProfile_Model> call = Apis.getAPIService().Update_profile(user_id, first_name, last_name, user_email, user_mobile, phoneCode,country, profile_photo, isdevice);
        call.enqueue(new Callback<User_EditProfile_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_EditProfile_Model> call, Response<User_EditProfile_Model> response) {
                dialog.dismiss();
                User_EditProfile_Model user1 = response.body();
                if (user1.getStatusCode().equals("200")) {
                    session.iseditor();
                    Utility.setdetail(EditProfile.this, user1.getData().getImage());
                    Utility.setCountryCode(EditProfile.this, user1.getData().getPhoneCode());
                    session.createUserLoginSession(String.valueOf(user1.getData().getId()),
                            user1.getData().getFirst_name(), user1.getData().getLast_name(),
                            user1.getData().getEmail(), user1.getData().getMobile(), "", user1.getData().getImage(), "");
                    session.setLogin();
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(EditProfile.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                    sweetAlertDialog.setTitleText("Thank You.");
                    sweetAlertDialog.setContentText("Your profile has successfully updated.");
                    sweetAlertDialog.setConfirmText("OK");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                    sweetAlertDialog.showCancelButton(false);
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sweetAlertDialog.cancel();
                          /*  Map<String, String> map = new HashMap<>();
                            map.put("user_id", user.get(UserSessionManager.KEY_ID));
                            GetProfileInfo(map);*/
                            finish();
                        }
                    });

                    if (sweetAlertDialog.isShowing()) {

                    } else {
                        sweetAlertDialog.show();
                    }
//                    CustomToast.makeText(getApplicationContext(), user1.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    // startActivity(new Intent(Edit_Profile.this, My_Profile.class));
                }

                else if (user1.getStatusCode().equals("201")) {
                    startActivity(new Intent(EditProfile.this, Verification_mobile.class).putExtra("mobile", (user1.getPhone())).putExtra("phoneCode", (user1.getPhoneCode())));
                    //   CustomToast.makeText(getApplicationContext(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING,true).show();
                }
                else if (user1.getStatusCode().equals("401")) {
                       CustomToast.makeText(getApplicationContext(), user1.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING,true).show();
                }
            }

            @Override
            public void onFailure(Call<User_EditProfile_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });

    }
}