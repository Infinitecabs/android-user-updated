package com.cab_taxi.infinitecabs.DrawerActivities.My_Trips;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Model.User_Complete_Ride_Model;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.gms.maps.MapView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CompletedFragment extends Fragment  {


    private RecyclerView recyclerCompeletd;
    UserSessionManager session;
    HashMap<String, String> user;
    CompletedAdapter completedAdapter;
    AppCompatTextView no_data_found;
    SwipeRefreshLayout swipeRefreshLayout;
    String pickuplat,picklong,droplat,droplong;
    private List<User_Complete_Ride_Model.Data> array_complete_ride_list;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed, container, false);
        Window window = getActivity().getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.black_text_color));

        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        Log.d("asdfh","2");
        initView(view);

        return view;
    }

    private void initView(View view) {
        recyclerCompeletd = view.findViewById(R.id.recyclerCompeletd);
        no_data_found = view.findViewById(R.id.no_data_found);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);

        Map<String, String> map = new HashMap<>();
        map.put("user_id",  user.get(UserSessionManager.KEY_ID));
        Complete_Ride(map);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorRed);
        swipeRefreshLayout.setSize(23);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Map<String, String> map = new HashMap<>();
                //(email,password)
                map.put("user_id",  user.get(UserSessionManager.KEY_ID));
                Complete_Ride(map);
                swipeRefreshLayout.setRefreshing(false);

            }
        });

    }
    public class CompletedAdapter extends RecyclerView.Adapter<CompletedAdapter.MyViewHolder> {


        public CompletedAdapter() {
        }

        @Override
        public CompletedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.completed_cards, parent, false);
            return new CompletedAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final CompletedAdapter.MyViewHolder holder, final int i) {

            String oldFormat= "dd/MM/yyyy HH:mm";
            String newFormat= "dd/MM/yyyy";
            String newFormat2= "HH:mm";
            String mStringDate = "25-11-15 14:23";
            String formatedDate = "";
            String formatedDate2 = "";
            SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
            Date myDate = null;
            try {
                myDate = dateFormat.parse(array_complete_ride_list.get(i).getBook_create_date_time());
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
            formatedDate = timeFormat.format(myDate);


            SimpleDateFormat dateFormat2 = new SimpleDateFormat(oldFormat);
            Date myDate2 = null;
            try {
                myDate2 = dateFormat2.parse(array_complete_ride_list.get(i).getBook_create_date_time());
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat2 = new SimpleDateFormat(newFormat2);
            formatedDate2 = timeFormat2.format(myDate2);

            holder.tv_rideTime.setText("Date: "+formatedDate);
            holder.tv_rideTime_.setText("Time: "+formatedDate2);
            holder.tv_serviceType.setText("Booking ID: "+array_complete_ride_list.get(i).getBooking_id());
            holder.tv_priceToday.setText("$"+array_complete_ride_list.get(i).getAmount());
//            holder.tv_rideTime.setText("Date :"+array_complete_ride_list.get(i).getBook_create_date_time());
            holder.drivername.setText("Driver Name: "+array_complete_ride_list.get(i).getDriver_name());
            holder.tv_from.setText(""+array_complete_ride_list.get(i).getPick_address());
            holder.tv_to.setText(""+array_complete_ride_list.get(i).getDrop_area());
//            getvide(holder.picktime,array_complete_ride_list.get(i).getPickupDateTime());
//            getvide2(holder.dropime,array_complete_ride_list.get(i).getDropTime());
//            holder.tv_date_time.setText("Date :"+array_complete_ride_list.get(i).getBook_create_date_time());
            holder.picktime.setText("Pick Up Date & Time: "+array_complete_ride_list.get(i).getPickupDateTime());
            holder.dropime.setText("Drop Off Date & Time: "+array_complete_ride_list.get(i).getDropTime());
            holder.tv_previousRideCharge.setText("$"+array_complete_ride_list.get(i).getAmount());
            holder.tv_carCategory.setText("Cab Type: "+array_complete_ride_list.get(i).getCar_type());
            holder.carnumber.setText("Cab Number: "+array_complete_ride_list.get(i).getDriver_car_number());
            holder.driverID.setText("Driver ID: "+array_complete_ride_list.get(i).getDriver_id());
/*

            pickuplat=array_complete_ride_list.get(i).getUser_drop_latitude();
            picklong=array_complete_ride_list.get(i).getUser_drop_long_latitude();
            droplat= array_complete_ride_list.get(i).getUser_pickup_latitude();
            droplong= array_complete_ride_list.get(i).getUser_pickup_long_latitude();*/
            /*holder.mMapView.getMapAsync(new OnMapReadyCallback()
                                        {
                                            @Override
                                            public void onMapReady(GoogleMap googleMap) {
                                                MapsInitializer.initialize(getActivity());
                                            }
                                        });*/
            holder.carduppor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.cardbottom.getVisibility()==View.VISIBLE){
                        holder.cardbottom.setVisibility(View.GONE);
                    }else{
                        holder.cardbottom.setVisibility(View.VISIBLE);

                    }

                    }
            });

        }

        @Override
        public int getItemCount() {
            return array_complete_ride_list.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            private CardView carduppor;
            private CardView cardbottom;
            private TextView tv_serviceType;
            private TextView tv_priceToday;
            private TextView tv_rideTime;
            private TextView tv_from;
            private TextView tv_to;
            private TextView tv_previousRideCharge;
            private TextView tv_date_time;
            private TextView drivername;
            private TextView tv_carCategory;
            private Button ivdecline;
            private Button ivaccept;
            private TextView dropime;
            private TextView tv_rideTime_;
            private TextView picktime;
            private TextView carnumber;
            private TextView driverID;
            private MapView mMapView;

            public MyViewHolder(View v) {
                super(v);
                carduppor = v.findViewById(R.id.carduppor);
                tv_serviceType = v.findViewById(R.id.tv_serviceType);
                tv_priceToday = v.findViewById(R.id.tv_priceToday);
                driverID = v.findViewById(R.id.driverID);
                mMapView = v.findViewById(R.id.mapView);
                tv_rideTime = v.findViewById(R.id.tv_rideTime);
                tv_date_time = v.findViewById(R.id.tv_date_time);
                drivername = v.findViewById(R.id.drivername);
                tv_to = v.findViewById(R.id.tv_to);
                tv_carCategory = v.findViewById(R.id.tv_carCategory);
                picktime = v.findViewById(R.id.picktime);
                dropime = v.findViewById(R.id.dropime);
                tv_rideTime_ = v.findViewById(R.id.tv_rideTime_);
                tv_previousRideCharge = v.findViewById(R.id.tv_previousRideCharge);
                tv_from = v.findViewById(R.id.tv_from);
                cardbottom = v.findViewById(R.id.cardbottom);
                carnumber = v.findViewById(R.id.carnumber);


            }
        }

    }


    private void Complete_Ride(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<User_Complete_Ride_Model> call = Apis.getAPIService().getCompleteRide(map);
        call.enqueue(new Callback<User_Complete_Ride_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Complete_Ride_Model> call, Response<User_Complete_Ride_Model> response) {
                dialog.dismiss();
                User_Complete_Ride_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()){
                            no_data_found.setVisibility(View.VISIBLE);
                            no_data_found.setText(userdata.getMessage());
                            swipeRefreshLayout.setVisibility(View.GONE);
                        }else {
                            no_data_found.setVisibility(View.GONE);
                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                            array_complete_ride_list = new ArrayList<>();
                            array_complete_ride_list.addAll(userdata.getData());
                            completedAdapter = new CompletedAdapter();
                            recyclerCompeletd.setAdapter(completedAdapter);
                            completedAdapter.notifyDataSetChanged();
                        }
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        no_data_found.setText(userdata.getMessage());
                        swipeRefreshLayout.setVisibility(View.GONE);
                     //   CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<User_Complete_Ride_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        map.put("user_id",  user.get(UserSessionManager.KEY_ID));
        Complete_Ride(map);
    }

    public void getvide(TextView textView,String string){
        String oldFormat= "dd/MM/yyyy HH:mm:ss";
        String newFormat= "dd/MM/yyyy HH:mm";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(string);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
        formatedDate = timeFormat.format(myDate);
        textView.setText("Pick Date/Time: "+formatedDate);

    }
    public void getvide2(TextView textView,String string){
        String oldFormat= "dd/MM/yyyy HH:mm:ss";
        String newFormat= "dd/MM/yyyy HH:mm";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(string);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
        formatedDate = timeFormat.format(myDate);
        textView.setText("Drop Date/Time: "+formatedDate);

    }
}