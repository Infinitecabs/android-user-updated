package com.cab_taxi.infinitecabs.DrawerActivities.Setting;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.LoginSignUp.SignUp;
import com.cab_taxi.infinitecabs.Model.OtpVerifiedModel;
import com.cab_taxi.infinitecabs.Model.ResendOtpModel;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.mukesh.OtpView;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
signup_verification.setText("Check your SMS message. We've sent you the pin at"+ "956149514");
*/
public class Verification_mobile extends AppCompatActivity implements View.OnClickListener {
    UserSessionManager session;
    HashMap<String, String> user;
    private ImageView iv_back;
    private TextView tv_signup;
    private TextView tv_checkOTP;
    private OtpView otpView;
    private TextView tv_resendCode;
    private Button btn_verify;
    String UserId = "";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        noInternetDialog = new NoInternetDialog.Builder(Verification_mobile.this).build();
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();
    }
    NoInternetDialog noInternetDialog;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_signup = findViewById(R.id.tv_signup);
        tv_signup.setVisibility(View.GONE);
        tv_checkOTP = findViewById(R.id.tv_checkOTP);
        otpView = findViewById(R.id.otpView);
        tv_resendCode = findViewById(R.id.tv_resendCode);
        btn_verify = findViewById(R.id.btn_verify);

        iv_back.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_resendCode.setOnClickListener(this);
        btn_verify.setOnClickListener(this);

            Map<String, String> map = new HashMap<>();
            map.put("userId", user.get(UserSessionManager.KEY_ID));
            map.put("phone", getIntent().getStringExtra("mobile"));
            map.put("phoneCode", getIntent().getStringExtra("phoneCode"));
            ResendOtp(map);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_signup:
                startActivity(new Intent(Verification_mobile.this, SignUp.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.tv_resendCode:
                Map<String, String> map = new HashMap<>();
                map.put("userId", user.get(UserSessionManager.KEY_ID));
                map.put("phone", getIntent().getStringExtra("mobile"));
                map.put("phoneCode", getIntent().getStringExtra("phoneCode"));
                ResendOtp(map);
                break;

            case R.id.btn_verify:
                submit();
                break;
        }
    }

    public void submit(){
        String otp = otpView.getText().toString().trim();
        if (TextUtils.isEmpty(otp)) {
            Toast.makeText(this, "Enter OTP", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("phone", getIntent().getStringExtra("mobile"));
        map.put("phoneCode", getIntent().getStringExtra("phoneCode"));
        map.put("otp", otp);
        verifyOtp(map);
    }

    public void ResendOtp(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(Verification_mobile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ResendOtpModel> call = Apis.getAPIService().ResendOtpforPhone(map);
        call.enqueue(new Callback<ResendOtpModel>() {
            @Override
            public void onResponse(Call<ResendOtpModel> call, Response<ResendOtpModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                ResendOtpModel user = response.body();
                if (user.getStatusCode().equals("200")) {
                 CustomToast.makeText(getApplicationContext(), "New code has been sent successfully on you mobile number.", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS,true).show();
                } else {
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR,true).show();
                }
            }

            @Override
            public void onFailure(Call<ResendOtpModel> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void verifyOtp(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(Verification_mobile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<OtpVerifiedModel> call = Apis.getAPIService().verifyOtpForPhone(map);
        call.enqueue(new Callback<OtpVerifiedModel>() {
            @Override
            public void onResponse(Call<OtpVerifiedModel> call, Response<OtpVerifiedModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                OtpVerifiedModel user = response.body();
                if (user.getStatusCode().equals("200")) {
                    Utility.setCountryCode(Verification_mobile.this,getIntent().getStringExtra("phoneCode"));
//                    session.iseditor();
                   /* session.createUserLoginSession(String.valueOf(user.getData().getId()),
                            user.getData().getFirst_name(), user.getData().getLast_name(),
                            user.getData().getEmail(), user.getData().getMobile(),
                            user.getData().getUser_status(), user.getData().getImage(), user.getData().getNotification());
*/
                   finish();
                } else {
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<OtpVerifiedModel> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
}
