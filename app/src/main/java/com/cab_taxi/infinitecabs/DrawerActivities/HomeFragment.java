package com.cab_taxi.infinitecabs.DrawerActivities;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Background_Service.AppLocationService;
import com.cab_taxi.infinitecabs.Background_Service.StatusManager_Background_Store_Data;
import com.cab_taxi.infinitecabs.CancleRideReason;
import com.cab_taxi.infinitecabs.Circular_Image_custom.CircularImageView;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Extra.CarMoveAnim;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.LocationUtil.PermissionUtils;
import com.cab_taxi.infinitecabs.Model.After_Accept_Driver_Track_User_Model;
import com.cab_taxi.infinitecabs.Model.ApplyCouponModel;
import com.cab_taxi.infinitecabs.Model.Booking_Car_Model;
import com.cab_taxi.infinitecabs.Model.CallDriverModel;
import com.cab_taxi.infinitecabs.Model.Cancel_Ride_Model;
import com.cab_taxi.infinitecabs.Model.Dasboard_Car_List_Model;
import com.cab_taxi.infinitecabs.Model.Searching_Driver_Model;
import com.cab_taxi.infinitecabs.Model.User_Dasboard_Search_Car_List_Model;
import com.cab_taxi.infinitecabs.Model.User_Status_Background_Model;
import com.cab_taxi.infinitecabs.Payment.PaymentMode;
import com.cab_taxi.infinitecabs.PollyLine_Track.ApiInterface;
import com.cab_taxi.infinitecabs.PollyLine_Track.BeginJourneyEvent;
import com.cab_taxi.infinitecabs.PollyLine_Track.CurrentJourneyEvent;
import com.cab_taxi.infinitecabs.PollyLine_Track.EndJourneyEvent;
import com.cab_taxi.infinitecabs.PollyLine_Track.JourneyEventBus;
import com.cab_taxi.infinitecabs.PollyLine_Track.Result;
import com.cab_taxi.infinitecabs.PollyLine_Track.Route;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.RateDriver;
import com.cab_taxi.infinitecabs.SelectDestination;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.android.gms.maps.model.JointType.ROUND;
import static com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED;

public class HomeFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String GOOGLEMAP_ZOOMIN_BUTTON = "GoogleMapZoomInButton";
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    // LogCat tag
    private static final String TAG = Dashboard.class.getSimpleName();
    private static final long ANIMATION_TIME_PER_ROUTE = 3000;
    private final boolean isFirstPosition = true;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST = 18945;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_SOURCE = 18946;
    int PAYMENT_REQUEST = 18947;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;
    double latitude;
    double longitude;
    ArrayList<String> permissions = new ArrayList<>();
    PermissionUtils permissionUtils;
    boolean isPermissionGranted;
    CarList_Adapter carList_adapter;
    String latitude1, longitude1;
    GoogleMap mMap;
    Marker marker;
    String booking_id;
    String booking_condition;
    String bookingtype;
    ProgressDialog progressDoalog;
    UserSessionManager session;
    HashMap<String, String> user_data;
    Context context;
    AppLocationService appLocationService;
    ///Driver status check local data
    StatusManager_Background_Store_Data statusManager_background_store_data;
    HashMap<String, String> background_store_data;
    int row_index;
    boolean position = false;
    Marker myMarker;
    String Payment_Method;
    Dialog dialog1;
    String driverId = "";
    String BookingId = "";
    int LAUNCH_SECOND_ACTIVITY = 1;
    String zipcode = "abc";
    //    <-----------------------------booking car code--------------------------------------------------------->
    String path = "0";
    String path1 = "0";
    Double driverlat = 0.0;
    Double driverlong = 0.0;
    String card_id;
    String polyLine;
    ////sss
    SweetAlertDialog sweetAlertDialog2;
    String paystatus = "0";
    PolylineOptions lineOptions;
    ImageView civ_cab;
    RatingBar MyRating;
    String CouponCode = "";
    String imageload = "0";
    RelativeLayout rl_pickup;
    String finddriver = "0";
    CountDownTimer countDownTimer;
    private ImageView iv_menu;
    private ImageView iv_home;
    private TextView tv_pickup;
    private TextView tv_carmKM;
    private TextView tv_destination;
    // Google client to interact with Google API
    private ImageView iv_next;
    private ImageView iv_current_location;
    private BottomSheetBehavior sheetBehavior;
    private MapView mMapView;
    // list of permissions
    private GoogleMap googleMap;
    private List<Dasboard_Car_List_Model.Data> array_get_car_list;
    private List<User_Dasboard_Search_Car_List_Model.Data> array_get_car_search_data;
    private RecyclerView recyclerView_carlist;
    private TextView tv_carfare1, tv_passengerCount1, car_type;
    private String tv_serviceType, tv_carfare, tv_passengerCount, cab_id, maxprice, minfare;
    private TextView tv_fareEstimation;
    private List<LatLng> polyLineList;
    private List<LatLng> polyLineList1;
    private int index, next;
    private PolylineOptions polylineOptions, blackPolylineOptions;
    private Polyline blackPolyline, greyPolyLine;
    private Handler handler;
    private LatLng startPosition, endPosition;
    private float v;
    private double lat, lng;
    private LatLng sydney;
    private Disposable disposable;
    private GoogleApiClient mGoogleApiClient;
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }
                getLocation();
                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();

                } else {
                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }
            }
        }
    };
    private LinearLayout bottom_sheet;
    // The minimum offset value upto which your bottomsheet to move
    private int mMinOffset;
    private LinearLayout layout_cab_available;
    private LinearLayout ll_bottom_readynow_later;
    private Button btn_book_now;
    private Button btn_book_later;
    //fare Estimation Layout
    private LinearLayout ll_fareEstimation;
    private LinearLayout layout_confirm_cancel;
    private TextView tv_date;
    private TextView tv_time;
    private TextView tv_paymentMode;
    private TextView tv_couponaply;
    private Button btn_confirm;
    private Button btn_cancel;
    private View inlude_call_driver_layout;
    private LinearLayout ll_calldriver;
    private Button btn_callDriver;
    private Button btn_cancelRide;
    private EditText et_noteDialog;
    private EditText et_coupon;
    private Button btn_proceedDialog;
    private Button btn_cancelDialog;
    private TextView tv_driverRating, tv_name_driver, tv_cab_no, tv_cab_model;
    private CircleImageView civ_driver;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private ApiInterface apiInterface;
    private LinearLayout bottom_sheet_home_fragment;
    private String paymenttype;
    private double latitude_user;
    private double longitude_user;
    private CircularImageView location_icon;
    private String picup_lat = "";
    private String picup_lng = "";
    private String dest_lat = "";
    private String dest_lng = "";
    private String car_image;
    private ImageView iv_car;
    private LinearLayout choose_ride_rel;
    private TextView tv_card;
    private TextView oldprice;
    private TextView tv_eta;
    private Double startLatitude;
    private Double startLongitude;
    private Marker carMarker;
    private Polyline blackPolyLine;
    Animator.AnimatorListener polyLineAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

//            addMarker(polyLineList1.get(polyLineList1.size() - 1));
        }


        @Override
        public void onAnimationEnd(Animator animator) {

            List<LatLng> blackLatLng = blackPolyLine.getPoints();
            List<LatLng> greyLatLng = greyPolyLine.getPoints();

            greyLatLng.clear();
            greyLatLng.addAll(blackLatLng);
            blackLatLng.clear();

            blackPolyLine.setPoints(blackLatLng);
            greyPolyLine.setPoints(greyLatLng);

            blackPolyLine.setZIndex(2);

            animator.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        appLocationService = new AppLocationService(
                context);
        statusManager_background_store_data = new StatusManager_Background_Store_Data(context);
        background_store_data = statusManager_background_store_data.getUserDetails();
        session = new UserSessionManager(getActivity());
        user_data = session.getUserDetails();
        mMapView = view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        polyLineList = new ArrayList<>();
        polyLineList1 = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://maps.googleapis.com/")
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
//        GeocodingLocation locationAddress = new GeocodingLocation();
        initView(view);
       /* locationAddress.getAddressFromLocation(tv_destination.getText().toString(),
                getActivity(), new GeocoderHandler());*/
        return view;
    }

    public void cantouch_main(Boolean cantouch) {
        rl_pickup.setEnabled(cantouch);
        tv_destination.setEnabled(cantouch);
        tv_pickup.setEnabled(cantouch);
        iv_home.setEnabled(cantouch);
        iv_home.setClickable(cantouch);
    }

    private void initView(View view) {
        iv_menu = view.findViewById(R.id.iv_menu);
        iv_home = view.findViewById(R.id.iv_home);
        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("asdfasdf", "asdfasdfasdf");
                cancelRideFromRequestAcceptedLayout();
                startActivity(new Intent(getActivity(), Dashboard.class));
            }
        });
        civ_cab = view.findViewById(R.id.civ_cab);
        MyRating = view.findViewById(R.id.MyRating);
        MyRating.setIsIndicator(true);
        oldprice = view.findViewById(R.id.oldprice);
        tv_card = view.findViewById(R.id.tv_card);
        tv_eta = view.findViewById(R.id.tv_eta);
        choose_ride_rel = view.findViewById(R.id.choose_ride_rel);
        tv_couponaply = view.findViewById(R.id.tv_couponaply);
        location_icon = view.findViewById(R.id.location_icon);
        tv_pickup = view.findViewById(R.id.tv_pickup);
        tv_destination = view.findViewById(R.id.tv_destination);
        iv_current_location = view.findViewById(R.id.iv_current_location);
        rl_pickup = view.findViewById(R.id.rl_pickup);
        layout_cab_available = view.findViewById(R.id.layout_cab_available);
        tv_fareEstimation = view.findViewById(R.id.tv_fareEstimation);
        tv_carmKM = view.findViewById(R.id.tv_carmKM);

        car_type = view.findViewById(R.id.car_type);
        iv_car = view.findViewById(R.id.iv_car);
        tv_passengerCount1 = view.findViewById(R.id.tv_passengerCount);
        tv_carfare1 = view.findViewById(R.id.tv_carfare);
        et_coupon = view.findViewById(R.id.et_coupon);

        tv_driverRating = view.findViewById(R.id.tv_driverRating);
        tv_name_driver = view.findViewById(R.id.tv_name_driver);
        tv_cab_no = view.findViewById(R.id.tv_cab_no);
        tv_cab_model = view.findViewById(R.id.tv_cab_model);
        civ_driver = view.findViewById(R.id.civ_driver);
        bottom_sheet_home_fragment = view.findViewById(R.id.bottom_sheet_home_fragment);
        iv_menu.setOnClickListener(this);
        rl_pickup.setOnClickListener(this);
        tv_destination.setOnClickListener(this);
        iv_current_location.setOnClickListener(this);
        layout_cab_available.setOnClickListener(this);
        bottom_sheet_home_fragment.setOnClickListener(this);
        // The View with the BottomSheetBehavior
        bottom_sheet = view.findViewById(R.id.bottom_sheet);
        CoordinatorLayout coordinatorLayout = view.findViewById(R.id.cl);
        View bottomSheet = view.findViewById(R.id.bottom_sheet);

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);

        //From Main Screen
        ll_bottom_readynow_later = view.findViewById(R.id.ll_bottom_readynow_later);
        btn_book_now = view.findViewById(R.id.btn_book_now);
        btn_book_later = view.findViewById(R.id.btn_book_later);

        btn_book_now.setOnClickListener(this);
        btn_book_later.setOnClickListener(this);

        //fare estimation layout
        ll_fareEstimation = view.findViewById(R.id.ll_fareEstimation);
        layout_confirm_cancel = view.findViewById(R.id.layout_confirm_cancel);
        tv_date = view.findViewById(R.id.tv_date);
        tv_time = view.findViewById(R.id.tv_time);
        tv_paymentMode = view.findViewById(R.id.tv_paymentMode);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        btn_cancel = view.findViewById(R.id.btn_cancel);

        tv_date.setOnClickListener(this);
        tv_time.setOnClickListener(this);
        tv_paymentMode.setOnClickListener(this);
        tv_couponaply.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        iv_menu.setOnClickListener(this);

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");
        String dateToStr = format.format(today);
        System.out.println(dateToStr);
        tv_time.setText(dateToStr);

        //From click on confirm or cancel buttons
        inlude_call_driver_layout = view.findViewById(R.id.inlude_call_driver_layout);
        ll_calldriver = view.findViewById(R.id.ll_calldriver);
        btn_callDriver = view.findViewById(R.id.btn_callDriver);
        btn_cancelRide = view.findViewById(R.id.btn_cancelRide);

        btn_callDriver.setOnClickListener(this);
        btn_cancelRide.setOnClickListener(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ////Driver lat lng send to server in thread ---------------------background

        Map<String, String> map = new HashMap<>();
        // map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
        map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
        User_Status_Check_Background_Store(map);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Map<String, String> map = new HashMap<>();
                map.put("lat", Utility.getlat(getActivity()));
                map.put("lng", Utility.getlng(getActivity()));
                getAllCar(map);
                mMap = googleMap;
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.setTrafficEnabled(false);
                mMap.setIndoorEnabled(false);
                mMap.setBuildingsEnabled(false);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setAllGesturesEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                location_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //mMap.clear();
                        LatLng latLng = new LatLng(Double.parseDouble(Utility.getlat(context) + ""), Double.parseDouble(Utility.getlng(context) + ""));
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                        googleMap.animateCamera(cameraUpdate);
                    }
                });
                if (mMap != null &&
                        mMapView.findViewById(Integer.parseInt("1")) != null) {
                    // Get the button view
                    View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    // and next place it, on bottom right (as Google Maps app)
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                            locationButton.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    layoutParams.setMargins(40, 0, 0, 490);
                }
//                <------------compass height adhust code---------------------->
                if (mMapView != null &&
                        mMapView.findViewById(Integer.parseInt("1")) != null) {
                    // Get the view
                    View locationCompass = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("5"));
                    // and next place it, on bottom right (as Google Maps app)
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                            locationCompass.getLayoutParams();
                    // position on right bottom
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                    layoutParams.setMargins(60, 600, 30, 0); // 160 la truc y , 30 la  truc x
                }
                //                <------------compass height adhust code---------------------->

                // move zoom controls to bottom right
                moveZoomControls(mMapView, -1, -1, 4, 600, false, false);
                // move zoom controls to bottom right
                LocationCallback mLocationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        List<Location> locationList = locationResult.getLocations();
                        if (locationList.size() > 0) {
                            //The last location in the list is the newest
                            Location location = locationList.get(locationList.size() - 1);
                            Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                            mLastLocation = location;
                            if (myMarker != null) {
                                myMarker.remove();
                                // myMarker.remove();
                            }
                            //Place current location marker
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            sydney = new LatLng(location.getLatitude(), location.getLongitude());
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(latLng);
                            markerOptions.title("Current Position");
                            int height = 150;
                            int width = 100;

                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.current_marker_1);
                            Bitmap b = bitmapdraw.getBitmap();
                            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                            // markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cureent_location_marker));
                            //  markerOptions.icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v)));
                            myMarker = mMap.addMarker(markerOptions);
//                            LatLng latLng = new LatLng(Double.parseDouble(Utility.getlat(context) + ""), Double.parseDouble(Utility.getlng(context) + ""));
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);

                            googleMap.animateCamera(cameraUpdate);
                            getLocation();
                            if (mLastLocation != null) {
                                latitude = mLastLocation.getLatitude();
                                longitude = mLastLocation.getLongitude();
                                getAddress();
                            } else {
                                showToast("Couldn't get the location. Make sure location is enabled on the device");
                            }
                        }
                    }
                };

                mLocationRequest = new LocationRequest();
//                mLocationRequest.setInterval(12000000); // two minute interval
//                mLocationRequest.setFastestInterval(12000000);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                mMap.clear();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //Location Permission already granted
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        googleMap.setMyLocationEnabled(true);
                    } else {
                        //Request Location Permission
                        checkLocationPermission();
                    }
                } else {
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    googleMap.setMyLocationEnabled(true);
                }
            }
        });
        recyclerView_carlist = view.findViewById(R.id.recyclerView);

        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
        }
    }

    private void moveView(View view, int left, int top, int right, int bottom, boolean horizontal, boolean vertical) {
        try {
            assert view != null;
            // replace existing layout params
            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);

            if (left >= 0) {
                rlp.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            }

            if (top >= 0) {
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            }

            if (right >= 0) {
                rlp.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            }

            if (bottom >= 0) {
                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            }

            if (horizontal) {
                rlp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            }

            if (vertical) {
                rlp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            }

            rlp.setMargins(left, top, right, bottom);
            view.setLayoutParams(rlp);
        } catch (Exception ex) {
            Log.e(TAG, "moveView() - failed: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }

    private void moveZoomControls(View mapView, int left, int top, int right, int bottom, boolean horizontal, boolean vertical) {

        assert mapView != null;

        View zoomIn = mapView.findViewWithTag(GOOGLEMAP_ZOOMIN_BUTTON);
        // we need the parent view of the zoomin/zoomout buttons - it didn't have a tag
        // so we must get the parent reference of one of the zoom buttons
        View zoomInOut = (View) zoomIn.getParent();
        if (zoomInOut != null) {
            moveView(zoomInOut, left, top, right, bottom, horizontal, vertical);
        }
    }

    private void showmessage(String message) {
        View parentLayout = getActivity().findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //getActivity().finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                open();
                break;

            case R.id.rl_pickup:
                mMap.clear();
                Intent intentDest = new Intent(getActivity(), SelectDestination.class).putExtra("pickup_location", tv_pickup.getText().toString());
                startActivityForResult(intentDest, PLACE_AUTOCOMPLETE_REQUEST_CODE_SOURCE);
                break;

            case R.id.tv_couponaply:
                if (tv_couponaply.getText().equals("Remove")) {
                    tv_fareEstimation.setText("$" + minfare + "-$" + maxprice);
                    tv_couponaply.setEnabled(true);
                    tv_couponaply.setText("Apply");
                    tv_couponaply.setTextColor(Color.BLACK);
                    et_coupon.setText("");
                    oldprice.setVisibility(View.GONE);
                    CustomToast.makeText(getActivity(), "Promo code removed", CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();

                } else {
                    if (et_coupon.getText().toString().isEmpty()) {
                        CustomToast.makeText(getActivity(), "Please Enter Promo code", CustomToast.LENGTH_LONG, CustomToast.WARNING, true).show();

//                        Toast.makeText(getActivity(), "Please Enter Promo code", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Map<String, String> map = new HashMap<>();
                        map.put("userID", user_data.get(UserSessionManager.KEY_ID));
                        map.put("code", et_coupon.getText().toString().trim());
                        map.put("min_amount", minfare);
                        map.put("max_amount", maxprice);
                        applypromocode(map);
                    }
                }

                break;
            case R.id.tv_destination:
                mMap.clear();
                Intent intentDest1 = new Intent(getActivity(), SelectDestination.class).putExtra("pickup_location", tv_pickup.getText().toString());
                startActivityForResult(intentDest1, PLACE_AUTOCOMPLETE_REQUEST_CODE_SOURCE);
                break;
            case R.id.iv_current_location:

                getLocation();

                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();

                } else {
                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }

                LatLng latLng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                googleMap.animateCamera(cameraUpdate);

                break;

            case R.id.layout_cab_available:

                if (sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    sheetBehavior.setState(STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                break;


            //hide next cab available layout and show confirm cancel cab layout

            case R.id.btn_book_now:
                if (tv_serviceType == null) {
                    CustomToast.makeText(getActivity(), "Please choose a ride before to continue with the booking.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                } else {

                    if (!Utility.getCard(getActivity()).equals("0")) {
                        tv_card.setText("Pay with the App");
                        card_id = Utility.getCard(getActivity());
                    }
                    if (Utility.getPaytype(getActivity()).equals("cash")) {
                        tv_card.setText("Pay the Driver Directly");
                        card_id = "0";
                    }
                    Payment_Method = tv_card.getText().toString();
                    if (Payment_Method.equals("Pay the Driver Directly")) {

                        Drawable img = getContext().getResources().getDrawable(R.drawable.ic_business);
                        img.setBounds(0, 0, 60, 60);
                        tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_business),null,null,null);
                    } else {
                        Drawable img = getContext().getResources().getDrawable(R.drawable.ic_credit_card);
                        img.setBounds(0, 0, 60, 60);
                        tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_credit_card),null,null,null);
                    }
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    String formattedDate = df.format(c.getTime());
                    tv_date.setText(formattedDate);

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("hh:mm");
                    String dateToStr = format.format(today);
                    System.out.println(dateToStr);
                    tv_time.setText(dateToStr);

                    booking_condition = "booknow";
                    tv_time.setEnabled(false);
                    tv_date.setEnabled(false);
                    showConfirmCancelLayout();
                    tv_fareEstimation.setText("$" + minfare + "-$" + maxprice);
                }
                break;

            case R.id.btn_book_later:
                if (tv_serviceType == null) {
                    CustomToast.makeText(getActivity(), "Please choose a ride before to continue with the booking.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                } else {
                    if (!Utility.getCard(getActivity()).equals("0")) {
                        tv_card.setText("Pay with the App");
                        card_id = Utility.getCard(getActivity());
                    }
                    if (Utility.getPaytype(getActivity()).equals("cash")) {
                        tv_card.setText("Pay the Driver Directly");
                        card_id = "0";
                    }
                    Payment_Method = tv_card.getText().toString();
                    if (Payment_Method.equals("Pay the Driver Directly")) {
                        Drawable img = getContext().getResources().getDrawable(R.drawable.ic_business);
                        img.setBounds(0, 0, 60, 60);
                        tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_business),null,null,null);
                    } else {
                        Drawable img = getContext().getResources().getDrawable(R.drawable.ic_credit_card);
                        img.setBounds(0, 0, 60, 60);
                        tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_credit_card),null,null,null);
                    }
                    tv_time.setEnabled(true);
                    tv_date.setEnabled(true);
                    booking_condition = "booklater";
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    String formattedDate = df.format(c.getTime());
                    tv_date.setText(formattedDate);

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("hh:mm");
                    String dateToStr = format.format(today);
                    System.out.println(dateToStr);
                    tv_time.setText(dateToStr);

                    showConfirmCancelLayout();
                    tv_fareEstimation.setText("$" + minfare + tv_carfare);
                }
                //  changeBottomSheetPeekHeight();
                break;
            //hide confirm or cancel layout and show call driver layout
            case R.id.tv_date:
                if (booking_condition.equals("booknow")) {
                } else {
                    showDatePicker();
                }
                break;
            case R.id.tv_time:
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                       /* Calendar datetime = Calendar.getInstance();
                        Calendar c = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                        datetime.set(Calendar.MINUTE, minute);
                        if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                            //it's after current
                            tv_time.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                        } else {
                            //it's before current'
                            Toast.makeText(getActivity(), "Invalid Time", Toast.LENGTH_LONG).show();
                        }*/

                        tv_time.setText(selectedHour + ":" + selectedMinute );
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");

                mTimePicker.show();
                break;
            case R.id.tv_paymentMode:
                Intent intentDest11 = new Intent(getActivity(), PaymentMode.class);
                startActivityForResult(intentDest11, PAYMENT_REQUEST);
                break;

            case R.id.btn_confirm:
                if (Payment_Method == null) {
                    CustomToast.makeText(getActivity(), "Please Select a Payment Method", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                } else if (Payment_Method.equals("0")) {
                    CustomToast.makeText(getActivity(), "Please Select a Payment Method", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                } else if (Payment_Method.equals("Payment")) {
                    CustomToast.makeText(getActivity(), "Please Select a Payment Method", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                } else {
                    showDialog();
                }
//                tv_card.setText(sharedpreferences.getString(Select, ""));
//                if (tv_card.getText().equals("Payment")){
//
//                    CustomToast.makeText(getActivity(), "Please Select a Payment Method", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
//                }else {
//                    showDialog();
//                }
                break;
            case R.id.btn_cancel:
                showCabAvailableLayout();
//                changeBottomSheetPeekHeight();
                break;
            //call driver dialog or cancel ride
            case R.id.btn_callDriver:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "+61280741930"));
                startActivity(intent);
                //vishal
            /*    Map<String, String> map = new HashMap<>();
                map.put("user_id",user_data.get(UserSessionManager.KEY_ID));
                CallriverApi(map);*/
                break;
            case R.id.btn_cancelRide:
               /* Map<String, String> map = new HashMap<>();
                map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                Cancel_Booking(map);*/
                Intent i = new Intent(getActivity(), CancleRideReason.class);
                startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
                break;
        }
    }

    private void open() {
        ((Dashboard) getActivity()).openDrawer();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    //method of layout
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private void showCallDriverLayout() {
        inlude_call_driver_layout.setVisibility(View.VISIBLE);
        ll_calldriver.setVisibility(View.VISIBLE);
        layout_confirm_cancel.setVisibility(View.GONE);
        ll_fareEstimation.setVisibility(View.GONE);
    }

    private void showCallDriverLayout1() {
        inlude_call_driver_layout.setVisibility(View.VISIBLE);
        ll_calldriver.setVisibility(View.VISIBLE);
        layout_confirm_cancel.setVisibility(View.GONE);
        ll_fareEstimation.setVisibility(View.GONE);
        ll_bottom_readynow_later.setVisibility(View.GONE);
    }

    private void showCabAvailableLayout() {
        layout_cab_available.setVisibility(View.VISIBLE);
        ll_bottom_readynow_later.setVisibility(View.VISIBLE);
        layout_confirm_cancel.setVisibility(View.GONE);
        ll_fareEstimation.setVisibility(View.GONE);
    }

    private void callDriver() {

//        startActivity(new Intent(getActivity(), RateDriver.class));
    }

    private void cancelRideFromRequestAcceptedLayout() {
        layout_cab_available.setVisibility(View.VISIBLE);
        ll_bottom_readynow_later.setVisibility(View.VISIBLE);
        inlude_call_driver_layout.setVisibility(View.GONE);
        ll_calldriver.setVisibility(View.GONE);
        Map<String, String> map = new HashMap<>();
        map.put("bookingId", booking_id);
        CallriverApi(map);
    }

    public void showDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        tv_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        googleMap.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String destination = "destination";
        if (resultCode == Activity.RESULT_OK) {
            if (marker != null) {
                marker.remove();
                myMarker.remove();
            }
            if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_SOURCE) {
                if (destination.equals(data.getStringExtra("destination"))) {
                    tv_destination.setText(data.getStringExtra("drop_location"));
                    tv_pickup.setText(data.getStringExtra("pick_location"));
                    picup_lat = data.getStringExtra("pic_lat");
                    picup_lng = data.getStringExtra("pic_lng");
                    dest_lat = data.getStringExtra("drop_lat");
                    dest_lng = data.getStringExtra("drop_lng");
                    Log.d("iiiresult111", picup_lat + "__" + picup_lng + "__" + dest_lat + "__" + dest_lng + "__");
//                    getAddress(Double.parseDouble(picup_lat),Double.parseDouble(picup_lng));
                    try {
                        getAddress11(Double.parseDouble(picup_lat), Double.parseDouble(picup_lng));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                   /* map.put("pickup_lat", picup_lat + "");
                    map.put("pickup_long", picup_lng + "");
                    map.put("drop_lat", dest_lat + "");
                    map.put("drop_long", dest_lng + "");*/
                    Map<String, String> map = new HashMap<>();
                    //(email,password)
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
                    String currentDateandTime = sdf.format(new Date());
                    Log.d("jhghjgjh", currentDateandTime);
                    map.put("user_pickup_latitude", picup_lat + "");
                    map.put("user_pickup_longlatitude", picup_lng + "");
                    map.put("user_drop_latitude", dest_lat + "");
                    map.put("user_drop_longlatitude", dest_lng + "");
                    map.put("current_time", currentDateandTime + "");
                    getSearchCarList(map);
                    /// two point source to destination polyline draw when select source and destination point before booking driver

                    LatLng latLng1 = new LatLng(Double.parseDouble(picup_lat), Double.parseDouble(picup_lng));
                    MarkerOptions markerOptions1 = new MarkerOptions();
                    markerOptions1.position(latLng1);
                    // markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                    // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v)));
                    int height1 = 150;
                    int width1 = 150;
                    BitmapDrawable bitmapdraw1 = (BitmapDrawable) getResources().getDrawable(R.drawable.source_marker_1);
                    Bitmap b1 = bitmapdraw1.getBitmap();
                    Bitmap smallMarker1 = Bitmap.createScaledBitmap(b1, width1, height1, false);
                    // markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                    markerOptions1.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1));
                    markerOptions1.title("Pickup Point");
                    mMap.addMarker(markerOptions1);
                    if (blackPolyLine == null) {
                    } else {
                        blackPolyLine.remove();
                        greyPolyLine.remove();
                    }
                    String key  = getResources().getString(R.string.key1)+getResources().getString(R.string.key2)+getResources().getString(R.string.key3);
                    apiInterface.getDirections("driving", "less_driving",
                            Double.parseDouble(picup_lat) + "," + Double.parseDouble(picup_lng), Double.parseDouble(dest_lat) + "," + Double.parseDouble(dest_lng),
                            key)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    new SingleObserver<Result>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                        }

                                        @Override
                                        public void onSuccess(Result result) {
                                            List<Route> routeList = result.getRoutes();
                                            for (Route route : routeList) {
                                                myMarker.remove();
                                                polyLineList.clear();
                                                String polyLine = route.getOverviewPolyline().getPoints();
                                                polyLineList = decodePoly(polyLine);
                                             /*   mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                                        (new CameraPosition.Builder().target(latLng1)
                                                                .zoom(12).build()));*/
                                                drawPolyLineAndAnimateCarSource_DestinationLine();
//                                                drawPolyLineOnMap_whenPlaceSearch(polyLineList);
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            e.printStackTrace();
                                        }
                                    });
                } else {

                    tv_destination.setText(data.getStringExtra("drop_location"));
                    tv_pickup.setText(data.getStringExtra("pick_location"));
                    picup_lat = data.getStringExtra("pic_lat");
                    picup_lng = data.getStringExtra("pic_lng");
                    dest_lat = data.getStringExtra("drop_lat");
                    dest_lng = data.getStringExtra("drop_lng");
                    try {
                        getAddress11(Double.parseDouble(picup_lat), Double.parseDouble(picup_lng));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("iiiresult111", picup_lat + "__" + picup_lng + "__" + dest_lat + "__" + dest_lng + "__");
                    //                <----------------------after search pickup and drop point then carlist show-------------------->
                    Map<String, String> map = new HashMap<>();
                    //(email,password)
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
                    String currentDateandTime = sdf.format(new Date());
                    Log.d("jhghjgjh", currentDateandTime);
                    map.put("user_pickup_latitude", picup_lat + "");
                    map.put("user_pickup_longlatitude", picup_lng + "");
                    map.put("user_drop_latitude", dest_lat + "");
                    map.put("user_drop_longlatitude", dest_lng + "");
                    map.put("current_time", currentDateandTime + "");
                    getSearchCarList(map);
                    /// two point source to destination polyline draw when select source and destination point before booking driver
                    LatLng latLng1 = new LatLng(Double.parseDouble(picup_lat), Double.parseDouble(picup_lng));
                    MarkerOptions markerOptions1 = new MarkerOptions();
                    markerOptions1.position(latLng1);
                    markerOptions1.title("Pickup Point");
//                    markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                    int height1 = 150;
                    int width1 = 100;
                    BitmapDrawable bitmapdraw1 = (BitmapDrawable) getResources().getDrawable(R.drawable.source_marker_1);
                    Bitmap b1 = bitmapdraw1.getBitmap();
                    Bitmap smallMarker1 = Bitmap.createScaledBitmap(b1, width1, height1, false);
                    // markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                    markerOptions1.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1));
                    // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v)));
                    mMap.addMarker(markerOptions1);
                    if (blackPolyLine == null) {
                    } else {
                        blackPolyLine.remove();
                        greyPolyLine.remove();
                    }
                    String key  = getResources().getString(R.string.key1)+getResources().getString(R.string.key2)+getResources().getString(R.string.key3);
                    apiInterface.getDirections("driving", "less_driving",
                            Double.parseDouble(picup_lat) + "," + Double.parseDouble(picup_lng), Double.parseDouble(dest_lat) + "," + Double.parseDouble(dest_lng),
                            key)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    new SingleObserver<Result>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                        }

                                        @Override
                                        public void onSuccess(Result result) {
                                            List<Route> routeList = result.getRoutes();
                                            for (Route route : routeList) {
                                                try {
                                                    myMarker.remove();
                                                    polyLineList.clear();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                /*mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                                        (new CameraPosition.Builder().target(latLng1)
                                                                .zoom(12).build()));*/
                                                String polyLine = route.getOverviewPolyline().getPoints();
                                                polyLineList = decodePoly(polyLine);
                                                drawPolyLineAndAnimateCarSource_DestinationLine();
//                                                drawPolyLineOnMap_whenPlaceSearch(polyLineList);
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            e.printStackTrace();
                                        }
                                    });

                }
            }
            if (requestCode == PAYMENT_REQUEST) {
                card_id = "";
                Payment_Method = data.getStringExtra("Payment_mode");
                card_id = data.getStringExtra("card_id");
                Log.d("payment", Payment_Method);
                Log.d("payment", card_id);
                if (Payment_Method.equals("cash")) {
                    Payment_Method = "Pay the Driver Directly";
                    Drawable img = getContext().getResources().getDrawable(R.drawable.ic_business);
                    img.setBounds(0, 0, 60, 60);
                    tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_business),null,null,null);
                } else {
                    Payment_Method = "Pay with the App";
                    Drawable img = getContext().getResources().getDrawable(R.drawable.ic_credit_card);
                    img.setBounds(0, 0, 60, 60);
                    tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_credit_card),null,null,null);
                }
                if (Payment_Method.equals("0")) {
                    tv_card.setText("Payment");
                    card_id = "";
                } else {
                    tv_card.setText(Payment_Method);
                }

                Log.d("sdfsdf", card_id + "______");
            }

            if (requestCode == LAUNCH_SECOND_ACTIVITY) {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
                    String reason = data.getStringExtra("reason");
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                    map.put("reason_id", result);
                    map.put("driver_id", driverId);
                    map.put("type", "user");
                    map.put("note", reason);
                    map.put("booking_id", booking_id);
                    Cancel_Booking(map);
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                    map.put("reason_id", "");
                    map.put("driver_id", driverId);
                    map.put("type", "user");
                    map.put("note", "");
                    map.put("booking_id", booking_id);
                    Cancel_Booking(map);
                    //Write your code if there's no result
                }
            }
        } else {
            if (!Utility.getCard(getActivity()).equals("0")) {
                tv_card.setText("Pay with the App");
                card_id = Utility.getCard(getActivity());
            }
            if (Utility.getPaytype(getActivity()).equals("cash")) {
                tv_card.setText("Pay the Driver Directly");
                card_id = "0";
            }
            if (Utility.getPaytype(getActivity()).equals("0")) {
                tv_card.setText("Payment");
                card_id = "0";
            }

            Payment_Method = tv_card.getText().toString();
            if (Payment_Method.equals("Pay the Driver Directly")) {
                Drawable img = getContext().getResources().getDrawable(R.drawable.ic_business);
                img.setBounds(0, 0, 60, 60);
                tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_business),null,null,null);
            } else {
                Drawable img = getContext().getResources().getDrawable(R.drawable.ic_credit_card);
                img.setBounds(0, 0, 60, 60);
                tv_card.setCompoundDrawables(img, null, null, null);
//                    tv_card.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_credit_card),null,null,null);
            }

            Log.d("hgjhgjhg", Payment_Method + "_________" + cab_id);
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        checkPlayServices();
        //This is an event bus for receiving journey events this can be shifted anywhere
        //in code.
        //Do remember to dispose when not in use. For eg. its necessary to dispose it in
        //onStop as activity is not visible.
        paystatus = "0";
        disposable = JourneyEventBus.getInstance().getOnJourneyEvent()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        if (o instanceof BeginJourneyEvent) {
                            showmessage("Journey has started");
//                            Snackbar.make(linearLayout, "Journey has started",
//                                    Snackbar.LENGTH_SHORT).show();
                        } else if (o instanceof EndJourneyEvent) {
                            showmessage("Journey has ended");
//                            Snackbar.make(linearLayout, "Journey has ended",
//                                    Snackbar.LENGTH_SHORT).show();
                        } else if (o instanceof CurrentJourneyEvent) {
//                            Map<String, String> map = new HashMap<>();
//                            map.put("booking_id", "1709");
//                            map.put("user_id", "5");
//                            Track_User_After_bookingaccept1(map);
                            /*
                             * This can be used to receive the current location update of the car
                             */
                            //   Log.d(TAG,"Current "+((CurrentJourneyEvent) o).getCurrentLatLng());
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    //    <-----------------------------booking car code alert dialog  end --------------------------------------------------------->
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
//        if (!disposable.isDisposed()) {
//            disposable.dispose();
//        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void getLocation() {
        if (isPermissionGranted) {
            try {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    //get Addresss-------------
    private Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            zipcode = addresses.get(0).getPostalCode();
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void getzipcode(double latitude, double longitude) {
        final Geocoder geocoder = new Geocoder(getActivity());
        final String zip = "90210";

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                // Use the address as needed
                String message = String.format("Latitude",
                        address.getPostalCode());
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(getActivity(), "Unable to geocode zipcode", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            // handle exception
        }
    }

    public String getAddress11(Double lat, Double lng) throws IOException {

        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        String zipcode = null;
        Address address = null;

        if (mGeocoder != null) {

            List<Address> addresses = mGeocoder.getFromLocation(lat, lng, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    address = addresses.get(i);
                    if (address.getPostalCode() != null) {
                        zipcode = address.getPostalCode();
                        Log.d(TAG, "Postal code: " + address.getPostalCode());
                        break;
                    }

                }
                return zipcode;
            }
        }

        return null;
    }


    //get Addresss-------------
    private void getAddress() {
        Address locationAddress = getAddress(latitude, longitude);
        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;
            if (address.equals("null")) {
                if (!TextUtils.isEmpty(address)) {
                    currentLocation = address;

                    if (!TextUtils.isEmpty(address1))
                        currentLocation += "\n" + address1;

                    if (!TextUtils.isEmpty(city)) {
                        currentLocation += "\n" + city;

                        if (!TextUtils.isEmpty(postalCode))
                            currentLocation += " - " + postalCode;
                    } else {
                        if (!TextUtils.isEmpty(postalCode))
                            currentLocation += "\n" + postalCode;
                    }

                    if (!TextUtils.isEmpty(state))
                        currentLocation += "\n" + state;

                    if (!TextUtils.isEmpty(country))
                        currentLocation += "\n" + country;

                    tv_pickup.setText(currentLocation);
                    tv_pickup.setVisibility(View.VISIBLE);
                }
            } else {
                currentLocation = address;
                tv_pickup.setText(currentLocation);
                tv_pickup.setVisibility(View.VISIBLE);
            }

        }
    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    // checkPlayServices----
    private boolean checkPlayServices() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(getActivity(), resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getActivity(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Once connected with google api, get the location
        getLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

    }

    private void showConfirmCancelLayout() {
        ll_fareEstimation.setVisibility(View.VISIBLE);
        layout_confirm_cancel.setVisibility(View.VISIBLE);
        layout_cab_available.setVisibility(View.GONE);
        ll_bottom_readynow_later.setVisibility(View.GONE);
    }

    //    <-----------------------------API Calling--------------------------------------------------------->
    private void showDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_note_for_driver);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        et_noteDialog = dialog.findViewById(R.id.et_noteDialog);
        btn_proceedDialog = dialog.findViewById(R.id.btn_proceedDialog);
        btn_cancelDialog = dialog.findViewById(R.id.btn_cancelDialog);
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                if (source.length() > 0 && Character.isWhitespace(source.charAt(0)) && et_noteDialog.getText().toString().length() == 0) {
                    return "";
                }
                return source;
            }
        };
        et_noteDialog.setFilters(new InputFilter[]{filter});

        btn_proceedDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (booking_condition.equals("booknow")) {
                    bookingtype = "now";
                } else {
                    bookingtype = "booklater";
                }
//                booking_condition = "booklater";
//                map.put("user_id", "5");
//                map.put("cab_id", "4");
//                map.put("pickup_lat", "-33.729752");
//                map.put("pickup_long", "150.836090");
//                map.put("drop_lat", "-66.729752");
//                map.put("drop_long", "250.836090");
//                map.put("pickup_date_time", "10-12-2019");
//                map.put("isdevice", "wf");
//                map.put("booking_type", bookingtype);
//                map.put("payment_type", "cash");
//                map.put("message", et_noteDialog.getText().toString());

                // LatLng destinationpoint = getLocationFromAddress(getActivity(), tv_destination.getText().toString());
                //  LatLng sourcepoint = getLocationFromAddress(getActivity(), tv_pickup.getText().toString());
                    try {
                        zipcode=   getAddress11(Double.parseDouble(picup_lat), Double.parseDouble(picup_lng));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                Log.d("sdf123456789", "___________" + zipcode + "_______________");
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                map.put("cab_id", cab_id);
                map.put("pickup_lat", picup_lat + "");
                map.put("pickup_long", picup_lng + "");
                map.put("drop_lat", dest_lat + "");
                map.put("drop_long", dest_lng + "");
                map.put("pickup_date_time", tv_date.getText().toString() + " " + tv_time.getText().toString());
                map.put("book_create_date_time", tv_date.getText().toString() + " " + tv_time.getText().toString());
                Log.d("pickup_date_time", tv_date.getText().toString() + " " + tv_time.getText().toString());
                map.put("isdevice", "android");
                map.put("booking_type", bookingtype);
                map.put("drop_address", tv_destination.getText().toString());
                map.put("pick_address", tv_pickup.getText().toString());
                if (tv_card.getText().toString().equals("Pay with the App")) {
                    map.put("payment_type", "card");
                } else {
                    map.put("payment_type", "cash");
                }
                map.put("pincode", zipcode);
//                map.put("pincode", "302020");
//                map.put("amount", maxprice);
                map.put("coupan", CouponCode);
                map.put("cabType", cab_id);
                map.put("message", et_noteDialog.getText().toString());
                map.put("cardId", card_id);
                Log.d("sdfasdf", maxprice);
                Booking_Car(map);
            }
        });

        btn_cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getAllCar(Map<String, String> map) {

        Call<Dasboard_Car_List_Model> call = Apis.getAPIService().getCarList(map);
        call.enqueue(new Callback<Dasboard_Car_List_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<Dasboard_Car_List_Model> call, Response<Dasboard_Car_List_Model> response) {
                // dialog.dismiss();
                Dasboard_Car_List_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()) {
                            CustomToast.makeText(getActivity(), "No Cab Available", CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                        } else {
                            // array_get_car_list = new ArrayList<>();
                            // array_get_car_list.addAll(userdata.getData());
                            for (int i = 0; i < userdata.getData().size(); i++) {
                                LatLng sydney = new LatLng(Double.parseDouble(userdata.getData().get(i).getLatitude()), Double.parseDouble(userdata.getData().get(i).getLonglatitude()));
                                latitude1 = userdata.getData().get(i).getLatitude();
                                longitude1 = userdata.getData().get(i).getLonglatitude();
                                int height = 80;
                                int width = 100;
                                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.car1);
                                Bitmap b = bitmapdraw.getBitmap();
                                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                Marker marker = mMap.addMarker(new MarkerOptions().position(sydney).title(userdata.getData().get(i).getCartype())
                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
//                                latitude = mLastLocation.getLatitude();
//                                longitude = mLastLocation.getLongitude();
                           /*     CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(new LatLng(Double.parseDouble(String.valueOf(userdata.getData().get(0).getLatitude() == null ? "0.00" : userdata.getData().get(0).getLatitude())),
                                                Double.parseDouble(String.valueOf(userdata.getData().get(0).getLonglatitude() == null ? "0.00" : userdata.getData().get(0).getLonglatitude()))))      // Sets the center of the map to location user
                                        .zoom(16)                   // Sets the zoom
                                        .bearing(90)                // Sets the orientation of the camera to east
                                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                                        .build();                   // Creates a CameraPosition from the builder
                                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))*/
                                //                                setMarkersOnMap(googleMap, array_get_car_list);

                            }
                        }
                    } else {
                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Dasboard_Car_List_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //enter pickup and destination then car list come api  ---------
    private void getSearchCarList(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<User_Dasboard_Search_Car_List_Model> call = Apis.getAPIService().searchCarList(map);
        call.enqueue(new Callback<User_Dasboard_Search_Car_List_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Dasboard_Search_Car_List_Model> call, Response<User_Dasboard_Search_Car_List_Model> response) {
                dialog.dismiss();
                User_Dasboard_Search_Car_List_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()) {
//                            no_data_found.setVisibility(View.VISIBLE);
//                            no_data_found.setText(userdata.getMessage());
//                            swipeRefreshLayout.setVisibility(View.GONE);
                        } else {
                            array_get_car_search_data = new ArrayList<>();
                            array_get_car_search_data.addAll(userdata.getData());
                            carList_adapter = new CarList_Adapter();
                            recyclerView_carlist.setAdapter(carList_adapter);
                            carList_adapter.notifyDataSetChanged();
                            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                                sheetBehavior.setState(STATE_EXPANDED);
                            } else {
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            }
                        }
                    } else {
                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<User_Dasboard_Search_Car_List_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //    <-----------------------------BookingCab API  --------------------------------------------------------->
    public void Booking_Car(final Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Booking_Car_Model> call = Apis.getAPIService().booking_car(map);
        call.enqueue(new Callback<Booking_Car_Model>() {
            @Override
            public void onResponse(Call<Booking_Car_Model> call, Response<Booking_Car_Model> response) {
                dialog.dismiss();
                Booking_Car_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        dialog1 = new Dialog(getActivity());
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.dialog_search);
                        dialog1.setCancelable(false);
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Button btn_search = dialog1.findViewById(R.id.btn_search);
                        btn_search.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();
                                finddriver = "1";
                                Map<String, String> map = new HashMap<>();
                                map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                map.put("reason_id", "");
                                map.put("driver_id", driverId);
                                map.put("type", "user");
                                map.put("note", "");
                                map.put("booking_id", booking_id);
                                Cancel_Booking(map);
                               /* cancelRideFromRequestAcceptedLayout();
                                startActivity(new Intent(getActivity(), Dashboard.class));*/
                            }
                        });
                        dialog1.show();

                        booking_id = user.getData().getBooking_id() + "";
                        Map<String, String> map = new HashMap<>();
                        map.put("booking_id", booking_id);
                        Searching_Driver(map);
//
                    } else if (user.getStatusCode().equals("202")) {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("Thank you for choosing Infinite Cabs");
                        sweetAlertDialog.setContentText("Your booking has successfully been received and an available cab will be sent to your pick up address." + "");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                                startActivity(new Intent(context, Dashboard.class));
                            }
                        });
                        sweetAlertDialog.show();
                    } else if (user.getStatusCode().equals("201")) {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("Thank you for choosing Infinite Cabs");
                        sweetAlertDialog.setContentText(user.getMessage() + "");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.girl));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                                startActivity(new Intent(context, Dashboard.class));
                            }
                        });
                        sweetAlertDialog.show();
                    } else {
                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                } else {
                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<Booking_Car_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    //    <-----------------------------Search Driver API  --------------------------------------------------------->
    public void Searching_Driver(final Map<String, String> map) {
        inlude_call_driver_layout.setVisibility(View.GONE);
        Call<Searching_Driver_Model> call = Apis.getAPIService().searchdeiver(map);
        call.enqueue(new Callback<Searching_Driver_Model>() {
            @Override
            public void onResponse(Call<Searching_Driver_Model> call, Response<Searching_Driver_Model> response) {

                Searching_Driver_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {

                        dialog1.dismiss();
                        driverId = user.getBookingData().getDriverId();
                        booking_id = user.getBookingData().getBookingId() + "";
                        tv_driverRating.setText(user.getBookingData().getUserstarrating());
                        MyRating.setRating(Float.parseFloat(user.getBookingData().getUserstarrating()));
                        tv_name_driver.setText(user.getBookingData().getDriverName());

                        tv_cab_model.setText(user.getBookingData().getDriverCar_Model());
                        try {
                          /*     Picasso.get().load("http://admin.infinitecabs.com.au/texiapp-new/user_image/" + user.getBookingData().getDriverImage())
                                    .error(R.drawable.user)
                                    .priority(Picasso.Priority.HIGH)
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);*/

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.mipmap.ic_launcher_round)
                                    .error(R.mipmap.ic_launcher_round);


                            Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getBookingData().getDriverImage()).apply(options).into(civ_driver);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tv_cab_no.setText(user.getBookingData().getDrivercar_no());
                        try {
                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.mipmap.ic_launcher_round)
                                    .error(R.mipmap.ic_launcher_round);

                            Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getBookingData().getDriverImage())
                                    .apply(options).into(civ_cab);
                            /*Picasso.get().load("https://admin.infinitecabs.com.au/" + user.getBookingData().getDriverImage())
                                    .error(R.drawable.car)
                                    .priority(Picasso.Priority.HIGH)
                                    .centerInside()
                                    .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_cab);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        showCallDriverLayout();

                        Map<String, String> map = new HashMap<>();
                        map.put("booking_id", booking_id);
                        map.put("driver_id", user.getBookingData().getDriverId());
                        map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                        Track_User_After_bookingaccept(map);
//                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    } else if (user.getStatusCode().equals("202")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Create an Intent that will start the Menu-Activity.
                                if (finddriver.equals("0")) {
                                    Map<String, String> map = new HashMap<>();
                                    map.put("booking_id", booking_id);
                                    Searching_Driver(map);
//                                    CustomToast.makeText(getActivity(), "Thank you for choosing Infinite Cabs \n Please wait while we are searching a closest driver for you." /*user.getMessage()*/, CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();
                                }
                            }
                        }, 10000);
                    } else if (user.getStatusCode().equals("201")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Create an Intent that will start the Menu-Activity.
                                if (finddriver.equals("0")) {
                                    Map<String, String> map = new HashMap<>();
                                    map.put("booking_id", booking_id);
                                    Searching_Driver(map);
//                                    CustomToast.makeText(getActivity(), "Thank you for choosing Infinite Cabs \n Please wait while we are searching a closest driver for you." /*user.getMessage()*/, CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();
                                }
                            }
                        }, 10000);
                    } else if (user.getStatusCode().equals("203")) {
                        finddriver = "1";
                        dialog1.dismiss();
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("We are very sorry");
                        sweetAlertDialog.setContentText(" No driver is currently available in the area");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                                startActivity(new Intent(context, Dashboard.class));
                            }
                        });
                        sweetAlertDialog.show();
                    } else if (user.getStatusCode().equals("206")) {
                        finddriver = "1";
                        dialog1.dismiss();
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("We are very sorry");
                        sweetAlertDialog.setContentText(" No driver is currently available in the area");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                                startActivity(new Intent(context, Dashboard.class));
                            }
                        });
                        sweetAlertDialog.show();
                    } else {
                        dialog1.dismiss();
                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                } else {
                    dialog1.dismiss();
                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<Searching_Driver_Model> call, Throwable t) {
                dialog1.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    //    <-----------------------------booking cancel API  --------------------------------------------------------->
    public void Cancel_Booking(final Map<String, String> map) {
    /*  Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show(); */
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Cancel_Ride_Model> call = Apis.getAPIService().cancelRide(map);
        call.enqueue(new Callback<Cancel_Ride_Model>() {
            @Override
            public void onResponse(Call<Cancel_Ride_Model> call, Response<Cancel_Ride_Model> response) {
                dialog.dismiss();
                Cancel_Ride_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        cancelRideFromRequestAcceptedLayout();
                        startActivity(new Intent(getActivity(), Dashboard.class));
                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    } else {
                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                } else {
                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<Cancel_Ride_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    //    <------------------------- driver tracking to user------------------------------>
    public void Track_User_After_bookingaccept(final Map<String, String> map) {
       /* ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Loading");
        dialog.setMessage("Tracking a route....");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);*/
        Call<After_Accept_Driver_Track_User_Model> call = Apis.getAPIService().track_user(map);
        call.enqueue(new Callback<After_Accept_Driver_Track_User_Model>() {
            @Override
            public void onResponse(Call<After_Accept_Driver_Track_User_Model> call, Response<After_Accept_Driver_Track_User_Model> response) {
//                dialog.dismiss();
                After_Accept_Driver_Track_User_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        Log.d("sdhfg", "apihit");
                        switch (user.getData().getStatus()) {
                            case "Completed":
                                cantouch_main(true);
                                Map<String, String> map3 = new HashMap<>();
                                map3.put("bookingId", user.getData().getBooking_id());
                                CallriverApi(map3);
                                if (user.getData().getPayStatus().equals("2")) {
                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                                    sweetAlertDialog.setTitleText("Trip Completed");
                                    sweetAlertDialog.setContentText("App payment error occur. Please pay the Cash directly to the Driver.");
                                    sweetAlertDialog.setConfirmText("OK");
                                    sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                                    sweetAlertDialog.showCancelButton(false);
                                    sweetAlertDialog.setCancelable(false);
                                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.cancel();
                                            startActivity(new Intent(getActivity(), RateDriver.class));
                                        }
                                    });
                                    sweetAlertDialog.show();
                                } else {
                                    Map<String, String> map = new HashMap<>();
                                    map.put("bookingId", user.getData().getBooking_id());
                                    CallriverApirate(map);
                                }

                                break;
                            case "New":
                                cantouch_main(true);
                                cancelRideFromRequestAcceptedLayout();
                                startActivity(new Intent(getActivity(), Dashboard.class));
                                break;
                            case "Driver_Declined":
                                cantouch_main(true);
                                showmessage("Driver has been canceled your ride we are finding new cab for you");
                                booking_id = user.getData().getBooking_id();
                                // Create an Intent that will start the Menu-Activity.
                                try {
                                    dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.setContentView(R.layout.dialog_search);
                                    dialog1.setCancelable(false);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    Button btn_search = dialog1.findViewById(R.id.btn_search);
                                    btn_search.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog1.dismiss();
                                            finddriver = "1";
                                        /*    cancelRideFromRequestAcceptedLayout();
                                            startActivity(new Intent(getActivity(), Dashboard.class));*/
                                            Map<String, String> map = new HashMap<>();
                                            map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                            map.put("reason_id", "");
                                            map.put("driver_id", driverId);
                                            map.put("type", "user");
                                            map.put("note", "");
                                            map.put("booking_id", booking_id);
                                            Cancel_Booking(map);
                                        }
                                    });
                                    dialog1.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                                if (finddriver.equals("0")) {
//                                    Map<String, String> map1 = new HashMap<>();
//                                    map1.put("booking_id", user.getData().getBooking_id());
//                                    Searching_Driver(map1);
//                                    Map<String, String> map = new HashMap<>();
//                                    map.put("booking_id", booking_id);
//                                    Searching_Driver(map);
////                        CustomToast.makeText(getActivity(), "Thank you for choosing Infinite Cabs \n Please wait while we are searching a closest driver for you." /*user.getMessage()*/, CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();
//                                    countDownTimer = new CountDownTimer(300000/*60000*/, 1000) {
//                                        public void onTick(long millisUntilFinished) {
//                                            Log.d("asdfasdf",millisUntilFinished/1000+"");
//                                        }
//
//                                        public void onFinish() {
//                                            finddriver = "1";
//                                            dialog1.dismiss();
//                                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
//                                            sweetAlertDialog.setTitleText("We are very sorry");
//                                            sweetAlertDialog.setContentText(" No driver is currently available in the area");
//                                            sweetAlertDialog.setConfirmText("OK");
//                                            sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
//                                            sweetAlertDialog.showCancelButton(false);
//                                            sweetAlertDialog.setCancelable(false);
//                                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                @Override
//                                                public void onClick(SweetAlertDialog sDialog) {
//                                                    sDialog.cancel();
//                                        startActivity(new Intent(context, Dashboard.class));
//                                                }
//                                            });
//                                            sweetAlertDialog.show();
////                                CustomToast.makeText(getActivity(), "", CustomToast.LENGTH_LONG, CustomToast.ERROR, true).show();
//                                        }
//
//                                    }.start();
//                                }

                                break;
                            case "User_Declined":
                                cantouch_main(true);
                                /*try {
                                    dialog1.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/
                                cancelRideFromRequestAcceptedLayout();
                                startActivity(new Intent(getActivity(), Dashboard.class));
                                break;
                            case "Accepted":
                                cantouch_main(false);
                                tv_driverRating.setText(user.getData().getDriverRating());
                                MyRating.setRating(Float.parseFloat(user.getData().getDriverRating()));
                                driverId = user.getData().getDriver_id();
                                booking_id = user.getData().getBooking_id();
                                tv_eta.setText("ETA: " + user.getData().getEta() + "");
                                tv_cab_no.setText(user.getData().getDriverCar_No());
                                tv_name_driver.setText(user.getData().getDriverName());
                                Log.d("adsad", "https://admin.infinitecabs.com.au/" + user.getData().getDriverImage());
                                Log.d("adsad", "https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage());
                                if (imageload.equals("0")) {
                                    try {
                                      /*  Picasso.get().load("http://admin.infinitecabs.com.au/texiapp-new/driverimages/" + user.getData().getDriverImage())
                                                .error(R.drawable.user)
                                                .priority(Picasso.Priority.HIGH)
                                                .networkPolicy(NetworkPolicy.OFFLINE)
                                                .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);*/


                                        RequestOptions options = new RequestOptions()
                                                .placeholder(R.mipmap.ic_launcher_round)
                                                .error(R.mipmap.ic_launcher_round);


                                        Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getData().getDriverImage()).apply(options).into(civ_driver);
                                        imageload = "1";
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {
                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.mipmap.ic_launcher_round)
                                            .error(R.mipmap.ic_launcher_round);

                                    Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage())
                                            .apply(options).into(civ_cab);

                                 /*   Picasso.get().load("https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage())
                                            .error(R.drawable.car)
                                            .priority(Picasso.Priority.HIGH)
                                            .centerInside()
                                            .networkPolicy(NetworkPolicy.OFFLINE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_cab);*/
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                tv_destination.setText(user.getData().getDrop_address());
                                tv_pickup.setText(user.getData().getPick_address());

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Create an Intent that will start the Menu-Activity.
                                        Map<String, String> map = new HashMap<>();
                                        map.put("booking_id", user.getData().getBooking_id());
                                        map.put("driver_id", user.getData().getDriver_id());
                                        map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                        Track_User_After_bookingaccept(map);
//                                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                                    }
                                }, 8000);
                                LatLng latLng1 = new LatLng(Double.parseDouble(user.getData().getLatitude()), Double.parseDouble(user.getData().getLonglatitude()));
                                LatLng latLng3 = new LatLng(Double.parseDouble(user.getData().getDroplatitude()), Double.parseDouble(user.getData().getDroplonglatitude()));
//                                LatLng latLng3= new LatLng(Double.parseDouble(user.getData().getDroplonglatitude()), Double.parseDouble(user.getData().getDroplatitude()));
                                driverlat = Double.parseDouble(user.getData().getLatitude());
                                driverlong = Double.parseDouble(user.getData().getLonglatitude());
                                if (path.equals("0")) {
                                    mMap.clear();
                                    marker = null;
                                    blackPolyLine = null;
                                    path = "1";
                                }
                                MarkerOptions markerOptions1 = new MarkerOptions();
                                markerOptions1.position(latLng3);
                                int height1 = 150;
                                int width1 = 100;
                                BitmapDrawable bitmapdraw1 = (BitmapDrawable) getResources().getDrawable(R.drawable.source_marker_1);
                                Bitmap b1 = bitmapdraw1.getBitmap();
                                Bitmap smallMarker1 = Bitmap.createScaledBitmap(b1, width1, height1, false);
                                // markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                                markerOptions1.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1));
                                mMap.addMarker(markerOptions1);
                                String key  = getResources().getString(R.string.key1)+getResources().getString(R.string.key2)+getResources().getString(R.string.key3);
                                apiInterface.getDirections("driving", "less_driving",
                                        Double.parseDouble(user.getData().getDroplatitude()) + "," + Double.parseDouble(user.getData().getDroplonglatitude()), Double.parseDouble(user.getData().getLatitude()) + "," + Double.parseDouble(user.getData().getLonglatitude()),
                                        key)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                                new SingleObserver<Result>() {
                                                    @Override
                                                    public void onSubscribe(Disposable d) {
                                                    }
                                                    @Override
                                                    public void onSuccess(Result result) {
                                                        List<Route> routeList = result.getRoutes();
                                                        for (Route route : routeList) {
                                                            polyLine = route.getOverviewPolyline().getPoints();
                                                            polyLineList1 = decodePoly(polyLine);
//                                                                drawPolyLineAndAnimateCar();
                                                            if (marker == null) {
//                                        mMap.clear();
                                                                int height = 100;
                                                                int width = 100;
                                                                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.car1);
                                                                Bitmap b = bitmapdraw.getBitmap();
                                                                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);


                                                                marker = mMap.addMarker(new MarkerOptions().position(latLng1)
                                                                        .flat(true)
                                                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                                                                  /*  mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                                                            (new CameraPosition.Builder().target(latLng1)
                                                                                    .zoom(16).build()));*/
                                                            } else {
                                                                CarMoveAnim.startcarAnimation(marker, mMap, marker.getPosition(), new LatLng(driverlat, driverlong), 0, null);
                                                            }
                                                            drawPolyLineOnMap(polyLineList1);
                                                            path = "1";

                                                        }
                                                    }

                                                    @Override
                                                    public void onError(Throwable e) {
                                                        e.printStackTrace();
                                                    }
                                                });
                                break;
                            case "paymentPending":
                                cantouch_main(false);
//                                timerHandler.removeCallbacks(timerRunnable);
                                if (paystatus.equals("0")) {

                                    SweetAlertDialog sweetAlertDialog2 = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                                    sweetAlertDialog2.setTitleText("Infinite Cabs");
                                    sweetAlertDialog2.setContentText("Processing Payment");
                                    sweetAlertDialog2.setConfirmText("Wait");
                                    /*     sweetAlertDialog2.setCancelText("Cancel");*/
                                    sweetAlertDialog2.setCancelable(false);
                                    Log.d("jjhkjhkjk", "hkjhkjhkjhkjhkhhkh");
                                    /*      sweetAlertDialog2.showCancelButton(false);*/
                                    sweetAlertDialog2.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
//                                            sweetAlertDialog.cancel();

                                        }
                                    });

                                    if (sweetAlertDialog2.isShowing()) {

                                    } else {
                                        sweetAlertDialog2.show();
                                    }

                            /*        if (user.getData().getPaymentlink().isEmpty()) {
                                        SweetAlertDialog sweetAlertDialog2 = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                                        sweetAlertDialog2.setTitleText("Infinite Cabs");
                                        sweetAlertDialog2.setContentText("Processing Payment");
                                        sweetAlertDialog2.setConfirmText("Wait");
                                        *//*     sweetAlertDialog2.setCancelText("Cancel");*//*
                                        sweetAlertDialog2.setCancelable(false);
                                        Log.d("jjhkjhkjk", "hkjhkjhkjhkjhkhhkh");
                                        *//*      sweetAlertDialog2.showCancelButton(false);*//*
                                        sweetAlertDialog2.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
//                                            sweetAlertDialog.cancel();

                                            }
                                        });

                                        if (sweetAlertDialog2.isShowing()) {

                                        } else {
                                            sweetAlertDialog2.show();
                                        }
                                    }
                                    else {
                                        SweetAlertDialog sweetAlertDialog2 = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                        sweetAlertDialog2.setTitleText("Infinite Cabs");
                                        sweetAlertDialog2.setContentText(user.getMessage() + "");
                                        sweetAlertDialog2.setConfirmText("Pay");
                                      *//*      sweetAlertDialog2.setCancelText("Cancel");
                                            sweetAlertDialog2.showCancelButton(false);*//*
                                        sweetAlertDialog2.setCancelable(false);
                                        sweetAlertDialog2.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                if (user.getData().getPaymentlink().isEmpty()) {

//                                                    startActivity(new Intent(getActivity(), PaymntWebview.class).putExtra("url", "cash"));
                                                } else {
                                                    startActivity(new Intent(getActivity(), PaymntWebview.class).putExtra("url", user.getData().getPaymentlink()));
                                                }

                                                sweetAlertDialog2.cancel();
                                            }
                                        });
                                          *//*  sweetAlertDialog2.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.cancel();
                                                    paystatus = "1";
                                                }
                                            });*//*
                                        paystatus = "1";
                                    }*/
                                }
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Create an Intent that will start the Menu-Activity.
                                        Map<String, String> map = new HashMap<>();
                                        map.put("booking_id", user.getData().getBooking_id());
                                        map.put("driver_id", user.getData().getDriver_id());
                                        map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                        Track_User_After_bookingaccept(map);
//                                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                                    }
                                }, 20000);
                                break;
                            case "Start":
                                cantouch_main(false);
                                tv_driverRating.setText(user.getData().getDriverRating());
                                MyRating.setRating(Float.parseFloat(user.getData().getDriverRating()));
                                driverId = user.getData().getDriver_id();
                                booking_id = user.getData().getBooking_id();
                                tv_eta.setText("ETA: " + user.getData().getEta() + "");
                                tv_cab_no.setText(user.getData().getDriverCar_No());
                                tv_name_driver.setText(user.getData().getDriverName());
                                Log.d("adsad", "https://admin.infinitecabs.com.au/" + user.getData().getDriverImage());
                                Log.d("adsad", "https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage());
                                if (imageload.equals("0")) {
                                    try {
                                   /*     Picasso.get().load("http://admin.infinitecabs.com.au/texiapp-new/driverimages/" + user.getData().getDriverImage())
                                                .error(R.drawable.user)
                                                .priority(Picasso.Priority.HIGH)
                                                .networkPolicy(NetworkPolicy.OFFLINE)
                                                .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);*/
                                        RequestOptions options = new RequestOptions()
                                                .placeholder(R.mipmap.ic_launcher_round)
                                                .error(R.mipmap.ic_launcher_round);

                                        Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getData().getDriverImage()).apply(options).into(civ_driver);
                                        imageload = "1";
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                try {
                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.mipmap.ic_launcher_round)
                                            .error(R.mipmap.ic_launcher_round);

                                    Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage())
                                            .apply(options).into(civ_cab);

                                    /*Picasso.get().load("https://admin.infinitecabs.com.au/car_image/" + user.getData().getCarImage())
                                            .error(R.drawable.car)
                                            .priority(Picasso.Priority.HIGH)
                                            .centerInside()
                                            .networkPolicy(NetworkPolicy.OFFLINE)
                                            .into(civ_cab);*/
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                booking_id = user.getData().getBooking_id();
                                tv_eta.setText("ETA: " + user.getData().getEta() + "");
                                tv_destination.setText(user.getData().getDrop_address());
                                tv_pickup.setText(user.getData().getPick_address());

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Create an Intent that will start the Menu-Activity.
                                        Map<String, String> map = new HashMap<>();
                                        map.put("booking_id", user.getData().getBooking_id());
                                        map.put("driver_id", user.getData().getDriver_id());
                                        map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                        Track_User_After_bookingaccept(map);
//                                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                                    }
                                }, 20000);
                                LatLng latLng2 = new LatLng(Double.parseDouble(user.getData().getLatitude()), Double.parseDouble(user.getData().getLonglatitude()));
                                LatLng latLng4 = new LatLng(Double.parseDouble(user.getData().getDroplatitude()), Double.parseDouble(user.getData().getDroplonglatitude()));
//                                LatLng latLng3= new LatLng(Double.parseDouble(user.getData().getDroplonglatitude()), Double.parseDouble(user.getData().getDroplatitude()));
                                driverlat = Double.parseDouble(user.getData().getLatitude());
                                driverlong = Double.parseDouble(user.getData().getLonglatitude());
                                if (path1.equals("0")) {
                                    mMap.clear();
                                    marker = null;
                                    blackPolyLine = null;
                                    path1 = "1";
                                }
                                MarkerOptions markerOptions2 = new MarkerOptions();
                                markerOptions2.position(latLng4);
                                int height2 = 150;
                                int width2 = 100;
                                BitmapDrawable bitmapdraw2 = (BitmapDrawable) getResources().getDrawable(R.drawable.source_marker_1);
                                Bitmap b2 = bitmapdraw2.getBitmap();
                                Bitmap smallMarker2 = Bitmap.createScaledBitmap(b2, width2, height2, false);
                                // markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker_1));
                                markerOptions2.icon(BitmapDescriptorFactory.fromBitmap(smallMarker2));
                                mMap.addMarker(markerOptions2);
                                String key1  = getResources().getString(R.string.key1)+getResources().getString(R.string.key2)+getResources().getString(R.string.key3);
                                apiInterface.getDirections("driving", "less_driving",
                                        Double.parseDouble(user.getData().getDroplatitude()) + "," + Double.parseDouble(user.getData().getDroplonglatitude()), Double.parseDouble(user.getData().getLatitude()) + "," + Double.parseDouble(user.getData().getLonglatitude()),
                                        key1)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                                new SingleObserver<Result>() {
                                                    @Override
                                                    public void onSubscribe(Disposable d) {
                                                    }

                                                    @Override
                                                    public void onSuccess(Result result) {
                                                        List<Route> routeList = result.getRoutes();
                                                        for (Route route : routeList) {

                                                            polyLine = route.getOverviewPolyline().getPoints();
                                                            polyLineList1 = decodePoly(polyLine);
//                                                                drawPolyLineAndAnimateCar();
                                                            if (marker == null) {
//                                        mMap.clear();
                                                                int height = 100;
                                                                int width = 100;
                                                                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.car1);
                                                                Bitmap b = bitmapdraw.getBitmap();
                                                                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);


                                                                marker = mMap.addMarker(new MarkerOptions().position(latLng2)
                                                                        .flat(true)
                                                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                                                              /*  marker = mMap.addMarker(new MarkerOptions().position(latLng2)
                                                                        .flat(true)
                                                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.car1)));*/
                                                                  /*  mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                                                            (new CameraPosition.Builder().target(latLng1)
                                                                                    .zoom(16).build()));*/
                                                            } else {
                                                                CarMoveAnim.startcarAnimation(marker, mMap, marker.getPosition(), new LatLng(driverlat, driverlong), 0, null);
                                                            }
                                                            drawPolyLineOnMap(polyLineList1);
                                                            path1 = "1";

                                                        }
                                                    }

                                                    @Override
                                                    public void onError(Throwable e) {
                                                        e.printStackTrace();
                                                    }
                                                });
                                break;
                        }

                    } else {
                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                } else {
                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<After_Accept_Driver_Track_User_Model> call, Throwable t) {
//                dialog.dismiss();
                cantouch_main(true);
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    // driver status in background API-------------------------
    private void User_Status_Check_Background_Store(Map<String, String> map) {
        Call<User_Status_Background_Model> call = Apis.getAPIService().getUserStatus(map);
        call.enqueue(new Callback<User_Status_Background_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Status_Background_Model> call, Response<User_Status_Background_Model> response) {
                // dialog.dismiss();
                final User_Status_Background_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        Utility.setUrl(getActivity(),user.getStartCall());

                        switch (user.getBookingData().getStatus()) {
                            case "Accepted":
                                cantouch_main(false);
                                showCallDriverLayout1();
                                tv_driverRating.setText(user.getBookingData().getDriverRating());
                                MyRating.setRating(Float.parseFloat(user.getBookingData().getDriverRating()));
                                tv_name_driver.setText(user.getBookingData().getDriverName());
                                tv_cab_no.setText(user.getBookingData().getDriverCar_No());
                                tv_name_driver.setText(user.getBookingData().getDriverName());
                                Log.d("qwerty","https://admin.infinitecabs.com.au/car_image/" + user.getBookingData().getCarImage());
                                try {

                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.mipmap.ic_launcher_round)
                                            .error(R.mipmap.ic_launcher_round);

                                    Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/car_image/" + user.getBookingData().getCarImage())
                                            .apply(options).into(civ_cab);
                                   /* Picasso.get().load("https://admin.infinitecabs.com.au/car_image/" + user.getBookingData().getCarImage())
                                            .error(R.drawable.car)
                                            .priority(Picasso.Priority.HIGH)
                                            .centerInside()
                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_cab);*/
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                tv_cab_no.setText(user.getBookingData().getDriverCar_No());
                                tv_cab_model.setText(user.getBookingData().getDriverCar_Model());
                                try {
                                   /* Picasso.get().load("http://admin.infinitecabs.com.au/texiapp-new/driverimages/" + user.getBookingData().getDriverImage())
                                            .error(R.drawable.user)
                                            .priority(Picasso.Priority.HIGH)
                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);*/
                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.mipmap.ic_launcher_round)
                                            .error(R.mipmap.ic_launcher_round);

                                    Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getBookingData().getDriverImage()).apply(options).into(civ_driver);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // Create an Intent that will start the Menu-Activity.
                                Map<String, String> map = new HashMap<>();
                                map.put("booking_id", user.getBookingData().getBookingId());
                                map.put("driver_id", user.getBookingData().getDriverId());
                                map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                Track_User_After_bookingaccept(map);
//                                CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                                break;
                            case "New":
                                cantouch_main(true);
                                cancelRideFromRequestAcceptedLayout();
//                                startActivity(new Intent(getActivity(), Dashboard.class));
                                break;
                            case "Search":
                                try {
                                    Map<String, String> ma1 = new HashMap<>();
                                    ma1.put("booking_id", user.getBookingData().getBookingId());
                                    Searching_Driver(ma1);
                                    booking_id = user.getBookingData().getBookingId();
                                    dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.setContentView(R.layout.dialog_search);
                                    dialog1.setCancelable(false);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    Button btn_search = dialog1.findViewById(R.id.btn_search);
                                    btn_search.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog1.dismiss();
                                            finddriver = "1";
                                        /*    cancelRideFromRequestAcceptedLayout();
                                            startActivity(new Intent(getActivity(), Dashboard.class));*/
                                            Map<String, String> map = new HashMap<>();
                                            map.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                            map.put("reason_id", "");
                                            map.put("driver_id", driverId);
                                            map.put("type", "user");
                                            map.put("note", "");
                                            map.put("booking_id", booking_id);
                                            Cancel_Booking(map);
                                        }
                                    });
                                    dialog1.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                                startActivity(new Intent(getActivity(), Dashboard.class));
                                break;
                            case "Start":
                                cantouch_main(false);
                                showCallDriverLayout1();
                                tv_driverRating.setText(user.getBookingData().getUserstarrating());
                                MyRating.setRating(Float.parseFloat(user.getBookingData().getUserstarrating()));
                                tv_name_driver.setText(user.getBookingData().getDriverName());
                                tv_cab_no.setText(user.getBookingData().getDriverCar_No());
                                tv_cab_model.setText(user.getBookingData().getDriverCar_Model());
                                try {
                                   /* Picasso.get().load("asdf" + user.getBookingData().getDriverImage())
                                            .error(R.drawable.user)
                                            .priority(Picasso.Priority.HIGH)
                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(civ_driver);*/
                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.mipmap.ic_launcher_round)
                                            .error(R.mipmap.ic_launcher_round);

                                    Glide.with(getActivity()).load("https://admin.infinitecabs.com.au/" + user.getBookingData().getDriverImage()).apply(options).into(civ_driver);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // Create an Intent that will start the Menu-Activity.
                                Map<String, String> map1 = new HashMap<>();
                                map1.put("booking_id", user.getBookingData().getBookingId());
                                map1.put("driver_id", user.getBookingData().getDriverId());
                                map1.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                Track_User_After_bookingaccept(map1);
//                                CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                                break;
                            case "Completed":
                                cantouch_main(true);
                                Map<String, String> map2 = new HashMap<>();
                                map2.put("bookingId", user.getBookingData().getBookingId());
                                CallriverApirate(map2);
                                break;
                            case "paymentPending":
                                cantouch_main(true);
                                Map<String, String> map3 = new HashMap<>();
                                map3.put("booking_id", user.getBookingData().getBookingId());
                                map3.put("driver_id", user.getBookingData().getDriverId());
                                map3.put("user_id", user_data.get(UserSessionManager.KEY_ID));
                                Track_User_After_bookingaccept(map3);
                               /* if (user.getBookingData().getPaymentlink().isEmpty()) {

                                    Log.d("hdgfsd", "dlkfjgl");

                                }
                                else {
                                    Log.d("hdgfsd", "dlkfjgl");
                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                    sweetAlertDialog.setTitleText("Infinite Cabs");
                                    sweetAlertDialog.setContentText(user.getMessage() + "");
                                    sweetAlertDialog.setConfirmText("Pay");
                                    sweetAlertDialog.setCancelText("Cancel");
                                    sweetAlertDialog.showCancelButton(true);
                                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sweetAlertDialog.cancel();
                                            if (user.getBookingData().getPaymentlink().isEmpty()) {
                                                startActivity(new Intent(getActivity(), PaymntWebview.class).putExtra("url", "cash"));
                                            } else {
                                                startActivity(new Intent(getActivity(), PaymntWebview.class).putExtra("url", user.getBookingData().getPaymentlink()));
                                            }
                                        }
                                    });
                                    sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.cancel();
                                        }
                                    });
                                    if (sweetAlertDialog.isShowing()) {

                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                }*/
                                break;
                        }

                    } else {
                        statusManager_background_store_data.iseditor();
                        statusManager_background_store_data.createUserLoginSession("",
                                "", "",
                                "", "", "",
                                "", "", "",
                                "", "", "", ""
                                , "",
                                "", "",
                                "", "");
                    }

                }
            }

            @Override
            public void onFailure(Call<User_Status_Background_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //////Polyline and animation..................................................................>

    //aplypromocode
    private void applypromocode(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ApplyCouponModel> call = Apis.getAPIService().applypromocode(map);
        call.enqueue(new Callback<ApplyCouponModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<ApplyCouponModel> call, Response<ApplyCouponModel> response) {
                dialog.dismiss();
                ApplyCouponModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        oldprice.setVisibility(View.VISIBLE);
//                        oldprice.setText("Before PromoCode Price: $" + minfare+"-$" + maxprice);
                        CouponCode = et_coupon.getText().toString();
//                        CustomToast.makeText(getActivity(),userdata.getMessage(), CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();
                        tv_fareEstimation.setText(userdata.getNewprice() + "");
                        maxprice = userdata.getMaxprice() + "";
//                        tv_couponaply.setEnabled(false);
                        tv_couponaply.setText("Remove");
                        tv_couponaply.setTextColor(Color.RED);

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        sweetAlertDialog.setTitleText("Thank You");
                        sweetAlertDialog.setContentText("Congratulations! Promo Code has successfully applied.");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                            }
                        });

                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }

                    } else if (userdata.getStatusCode().equals("205")) {
                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_LONG, CustomToast.ERROR, true).show();
                    } else {
                        CustomToast.makeText(getActivity(), "Promo Code is invalid or expired", CustomToast.LENGTH_LONG, CustomToast.ERROR, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApplyCouponModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    //aplypromocode
    private void CallriverApi(Map<String, String> map) {
      /*  final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();*/
        Call<CallDriverModel> call = Apis.getAPIService1(getActivity()).sess_del(map);
        call.enqueue(new Callback<CallDriverModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<CallDriverModel> call, Response<CallDriverModel> response) {
//                dialog.dismiss();
                CallDriverModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {

                    } else {
                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CallDriverModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Callriver_newApi(Map<String, String> map) {
      /*  final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();*/
        Call<CallDriverModel> call = Apis.getAPIService1(getActivity()).sess_del_new(map);
        call.enqueue(new Callback<CallDriverModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<CallDriverModel> call, Response<CallDriverModel> response) {
//                dialog.dismiss();
                CallDriverModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {

                    } else {
                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CallDriverModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void CallriverApirate(Map<String, String> map) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        sweetAlertDialog.setTitleText("Thank you for using Infinite Cabs");
        sweetAlertDialog.setContentText("Your ride has successfully completed. Please rate your driver now.");
        sweetAlertDialog.setConfirmText("Driver Rating");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.check));
        sweetAlertDialog.showCancelButton(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sweetAlertDialog.cancel();
                startActivity(new Intent(getActivity(), RateDriver.class));
            }
        });

        if (sweetAlertDialog.isShowing()) {

        } else {
            sweetAlertDialog.show();
        }
      /*  final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<CallDriverModel> call = Apis.getAPIService().sess_del(map);
        call.enqueue(new Callback<CallDriverModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<CallDriverModel> call, Response<CallDriverModel> response) {
                dialog.dismiss();
                CallDriverModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        startActivity(new Intent(getActivity(), RateDriver.class));
                    } else {
                        Log.d("hdgfsd", "dlkfjgl");
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Infinite Cabs");
                        sweetAlertDialog.setContentText("Your ride has been successfully completed. Please leave your valuable feedback for driver.\n");
                        sweetAlertDialog.setConfirmText("Leave Feedback");
                       sweetAlertDialog.setCancelable(false);

                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sweetAlertDialog.cancel();
                                startActivity(new Intent(getActivity(), RateDriver.class));
                            }
                        });

                        if (sweetAlertDialog.isShowing()) {

                        } else {
                            sweetAlertDialog.show();
                        }


//                        CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CallDriverModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });*/
    }

    public void drawPolyLineOnMap_whenPlaceSearch(List<LatLng> list) {
//        mMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polyLineList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 350);
        mMap.animateCamera(mCameraUpdate);


        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.width(10);
        lineOptions.color(getResources().getColor(R.color.colorAccent));
        lineOptions.startCap(new SquareCap());
        lineOptions.endCap(new SquareCap());
        lineOptions.jointType(ROUND);
        blackPolyLine = mMap.addPolyline(lineOptions);
//        blackPolyLine.remove();

        PolylineOptions greyOptions = new PolylineOptions();
        greyOptions.width(10);
        greyOptions.color(Color.BLACK);
        greyOptions.startCap(new SquareCap());
        greyOptions.endCap(new SquareCap());
        greyOptions.jointType(ROUND);
        int height1 = 150;
        int width1 = 100;
        BitmapDrawable bitmapdraw1 = (BitmapDrawable) getResources().getDrawable(R.drawable.destination_marker_1);
        Bitmap b1 = bitmapdraw1.getBitmap();
        Bitmap smallMarker1 = Bitmap.createScaledBitmap(b1, width1, height1, false);
        //  markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker));
//        end marker
        mMap.addMarker(new MarkerOptions()
                .position(polyLineList.get(polyLineList.size() - 1)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)).title("Drop Point"));
        greyPolyLine = mMap.addPolyline(greyOptions);
//        greyPolyLine = mMap.upd(greyOptions);

//        greyPolyLine.remove();
        animatePolyLine(list);
    }

    public void drawPolyLineOnMap(List<LatLng> list) {
//        mMap.clear();
        if (blackPolyLine == null) {
            Log.d("iiresult", "already add");
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : polyLineList1) {
                builder.include(latLng);
            }
            LatLngBounds bounds = builder.build();
            CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 350);
            mMap.animateCamera(mCameraUpdate);

            lineOptions = new PolylineOptions();
            lineOptions.width(10);
            lineOptions.color(getResources().getColor(R.color.colorAccent));
            lineOptions.startCap(new SquareCap());
            lineOptions.endCap(new SquareCap());
            lineOptions.jointType(ROUND);
//        blackPolyLine.remove();

            PolylineOptions greyOptions = new PolylineOptions();
            greyOptions.width(10);
            greyOptions.color(Color.BLACK);

            greyOptions.startCap(new SquareCap());
            greyOptions.endCap(new SquareCap());
            greyOptions.jointType(ROUND);
            for (int i = 0; i < list.size(); i++) {
                lineOptions.add(list.get(i));
//                greyOptions.add(list.get(i));
            }
            blackPolyLine = mMap.addPolyline(lineOptions);
//            greyPolyLine = mMap.addPolyline(greyOptions);
//        greyPolyLine = mMap.upd(greyOptions);

//        greyPolyLine.remove();
        } else {
            if (blackPolyLine != null) {
                blackPolyLine.setPoints(list);
            }

        }
//        Collections.reverse(list);
//        animatePolyLine(list);
    }

    private void animatePolyLine(List<LatLng> list) {
        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
        animator.setDuration(3000);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                List<LatLng> latLngList = new ArrayList<>();
                latLngList = blackPolyLine.getPoints();
                int initialPointSize = latLngList.size();
                int animatedValue = (int) animator.getAnimatedValue();
                int newPoints = (animatedValue * list.size()) / 100;
                if (initialPointSize < newPoints) {
                    latLngList.addAll(list.subList(initialPointSize, newPoints));
                    blackPolyLine.setPoints(latLngList);
                }
            }
        });
        animator.addListener(polyLineAnimationListener);
        animator.start();
    }

    private void drawPolyLineAndAnimateCarSource_DestinationLine() {
        //Adjusting bounds
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polyLineList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 350);
        mMap.animateCamera(mCameraUpdate);

        polylineOptions = new PolylineOptions();
        polylineOptions.color(getResources().getColor(R.color.colorAccent));
        polylineOptions.width(10);
        polylineOptions.startCap(new SquareCap());
        polylineOptions.endCap(new SquareCap());
        polylineOptions.jointType(ROUND);
        polylineOptions.addAll(polyLineList);
        greyPolyLine = mMap.addPolyline(polylineOptions);

        blackPolylineOptions = new PolylineOptions();
        blackPolylineOptions.width(10);
        blackPolylineOptions.color(getResources().getColor(R.color.colorAccent));
        blackPolylineOptions.startCap(new SquareCap());
        blackPolylineOptions.endCap(new SquareCap());
        blackPolylineOptions.jointType(ROUND);
        blackPolyline = mMap.addPolyline(blackPolylineOptions);

        int height1 = 150;
        int width1 = 100;
        BitmapDrawable bitmapdraw1 = (BitmapDrawable) getResources().getDrawable(R.drawable.destination_marker_1);
        Bitmap b1 = bitmapdraw1.getBitmap();
        Bitmap smallMarker1 = Bitmap.createScaledBitmap(b1, width1, height1, false);
        //  markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker));
//        end marker
        mMap.addMarker(new MarkerOptions()
                .position(polyLineList.get(polyLineList.size() - 1)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)));

        ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
        polylineAnimator.setDuration(2000);
        polylineAnimator.setInterpolator(new LinearInterpolator());
        polylineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                List<LatLng> points = greyPolyLine.getPoints();
                int percentValue = (int) valueAnimator.getAnimatedValue();
                int size = points.size();
                int newPoints = (int) (size * (percentValue / 100.0f));
                List<LatLng> p = points.subList(0, newPoints);
                blackPolyline.setPoints(p);
            }
        });
        polylineAnimator.start();
//        marker = mMap.addMarker(new MarkerOptions().position(sydney)
//                .flat(true)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car1)));
        handler = new Handler();
        index = -1;
        next = 1;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                blackPolyLine.remove();
                if (index < polyLineList.size() - 1) {
                    index++;
                    next = index + 1;
                }
                if (index < polyLineList.size() - 1) {
                    startPosition = polyLineList.get(index);
                    endPosition = polyLineList.get(next);
                }
//                if (index == 0) {
//                    //BeginJourneyEvent beginJourneyEvent = new BeginJourneyEvent();
//                   // beginJourneyEvent.setBeginLatLng(startPosition);
//                    //JourneyEventBus.getInstance().setOnJourneyBegin(beginJourneyEvent);
//                }
//                if (index == polyLineList.size() - 1) {
//                    //EndJourneyEvent endJourneyEvent = new EndJourneyEvent();
//                   // endJourneyEvent.setEndJourneyLatLng(new LatLng(polyLineList.get(index).latitude,
//                    //        polyLineList.get(index).longitude));
//                  //  JourneyEventBus.getInstance().setOnJourneyEnd(endJourneyEvent);
//                }
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(2000);
                valueAnimator.setInterpolator(new LinearInterpolator());
//                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                    @Override
//                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                        v = valueAnimator.getAnimatedFraction();
//                        //  WHEN CAR UPDATE LAT/LNG THIS CODE COMMENT IT
//                        lng = v * endPosition.longitude + (1 - v)
//                                * startPosition.longitude;
//                        lat = v * endPosition.latitude + (1 - v)
//                                * startPosition.latitude;
//                        //  WHEN CAR UPDATE LAT/LNG THIS CODE COMMENT IT
//                        LatLng newPos = new LatLng(lat, lng);
//                        // CustomToast.makeText(getActivity(), "LAT"+newPos, CustomToast.LENGTH_SHORT, CustomToast.ERROR,true).show();
//                        CurrentJourneyEvent currentJourneyEvent = new CurrentJourneyEvent();
//                        currentJourneyEvent.setCurrentLatLng(newPos);
//                        JourneyEventBus.getInstance().setOnJourneyUpdate(currentJourneyEvent);
//                        marker.setPosition(newPos);
//                        marker.setAnchor(0.5f, 0.5f);
//                        marker.setRotation(getBearing(startPosition, newPos));
//                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition
//                                (new CameraPosition.Builder().target(newPos)
//                                        .zoom(15.5f).build()));
//                    }
//                });
//                valueAnimator.start();
                if (index != polyLineList.size() - 1) {
                    handler.postDelayed(this, 30000);
                }
            }
        }, 3000);
    }    //Pickup and Destination enter then catr list come in recycleview adapter-------------

    public class CarList_Adapter extends RecyclerView.Adapter<CarList_Adapter.MyViewHolder> {


        public CarList_Adapter() {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.list_item, parent, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {

            holder.tv_serviceType.setText(array_get_car_search_data.get(i).getCartype());
            holder.tv_carfare.setText("$" + array_get_car_search_data.get(i).getMin_rate() + "-" + "$" + array_get_car_search_data.get(i).getMax_rate());
            if (array_get_car_search_data.get(i).getSeat_capacity().equals("0")) {
                holder.tv_passengerCount.setVisibility(View.GONE);
            } else {
                holder.tv_passengerCount.setText(array_get_car_search_data.get(i).getSeat_capacity() + " Seater");
            }

            Picasso.get().load(array_get_car_search_data.get(i).getCarimage())
                    .priority(Picasso.Priority.HIGH)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(holder.iv_car);

            holder.listItem_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    row_index = i;
                    position = true;
                    notifyDataSetChanged();
                    tv_serviceType = (array_get_car_search_data.get(i).getCartype());
                    minfare = array_get_car_search_data.get(i).getMin_rate() + "";
                    maxprice = array_get_car_search_data.get(i).getMax_rate() + "";
                    tv_carfare = (/*"$" + array_get_car_search_data.get(i).getMin_rate() + "-" + */"-$" + array_get_car_search_data.get(i).getMax_rate());
                    if (array_get_car_search_data.get(i).getSeat_capacity().equals("0")) {
                        tv_passengerCount1.setVisibility(View.GONE);
                    } else {
                        tv_passengerCount = (array_get_car_search_data.get(i).getSeat_capacity() + " Seater");
                        tv_passengerCount1.setVisibility(View.VISIBLE);
                        tv_carmKM.setText(array_get_car_search_data.get(i).getKm() + " KM");
                        tv_carmKM.setVisibility(View.VISIBLE);
                    }
                    car_image = array_get_car_search_data.get(i).getCarimage();
                    cab_id = array_get_car_search_data.get(i).getCab_type_id();
                    car_type.setText(tv_serviceType);
                    choose_ride_rel.setBackground(ContextCompat.getDrawable(context, R.drawable.background5));
                    tv_passengerCount1.setText(tv_passengerCount);
                    Picasso.get().load(car_image)
                            .error(R.drawable.car_1)
                            .priority(Picasso.Priority.HIGH)
                            .networkPolicy(NetworkPolicy.NO_CACHE).into(iv_car);
                    tv_carfare1.setText("$" + minfare + "-" + "$" + maxprice);
                    tv_carfare1.setVisibility(View.VISIBLE);

                    if (sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                        sheetBehavior.setState(STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
            });

            if (position) {
                if (row_index == i) {
                    //holder.catalogue_items.setBackgroundColor(Color.parseColor("#BCBCBC"));
                    holder.listItem_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.background5));
                } else {
                    holder.listItem_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.background_9));
                }
            } else {
            }
        }

        @Override
        public int getItemCount() {
            return array_get_car_search_data.size();
        }

        public long getItemId(int position) {
            return position;
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private final ImageView iv_car;
            private final TextView tv_serviceType;
            private final TextView tv_passengerCount;
            private final TextView tv_carfare;
            private final RelativeLayout listItem_layout;

            private MyViewHolder(View v) {
                super(v);
                this.iv_car = itemView.findViewById(R.id.iv_car);
                this.tv_serviceType = itemView.findViewById(R.id.tv_serviceType);
                this.tv_passengerCount = itemView.findViewById(R.id.tv_passengerCount);
                this.tv_carfare = itemView.findViewById(R.id.tv_carfare);
                listItem_layout = itemView.findViewById(R.id.listItem_layout);
            }
        }
    }
}
