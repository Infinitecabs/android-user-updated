package com.cab_taxi.infinitecabs.DrawerActivities;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Model.Card_List;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.Payment.AddCard;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyCards extends AppCompatActivity implements View.OnClickListener {

    private WebView wevView;
    private ImageView iv_back;
    private LinearLayout llerror;
    private RelativeLayout header;
    private TextView tv_addcredit;
    private RecyclerView card_list;
    private Button btn_addPaymentMethod;
    private NestedScrollView nestedScrollView;
    private AppCompatTextView no_data_found;
    List<Card_List.Data>array_card_list;
    Card_ListAdapter card_listAdapter;
    UserSessionManager session;
    HashMap<String, String> user;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_cards);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(MyCards.this).build();

        initView();
        iv_back = (ImageView) findViewById(R.id.iv_back);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

    }
    NoInternetDialog noInternetDialog;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    private void initView() {
        header = (RelativeLayout) findViewById(R.id.header);
        llerror =  findViewById(R.id.llerror);
        no_data_found =  findViewById(R.id.no_data_found);
        tv_addcredit = (TextView) findViewById(R.id.tv_addcredit);
        card_list = (RecyclerView) findViewById(R.id.card_list);
        btn_addPaymentMethod = (Button) findViewById(R.id.btn_addPaymentMethod);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        btn_addPaymentMethod.setOnClickListener(this);
        Map<String, String> map = new HashMap<>();
        //(email,password)
        map.put("user_id", user.get(UserSessionManager.KEY_ID));
        Card_Lists(map);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addPaymentMethod:
                startActivity(new Intent(MyCards.this, AddCard.class)
                .putExtra("cardform","My Cards"));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

        }
    }

    public class Card_ListAdapter extends RecyclerView.Adapter<Card_ListAdapter.MyViewHolder> {


        public Card_ListAdapter() {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_payment_mode, parent, false);

            return new MyViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(@NonNull final Card_ListAdapter.MyViewHolder holder, final int i) {
          /*  holder.tv_expiryDate.setText(array_card_list.get(i).getExpairy_date());
            holder.tv_card_number.setText(array_card_list.get(i).getCard_number());
*/
        }

        @Override
        public int getItemCount() {
            return array_card_list.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            private LinearLayout ll_payment_item;
            private LinearLayout ll_card;
            private ImageView iv_cardtype;
            private TextView tv_card_number;
            private TextView tv_expiryDate;

            public MyViewHolder(View v) {
                super(v);
                this.iv_cardtype = itemView.findViewById(R.id.iv_cardtype);
                this.ll_payment_item = itemView.findViewById(R.id.ll_payment_item);
                this.ll_card = itemView.findViewById(R.id.ll_card);
                this.tv_card_number = itemView.findViewById(R.id.tv_card_number);
                this.tv_expiryDate = itemView.findViewById(R.id.tv_expiryDate);
            }
        }

    }
    private void Card_Lists(Map<String, String> map) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Card_List> call = Apis.getAPIService().getCardList(map);
        call.enqueue(new Callback<Card_List>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<Card_List> call, Response<Card_List> response) {
                dialog.dismiss();
                Card_List userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {

                       /* if (userdata.getData().isEmpty()){
                            llerror.setVisibility(View.VISIBLE);
                            no_data_found.setVisibility(View.VISIBLE);
                            no_data_found.setText("Data Not Found");
                            card_listAdapter.notifyDataSetChanged();
                        }else {
                            llerror.setVisibility(View.GONE);
                            array_card_list = new ArrayList<>();
                            array_card_list.addAll(userdata.getData());
                            card_listAdapter = new Card_ListAdapter();
                            card_list.setAdapter(card_listAdapter);
                            card_listAdapter.notifyDataSetChanged();
                        }
*/


                    } else {

//                        swipeToRefresh.setVisibility(View.GONE);
                        // CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<Card_List> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
}
