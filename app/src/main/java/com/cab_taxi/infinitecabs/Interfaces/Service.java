package com.cab_taxi.infinitecabs.Interfaces;

import com.cab_taxi.infinitecabs.Model.After_Accept_Driver_Track_User_Model;
import com.cab_taxi.infinitecabs.Model.ApplyCouponModel;
import com.cab_taxi.infinitecabs.Model.Booking_Car_Model;
import com.cab_taxi.infinitecabs.Model.CallDriverModel;
import com.cab_taxi.infinitecabs.Model.Cancel_Ride_Model;
import com.cab_taxi.infinitecabs.Model.CancelreasonListModel;
import com.cab_taxi.infinitecabs.Model.Card_List;
import com.cab_taxi.infinitecabs.Model.Contact_Us_Model;
import com.cab_taxi.infinitecabs.Model.Dasboard_Car_List_Model;
import com.cab_taxi.infinitecabs.Model.DeleteCardModel;
import com.cab_taxi.infinitecabs.Model.EmailVaildetionModel;
import com.cab_taxi.infinitecabs.Model.Faviourate_Place_Model;
import com.cab_taxi.infinitecabs.Model.ForgetPasswordModel;
import com.cab_taxi.infinitecabs.Model.Insert_Card_Model;
import com.cab_taxi.infinitecabs.Model.LoginModel;
import com.cab_taxi.infinitecabs.Model.Notification_ON_OFF_Model;
import com.cab_taxi.infinitecabs.Model.OtpVerifiedModel;
import com.cab_taxi.infinitecabs.Model.Password_Change_Model;
import com.cab_taxi.infinitecabs.Model.Profile_Model;
import com.cab_taxi.infinitecabs.Model.PromoCodeModel;
import com.cab_taxi.infinitecabs.Model.ResendOtpModel;
import com.cab_taxi.infinitecabs.Model.Save_address_Model;
import com.cab_taxi.infinitecabs.Model.Searching_Driver_Model;
import com.cab_taxi.infinitecabs.Model.SignUpModel;
import com.cab_taxi.infinitecabs.Model.UserUpcommingRideModel;
import com.cab_taxi.infinitecabs.Model.User_Cancel_Ride_Model;
import com.cab_taxi.infinitecabs.Model.User_Complete_Ride_Model;
import com.cab_taxi.infinitecabs.Model.User_Dasboard_Search_Car_List_Model;
import com.cab_taxi.infinitecabs.Model.User_EditProfile_Model;
import com.cab_taxi.infinitecabs.Model.User_Lat_Lng_Send_Model;
import com.cab_taxi.infinitecabs.Model.User_Status_Background_Model;
import com.cab_taxi.infinitecabs.Model.User_Submit_Rating_Model;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Service {
    @FormUrlEncoded
    @POST("/web_service/login")
    Call<LoginModel> Loginapi(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/sign_up")
    Call<SignUpModel> SignUpApi(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/emailExists")
    Call<EmailVaildetionModel> emailExists(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/ResendOtp")
    Call<ResendOtpModel> ResendOtp(
            @FieldMap() Map<String, String> parem
    ); @FormUrlEncoded
    @POST("/web_service/ResendOtpforPhone")
    Call<ResendOtpModel> ResendOtpforPhone(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/verifyOtp")
    Call<OtpVerifiedModel> verifyOtp(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/verifyOtpForPhone")
    Call<OtpVerifiedModel> verifyOtpForPhone(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/forgot_password")
    Call<ForgetPasswordModel> FogotPasswordApi(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/veriftOtpforpassword")
    Call<ForgetPasswordModel> veriftOtpforpassword(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/getUserProfile")
    Call<Profile_Model> getUserProfile(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/userCompleteRide")
    Call<User_Complete_Ride_Model> getCompleteRide(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/userCancelRide")
    Call<User_Cancel_Ride_Model> getCancelRide(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/serchUserTexi")
    Call<Dasboard_Car_List_Model> getCarList(
            @FieldMap() Map<String, String> parem
    );

    @Multipart
    @POST("/web_service/profile_edit")
    Call<User_EditProfile_Model> Update_profile(@Part MultipartBody.Part user_id,
                                                @Part MultipartBody.Part first_name,
                                                @Part MultipartBody.Part last_name,
                                                @Part MultipartBody.Part email,
                                                @Part MultipartBody.Part mobile,
                                                @Part MultipartBody.Part phoneCode,
                                                @Part MultipartBody.Part country,
                                                @Part MultipartBody.Part isdevice,
                                                @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST("/web_service/serchUserTexiAmount")
    Call<User_Dasboard_Search_Car_List_Model> searchCarList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/addUserLocation")
    Call<Save_address_Model> saveaddress_data(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/boookingCab")
    Call<Booking_Car_Model> booking_car(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/locationList")
    Call<Faviourate_Place_Model> getFavouritePlace(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/deleteLocation")
    Call<Faviourate_Place_Model> Delete_address(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/declinedBookings")
    Call<Cancel_Ride_Model> cancelRide(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/contactus")
    Call<Contact_Us_Model> submit_contact(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/searchDriver")
    Call<Searching_Driver_Model> searchdeiver(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/get_user_driver_latlong")
    Call<After_Accept_Driver_Track_User_Model> track_user(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/change_password")
    Call<Password_Change_Model> changepassword(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/notification_update")
    Call<Notification_ON_OFF_Model> notification(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/driverFeedback")
    Call<User_Submit_Rating_Model> submit_rating(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/getcardlist")
    Call<Card_List> getCardList(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/locationList")
    Call<Card_List> locationList(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/insertCard")
    Call<Insert_Card_Model> getInsertCard(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/CheckRequestuser")
    Call<User_Status_Background_Model> getUserStatus(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/Userlivelatlong")
    Call<User_Lat_Lng_Send_Model> SendLatLng(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/userUpcommingRide")
    Call<UserUpcommingRideModel> userUpcommingRide(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/deleteCard")
    Call<DeleteCardModel> deleteCard(
            @FieldMap() Map<String, String> parem
    );

    @FormUrlEncoded
    @POST("/web_service/applypromocode")
    Call<ApplyCouponModel> applypromocode(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/web_service/CheckRequestuserSide")
    Call<CallDriverModel> CheckRequestuserSide(
            @FieldMap() Map<String, String> parem
    );
    @FormUrlEncoded
    @POST("/twiCall/session_delete.php")
    Call<CallDriverModel> sess_del(
            @FieldMap() Map<String, String> parem
    );@FormUrlEncoded
    @POST("/twiCall/callconnect_deepak_number.php")
    Call<CallDriverModel> sess_del_new(
            @FieldMap() Map<String, String> parem
    );
    @GET("/web_service/CancelreasonList")
    Call<CancelreasonListModel> CancelreasonList();

    @GET("/web_service/promocodeList")
    Call<PromoCodeModel> promocodeList(
    );
//
//    @FormUrlEncoded
//    @POST("/lalsacademy/Webservice/activityDetail")
//    Call<myActivityDetailModel> ActivityDetail(
//            @FieldMap() Map<String, String> parem
//    );


}
