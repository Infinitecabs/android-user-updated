package com.cab_taxi.infinitecabs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Model.CancelreasonListModel;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancleRideReason extends AppCompatActivity implements View.OnClickListener {

    List<CancelreasonListModel.Data> cancellist;
    private ImageView iv_back;
    private CardView header1;
    private RecyclerView recyclercancelreason;
    private SwipeRefreshLayout swipeToRefresh;
    private AppCompatTextView no_data_found;
    private Button btn_cancelRide;

    UserSessionManager session;
    HashMap<String, String> user_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancle_ride_reason);
        session = new UserSessionManager(CancleRideReason.this);
        user_data = session.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(CancleRideReason.this).build();
        initView();
    }
    NoInternetDialog noInternetDialog;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","jh");
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });
        header1 = findViewById(R.id.header1);
        recyclercancelreason = findViewById(R.id.recyclercancelreason);
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        no_data_found = findViewById(R.id.no_data_found);
//        recyclercancelreason.setAdapter(new CancelAdapter());
        CancelreasonList();
        btn_cancelRide = (Button) findViewById(R.id.btn_cancelRide);
        btn_cancelRide.setOnClickListener(this);
    }

    private void CancelreasonList() {
        final Dialog dialog = new Dialog(CancleRideReason.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<CancelreasonListModel> call = Apis.getAPIService().CancelreasonList();
        call.enqueue(new Callback<CancelreasonListModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<CancelreasonListModel> call, Response<CancelreasonListModel> response) {
                dialog.dismiss();
                CancelreasonListModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().isEmpty()) {
                            no_data_found.setVisibility(View.VISIBLE);
                            no_data_found.setText(userdata.getMessage());
                            swipeToRefresh.setVisibility(View.GONE);
                        } else {
                            no_data_found.setVisibility(View.GONE);
                            swipeToRefresh.setVisibility(View.VISIBLE);
                            cancellist = new ArrayList<>();
                            cancellist.addAll(userdata.getData());
                            CancelAdapter completedAdapter = new CancelAdapter();
                            recyclercancelreason.setAdapter(completedAdapter);
                            completedAdapter.notifyDataSetChanged();
                        }
                    } else {
                        no_data_found.setVisibility(View.VISIBLE);
                        no_data_found.setText(userdata.getMessage());
                        swipeToRefresh.setVisibility(View.GONE);
                        //   CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CancelreasonListModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancelRide:

                break;
        }
    }

    //    <-----------------------------booking car cancel code  --------------------------------------------------------->
//    public void Cancel_Booking(final Map<String, String> map) {
//    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
//        dialog.show();  */
//        final Dialog dialog = new Dialog(CancleRideReason.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Call<Cancel_Ride_Model> call = Apis.getAPIService().cancelRide(map);
//        call.enqueue(new Callback<Cancel_Ride_Model>() {
//            @Override
//            public void onResponse(Call<Cancel_Ride_Model> call, Response<Cancel_Ride_Model> response) {
//                dialog.dismiss();
//                Cancel_Ride_Model user = response.body();
//                if (user != null) {
//                    if (user.getStatusCode().equals("200")) {
//                        cancelRideFromRequestAcceptedLayout();
//                        startActivity(new Intent(getActivity(), Dashboard.class));
//                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
//                    } else {
//                        CustomToast.makeText(getActivity(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
//                    }
//                } else {
//                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Cancel_Ride_Model> call, Throwable t) {
//                dialog.dismiss();
//                Log.d("response", "gh" + t.getMessage());
//            }
//        });
//    }





    public class CancelAdapter extends RecyclerView.Adapter<CancelAdapter.MyViewHolder> {

        public CancelAdapter() {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(CancleRideReason.this).inflate(R.layout.reason, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {
            holder.tv_reason.setText(cancellist.get(i).getReason_title());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (i==cancellist.size()-1){
                        showDialog1(cancellist.get(i).getReason_id());
                        Log.d("resaon_position",i+"");
                    }else{
                        Log.d("resaon_position",i+"234");
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result",cancellist.get(i).getReason_id());
                        returnIntent.putExtra("reason","");
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return cancellist.size();
        }

        public long getItemId(int position) {
            return position;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_reason;

            public MyViewHolder(View v) {
                super(v);
                tv_reason = v.findViewById(R.id.tv_reason);


            }
        }

    }

    private void showDialog1(String id) {
        final Dialog dialog = new Dialog(CancleRideReason.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_resason);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
      EditText et_noteDialog = dialog.findViewById(R.id.et_noteDialog);
      Button  btn_proceedDialog = dialog.findViewById(R.id.btn_proceedDialog);
        Button  btn_cancelDialog = dialog.findViewById(R.id.btn_cancelDialog);
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                if (source.length() > 0 && Character.isWhitespace(source.charAt(0)) && et_noteDialog.getText().toString().length() == 0) {
                    return "";
                }
                return source;
            }
        };
        et_noteDialog.setFilters(new InputFilter[]{filter});

        btn_proceedDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",id);
                returnIntent.putExtra("reason",et_noteDialog.getText().toString());
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        btn_cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
