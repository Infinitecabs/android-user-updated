package com.cab_taxi.infinitecabs.LoginSignUp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.Model.ForgetPasswordModel;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.mukesh.OtpView;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    String UserId = "";
    NoInternetDialog noInternetDialog;
    private ImageView iv_back;
    private TextView tv_login;
    private EditText et_email;
    private ImageView iv_clear;
    private Button btn_submit;
    private RelativeLayout header;
    private TextInputLayout textInputLayout;
    private OtpView otpView;
    CountryCodePicker countryCodePicker;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        noInternetDialog = new NoInternetDialog.Builder(ForgotPassword.this).build();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_login = findViewById(R.id.tv_login);
        et_email = findViewById(R.id.et_email);
        iv_clear = findViewById(R.id.iv_clear);
        btn_submit = findViewById(R.id.btn_submit);
        countryCodePicker = findViewById(R.id.ccp);
        iv_back.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        iv_clear.setOnClickListener(this);
        btn_submit.setOnClickListener(this);


        et_email.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    iv_clear.setVisibility(View.VISIBLE);
                else {
                    iv_clear.setVisibility(View.GONE);
                }
            }
        });
        header = (RelativeLayout) findViewById(R.id.header);
        header.setOnClickListener(this);
        textInputLayout = (TextInputLayout) findViewById(R.id.textInputLayout);
        textInputLayout.setOnClickListener(this);
        otpView = (OtpView) findViewById(R.id.otpView);
        otpView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_login:
                startActivity(new Intent(ForgotPassword.this, Login.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.iv_clear:
                et_email.setText("");
                break;

            case R.id.btn_submit:
                if (UserId.isEmpty())
                    submit();
                else
                    submit1();
                break;
        }
    }

    private void submit() {
        // validate
        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            CustomToast.makeText(getApplicationContext(), "Please Enter Your Mobile", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
//            Toast.makeText(this, "Please Enter Your Mobile", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("dsfasdf",email.length()+"");
        if (!Utility.isValidMobile(email)) {
            CustomToast.makeText(getApplicationContext(), "Please enter valid mobile no.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("phone", email);
        map.put("phoneCode", countryCodePicker.getSelectedCountryCodeWithPlus());
        map.put("country", countryCodePicker.getSelectedCountryEnglishName());
        FogotPasswordApi(map);
//       startActivity(new Intent(ForgotPassword.this, Verification.class));
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    private void submit1() {
        // validate
        String otp = otpView.getText().toString().trim();
        if (TextUtils.isEmpty(otp)) {
            Toast.makeText(this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("otp", otp);
        map.put("userId", UserId);
        veriftOtpforpassword(map);
//       startActivity(new Intent(ForgotPassword.this, Verification.class));
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void FogotPasswordApi(final Map<String, String> map) {
        final Dialog dialog = new Dialog(ForgotPassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ForgetPasswordModel> call = Apis.getAPIService().FogotPasswordApi(map);

        call.enqueue(new Callback<ForgetPasswordModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordModel> call, Response<ForgetPasswordModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                ForgetPasswordModel user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {


                        otpView.setVisibility(View.VISIBLE);
                        UserId = user.getUserId() + "";
                        et_email.setEnabled(false);
                        iv_clear.setEnabled(false);
                    /*    new SweetAlertDialog(ForgotPassword.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Infinite Cabs")
                                .setContentText(user.getMessage() + "")
                                .setConfirmText("OK!")
                                .showCancelButton(false)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                        finish();
                                    }
                                })
                                .show();*/


                    } else {

                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                    }

                } else {

                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public void veriftOtpforpassword(final Map<String, String> map) {
        final Dialog dialog = new Dialog(ForgotPassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ForgetPasswordModel> call = Apis.getAPIService().veriftOtpforpassword(map);

        call.enqueue(new Callback<ForgetPasswordModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordModel> call, Response<ForgetPasswordModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                ForgetPasswordModel user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {

                        SweetAlertDialog sweetAlertDialog= new SweetAlertDialog(ForgotPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Infinite Cabs");
                        sweetAlertDialog.setContentText("We sent your new password via SMS to mobile number you entered.");
                        sweetAlertDialog.setConfirmText("OK");
                        sweetAlertDialog.showCancelButton(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                        finish();
                                    }
                                });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();


                    } else {

                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                    }

                } else {

                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = ForgotPassword.this.findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }
}
