package com.cab_taxi.infinitecabs.LoginSignUp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.Terms_and_conditions;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.Model.SignUpModel;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    public static final String MyPREFERENCES = "Firbase_Token";
    public String Token;
    UserSessionManager session;
    SharedPreferences sharedpreferences;
    CountryCodePicker countryCodePicker;
    private ImageView iv_back;
    private TextView tv_login, tvterm;
    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_email;
    private EditText et_mobileNo;
    private EditText et_password;
    private EditText et_repassword;
    private EditText et_address;
    private CheckBox checkbox;
    private Button btn_register;
    private String device_id;
    private CoordinatorLayout snackbar;
    private RelativeLayout header;
    private EditText et_gender;
    private RelativeLayout rl_gender;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());

        initView();
    }

    private void initView() {
        tvterm = findViewById(R.id.tvterm);
        iv_back = findViewById(R.id.iv_back);
        snackbar = findViewById(R.id.snackbar);
        tv_login = findViewById(R.id.tv_login);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_email = findViewById(R.id.et_email);
        et_mobileNo = findViewById(R.id.et_mobileNo);
        et_password = findViewById(R.id.et_password);
        et_repassword = findViewById(R.id.et_repassword);
        et_address = findViewById(R.id.et_address);
        checkbox = findViewById(R.id.checkbox);
        countryCodePicker = findViewById(R.id.ccp);
        btn_register = findViewById(R.id.btn_register);
        snackbar.bringToFront();
        iv_back.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        checkbox.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        et_email.setText(getIntent().getStringExtra("email"));
        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Token = getSharedPreferences("Firbase_Token", MODE_PRIVATE).getString("regId", "");
        header = (RelativeLayout) findViewById(R.id.header);
        header.setOnClickListener(this);
        et_gender = (EditText) findViewById(R.id.et_gender);
        et_gender.setOnClickListener(this);
        tvterm.setOnClickListener(this);
        rl_gender = (RelativeLayout) findViewById(R.id.rl_gender);
        rl_gender.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String[] types = {"Male", "Female"};
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tvterm:
                startActivity(new Intent(this, Terms_and_conditions.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.et_gender:
                AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                builder.setTitle("");
                builder.setItems(types, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        et_gender.setText(types[which]);
                    }
                });
                builder.show();
                break;
            case R.id.tv_login:
                startActivity(new Intent(SignUp.this, Login.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.btn_register:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String fName = et_first_name.getText().toString().trim();
        if (TextUtils.isEmpty(fName)) {
//            Toast.makeText(this, "First Name", Toast.LENGTH_SHORT).show();
            CustomToast.makeText(getApplicationContext(), "Please enter your first name", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }

        String lName = et_last_name.getText().toString().trim();
        if (TextUtils.isEmpty(lName)) {

            CustomToast.makeText(getApplicationContext(), "Please enter your last name", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }

        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {

            CustomToast.makeText(getApplicationContext(), "Please enter Email", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!Utility.isValidEmailId(email)) {
            CustomToast.makeText(getApplicationContext(), "Please enter Valid Email", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        String gender = et_gender.getText().toString().trim();
        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(this, "Please select Gender", Toast.LENGTH_SHORT).show();
            return;
        }


        String mobNo = et_mobileNo.getText().toString().trim();
        if (TextUtils.isEmpty(mobNo)) {

            CustomToast.makeText(getApplicationContext(), "Please enter mobile no.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!Utility.isValidMobile(mobNo)) {
            CustomToast.makeText(getApplicationContext(), "Please enter valid mobile no.", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
            return;
        }
        String pass = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(pass)) {
            CustomToast.makeText(getApplicationContext(), " Please enter password", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        String strrepassword = et_repassword.getText().toString().trim();
        if (TextUtils.isEmpty(strrepassword)) {
            CustomToast.makeText(getApplicationContext(), " Please enter Confirm password", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!pass.equals(strrepassword)) {
            CustomToast.makeText(getApplicationContext(), "Password do not match", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }

        String adrs = et_address.getText().toString().trim();
        if (TextUtils.isEmpty(adrs)) {
            CustomToast.makeText(getApplicationContext(), "Please enter address", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }
        if (!checkbox.isChecked()) {

            CustomToast.makeText(getApplicationContext(), "Please Accept Terms And Conditions", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

            return;
        }


    /*    String number = "+91"+userPhoneString;
        String android_id = Settings.Secure.getString(Register.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);*/
        // TODO validate success, do something
        Map<String, String> map = new HashMap<>();
        map.put("first_name", fName);
        map.put("last_name", lName);
        map.put("email", email);
        map.put("gender", gender);
        map.put("mobile", mobNo);
        map.put("password", pass);
        map.put("isdevice", "android");
        map.put("address", adrs);
        map.put("deviceToken", Token);
        map.put("phoneCode", countryCodePicker.getSelectedCountryCodeWithPlus());
        map.put("country", countryCodePicker.getSelectedCountryEnglishName());
        Log.d("asdfdsd", countryCodePicker.getSelectedCountryCodeWithPlus() + "_____" + countryCodePicker.getSelectedCountryEnglishName());
        Signup(map);
    }

    public void Signup(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(SignUp.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SignUpModel> call = Apis.getAPIService().SignUpApi(map);
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                SignUpModel user = response.body();
                if (user.getStatusCode().equals("200")) {
                    session.iseditor();
                    session.createUserLoginSession(String.valueOf(user.getData().get(0).getId()),
                            user.getData().get(0).getFirst_name(), user.getData().get(0).getLast_name(),
                            user.getData().get(0).getEmail(), user.getData().get(0).getMobile(),
                            user.getData().get(0).getUser_status(), user.getData().get(0).getImage(), user.getData().get(0).getNotification());
                    Utility.setdetail(SignUp.this, user.getData().get(0).getImage());
                    Utility.setCountryCode(SignUp.this, user.getData().get(0).getPhoneCode());
                    startActivity(new Intent(SignUp.this, Verification.class).putExtra("userId", String.valueOf(user.getData().get(0).getId())).putExtra("tag", "2"));
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                } else if (user.getStatusCode().equals("8")) {
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                } else {
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }

            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snack = Snackbar.make(snackbar, message, Snackbar.LENGTH_LONG);
        View snackBarView = snack.getView();
        // snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
        snackBarView.setBackgroundResource(R.drawable.edtbackground);

        snack.setAction("CLOSE", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        snack.setActionTextColor(getResources().getColor(android.R.color.black));
        snack.show();


    }

}
