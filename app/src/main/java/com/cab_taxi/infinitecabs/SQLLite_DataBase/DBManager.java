package com.cab_taxi.infinitecabs.SQLLite_DataBase;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;

public class DBManager {

    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String placeposition, String placenaeme,String placeaddress,String lat,String lng ) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.PLACEPOSITION, placeposition);
        contentValue.put(DatabaseHelper.PLACENAME, placenaeme);
        contentValue.put(DatabaseHelper.PLACEADDRESS, placeaddress);
        contentValue.put(DatabaseHelper.LAT, lat);
        contentValue.put(DatabaseHelper.LNG, lng);
        database.insert(DatabaseHelper.TABLE_NAME, null, contentValue);
        CustomToast.makeText(context, "Save Address Insert Successfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS,true).show();

    }

    public Cursor fetch() {
        String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.PLACEPOSITION, DatabaseHelper.PLACENAME, DatabaseHelper.PLACEADDRESS, DatabaseHelper.LAT, DatabaseHelper.LNG };
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _id, String placeposition, String placenaeme,String placeaddress,String lat,String lng ) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.PLACEPOSITION, placeposition);
        contentValues.put(DatabaseHelper.PLACENAME, placenaeme);
        contentValues.put(DatabaseHelper.PLACEADDRESS, placeaddress);
        contentValues.put(DatabaseHelper.LAT, lat);
        contentValues.put(DatabaseHelper.LNG, lng);
        int i = database.update(DatabaseHelper.TABLE_NAME, contentValues, DatabaseHelper._ID + " = " + _id, null);
        return i;
    }

    public void delete(long _id) {
        database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper._ID + "=" + _id, null);
        CustomToast.makeText(context, "Save Address Delete Successfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS,true).show();

    }
    public void deleteInformation(String whereClause , String[] whereArgs, SQLiteDatabase db){


        db.delete(DatabaseHelper.TABLE_NAME,whereClause,whereArgs);


        //Log.e(TAG,"One row is deleted with username: "+ Arrays.toString(whereArgs));
    }
}
