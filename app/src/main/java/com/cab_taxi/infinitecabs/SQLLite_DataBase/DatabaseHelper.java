package com.cab_taxi.infinitecabs.SQLLite_DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "SAVE_ADDRESS";

    // Table columns
    public static final String _ID = "_id";
    public static final String PLACEPOSITION = "place_postion";
    public static final String PLACENAME = "place_name";
    public static final String PLACEADDRESS = "place_address";
    public static final String LAT = "lat";
    public static final String LNG = "lng";

    // Database Information
    static final String DB_NAME = "SAVEADDRESS_TAXIAPP.DB";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PLACEPOSITION + " TEXT NOT NULL, " + PLACENAME + " TEXT," + PLACEADDRESS + " TEXT NOT NULL, " + LAT + " TEXT, " + LNG + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}