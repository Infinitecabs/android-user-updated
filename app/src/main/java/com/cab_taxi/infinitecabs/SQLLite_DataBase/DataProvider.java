package com.cab_taxi.infinitecabs.SQLLite_DataBase;

public class DataProvider {

    private String placeposition;
    private String placename;
    private String placeaddress;
    private String lat;
    private String lng;

    public String getPlaceposition() {
        return placeposition;
    }

    public String getPlacename() {
        return placename;
    }

    public String getPlaceaddress() {
        return placeaddress;
    }
    public String getLat() {
        return lat;
    }
    public String getLng() {
        return lng;
    }
    public DataProvider(String placeposition, String placename, String placeaddress,String lat,String lng){

        this.placeposition = placeposition;
        this.placename = placename;
        this.placeaddress = placeaddress;
        this.lat = lat;
        this.lng = lng;
    }
}