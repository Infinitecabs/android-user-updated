package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Insert_Card_Model {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("cardId")
    private String cardId;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("card_type")
        private String card_type;
        @Expose
        @SerializedName("csv_no")
        private String csv_no;
        @Expose
        @SerializedName("expairy_date")
        private String expairy_date;
        @Expose
        @SerializedName("card_number")
        private String card_number;
        @Expose
        @SerializedName("card_holder_name")
        private String card_holder_name;
        @Expose
        @SerializedName("user_id")
        private String user_id;

        public String getCard_type() {
            return card_type;
        }

        public void setCard_type(String card_type) {
            this.card_type = card_type;
        }

        public String getCsv_no() {
            return csv_no;
        }

        public void setCsv_no(String csv_no) {
            this.csv_no = csv_no;
        }

        public String getExpairy_date() {
            return expairy_date;
        }

        public void setExpairy_date(String expairy_date) {
            this.expairy_date = expairy_date;
        }

        public String getCard_number() {
            return card_number;
        }

        public void setCard_number(String card_number) {
            this.card_number = card_number;
        }

        public String getCard_holder_name() {
            return card_holder_name;
        }

        public void setCard_holder_name(String card_holder_name) {
            this.card_holder_name = card_holder_name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }
}
