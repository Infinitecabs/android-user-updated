package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Dasboard_Car_List_Model {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("seat_capacity")
        private String seat_capacity;
        @Expose
        @SerializedName("max_rate")
        private int max_rate;
        @Expose
        @SerializedName("min_rate")
        private String min_rate;
        @Expose
        @SerializedName("cartype")
        private String cartype;
        @Expose
        @SerializedName("cab_id")
        private String cab_id;
        @Expose
        @SerializedName("distance")
        private double distance;
        @Expose
        @SerializedName("longlatitude")
        private String longlatitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("driver_id")
        private String driver_id;

        public String getSeat_capacity() {
            return seat_capacity;
        }

        public void setSeat_capacity(String seat_capacity) {
            this.seat_capacity = seat_capacity;
        }

        public int getMax_rate() {
            return max_rate;
        }

        public void setMax_rate(int max_rate) {
            this.max_rate = max_rate;
        }

        public String getMin_rate() {
            return min_rate;
        }

        public void setMin_rate(String min_rate) {
            this.min_rate = min_rate;
        }

        public String getCartype() {
            return cartype;
        }

        public void setCartype(String cartype) {
            this.cartype = cartype;
        }

        public String getCab_id() {
            return cab_id;
        }

        public void setCab_id(String cab_id) {
            this.cab_id = cab_id;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getLonglatitude() {
            return longlatitude;
        }

        public void setLonglatitude(String longlatitude) {
            this.longlatitude = longlatitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }
    }
}
