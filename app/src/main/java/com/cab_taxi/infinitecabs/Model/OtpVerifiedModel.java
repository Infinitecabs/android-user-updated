package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpVerifiedModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("selected")
        private String selected;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("csv_no")
        private String csv_no;
        @Expose
        @SerializedName("expairy_date")
        private String expairy_date;
        @Expose
        @SerializedName("card_number")
        private String card_number;
        @Expose
        @SerializedName("card_holder_name")
        private String card_holder_name;
        @Expose
        @SerializedName("cardId")
        private String cardId;
        @Expose
        @SerializedName("paymentMode")
        private String paymentMode;
        @Expose
        @SerializedName("otpstatus")
        private String otpstatus;
        @Expose
        @SerializedName("notification")
        private String notification;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("user_status")
        private String user_status;
        @Expose
        @SerializedName("device_id")
        private String device_id;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("last_name")
        private String last_name;
        @Expose
        @SerializedName("first_name")
        private String first_name;
        @Expose
        @SerializedName("id")
        private String id;

        public String getSelected() {
            return selected;
        }

        public void setSelected(String selected) {
            this.selected = selected;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCsv_no() {
            return csv_no;
        }

        public void setCsv_no(String csv_no) {
            this.csv_no = csv_no;
        }

        public String getExpairy_date() {
            return expairy_date;
        }

        public void setExpairy_date(String expairy_date) {
            this.expairy_date = expairy_date;
        }

        public String getCard_number() {
            return card_number;
        }

        public void setCard_number(String card_number) {
            this.card_number = card_number;
        }

        public String getCard_holder_name() {
            return card_holder_name;
        }

        public void setCard_holder_name(String card_holder_name) {
            this.card_holder_name = card_holder_name;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getOtpstatus() {
            return otpstatus;
        }

        public void setOtpstatus(String otpstatus) {
            this.otpstatus = otpstatus;
        }

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUser_status() {
            return user_status;
        }

        public void setUser_status(String user_status) {
            this.user_status = user_status;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
