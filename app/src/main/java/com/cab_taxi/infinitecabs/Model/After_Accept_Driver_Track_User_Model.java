package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class After_Accept_Driver_Track_User_Model {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("longlatitude")
        private String longlatitude;
        @Expose
        @SerializedName("droplatitude")
        private String droplatitude;

        @Expose
        @SerializedName("droplonglatitude")
        private String droplonglatitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("booking_id")
        private String booking_id;
        @Expose
        @SerializedName("payStatus")
        private String payStatus;
        @Expose
        @SerializedName("driverRating")
        private String driverRating;
        @Expose
        @SerializedName("pick_address")
        private String pick_address;
        @Expose
        @SerializedName("drop_address")
        private String drop_address;
        @Expose
        @SerializedName("driver_id")
        private String driver_id;
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("paymentlink")
        private String paymentlink;
        @Expose
        @SerializedName("driverCar_Model")
        private String driverCar_Model;
        @Expose
        @SerializedName("driverCar_No")
        private String driverCar_No;
        @Expose
        @SerializedName("driverName")
        private String driverName;
        @Expose
        @SerializedName("driverImage")
        private String driverImage;
        @Expose
        @SerializedName("eta")
        private String eta;
        @Expose
        @SerializedName("carImage")
        private String carImage;

        public String getPayStatus() {
            return payStatus;
        }

        public void setPayStatus(String payStatus) {
            this.payStatus = payStatus;
        }

        public String getCarImage() {
            return carImage;
        }

        public String getDriverRating() {
            return driverRating;
        }

        public void setDriverRating(String driverRating) {
            this.driverRating = driverRating;
        }

        public void setCarImage(String carImage) {
            this.carImage = carImage;
        }

        public String getDriverCar_Model() {
            return driverCar_Model;
        }

        public void setDriverCar_Model(String driverCar_Model) {
            this.driverCar_Model = driverCar_Model;
        }

        public String getDriverCar_No() {
            return driverCar_No;
        }

        public void setDriverCar_No(String driverCar_No) {
            this.driverCar_No = driverCar_No;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverImage() {
            return driverImage;
        }

        public void setDriverImage(String driverImage) {
            this.driverImage = driverImage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLonglatitude() {
            return longlatitude;
        }

        public String getEta() {
            return eta;
        }

        public String getDroplatitude() {
            return droplatitude;
        }

        public String getPaymentlink() {
            return paymentlink;
        }

        public void setPaymentlink(String paymentlink) {
            this.paymentlink = paymentlink;
        }

        public void setDroplatitude(String droplatitude) {
            this.droplatitude = droplatitude;
        }

        public String getDroplonglatitude() {
            return droplonglatitude;
        }

        public void setDroplonglatitude(String droplonglatitude) {
            this.droplonglatitude = droplonglatitude;
        }

        public String getPick_address() {
            return pick_address;
        }

        public void setPick_address(String pick_address) {
            this.pick_address = pick_address;
        }

        public String getDrop_address() {
            return drop_address;
        }

        public void setDrop_address(String drop_address) {
            this.drop_address = drop_address;
        }

        public void setEta(String eta) {
            this.eta = eta;
        }

        public void setLonglatitude(String longlatitude) {
            this.longlatitude = longlatitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }
}
