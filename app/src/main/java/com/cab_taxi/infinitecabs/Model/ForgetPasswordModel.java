package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ForgetPasswordModel {
    @Expose
    @SerializedName("userId")
    private String userId;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;
    @Expose
    @SerializedName("otp")
    private String otp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
