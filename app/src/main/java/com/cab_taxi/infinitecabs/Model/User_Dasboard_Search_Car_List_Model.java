package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class User_Dasboard_Search_Car_List_Model {
    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("user_drop_long_lattitude")
        private String user_drop_long_lattitude;
        @Expose
        @SerializedName("user_drop_lattitude")
        private String user_drop_lattitude;
        @Expose
        @SerializedName("user_picup_long_lattitude")
        private String user_picup_long_lattitude;
        @Expose
        @SerializedName("user_picup_lattitude")
        private String user_picup_lattitude;
        @Expose
        @SerializedName("km")
        private String km;
        @Expose
        @SerializedName("seat_capacity")
        private String seat_capacity;
        @Expose
        @SerializedName("max_rate")
        private int max_rate;
        @Expose
        @SerializedName("min_rate")
        private int min_rate;
        @Expose
        @SerializedName("distance")
        private double distance;
        @Expose
        @SerializedName("car_rate")
        private String car_rate;
        @Expose
        @SerializedName("carimage")
        private String carimage;
        @Expose
        @SerializedName("cartype")
        private String cartype;
        @Expose
        @SerializedName("cab_type_id")
        private String cab_type_id;

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getUser_drop_long_lattitude() {
            return user_drop_long_lattitude;
        }

        public void setUser_drop_long_lattitude(String user_drop_long_lattitude) {
            this.user_drop_long_lattitude = user_drop_long_lattitude;
        }

        public String getUser_drop_lattitude() {
            return user_drop_lattitude;
        }

        public void setUser_drop_lattitude(String user_drop_lattitude) {
            this.user_drop_lattitude = user_drop_lattitude;
        }

        public String getUser_picup_long_lattitude() {
            return user_picup_long_lattitude;
        }

        public void setUser_picup_long_lattitude(String user_picup_long_lattitude) {
            this.user_picup_long_lattitude = user_picup_long_lattitude;
        }

        public String getUser_picup_lattitude() {
            return user_picup_lattitude;
        }

        public void setUser_picup_lattitude(String user_picup_lattitude) {
            this.user_picup_lattitude = user_picup_lattitude;
        }

        public String getSeat_capacity() {
            return seat_capacity;
        }

        public void setSeat_capacity(String seat_capacity) {
            this.seat_capacity = seat_capacity;
        }

        public int getMax_rate() {
            return max_rate;
        }

        public void setMax_rate(int max_rate) {
            this.max_rate = max_rate;
        }

        public int getMin_rate() {
            return min_rate;
        }

        public void setMin_rate(int min_rate) {
            this.min_rate = min_rate;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getCar_rate() {
            return car_rate;
        }

        public void setCar_rate(String car_rate) {
            this.car_rate = car_rate;
        }

        public String getCarimage() {
            return carimage;
        }

        public void setCarimage(String carimage) {
            this.carimage = carimage;
        }

        public String getCartype() {
            return cartype;
        }

        public void setCartype(String cartype) {
            this.cartype = cartype;
        }

        public String getCab_type_id() {
            return cab_type_id;
        }

        public void setCab_type_id(String cab_type_id) {
            this.cab_type_id = cab_type_id;
        }
    }
}
