package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallDriverModel {

    @Expose
    @SerializedName("bookingData")
    private BookingData bookingData;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public BookingData getBookingData() {
        return bookingData;
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class BookingData {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("twilio")
        private String twilio;
        @Expose
        @SerializedName("bookingdate")
        private String bookingdate;
        @Expose
        @SerializedName("driverMobile")
        private String driverMobile;
        @Expose
        @SerializedName("driverName")
        private String driverName;
        @Expose
        @SerializedName("driverId")
        private String driverId;
        @Expose
        @SerializedName("bookingId")
        private String bookingId;

        public String getTwilio() {
            return twilio;
        }

        public void setTwilio(String twilio) {
            this.twilio = twilio;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBookingdate() {
            return bookingdate;
        }

        public void setBookingdate(String bookingdate) {
            this.bookingdate = bookingdate;
        }

        public String getDriverMobile() {
            return driverMobile;
        }

        public void setDriverMobile(String driverMobile) {
            this.driverMobile = driverMobile;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }
    }
}
